# coding=utf-8
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from pandas import Series
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import numpy as np
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False


def plot_single_ax(ax, data, oslon, oslat, ranges=None, labels=None, clipshape=False, colorbar=False,
                   gridline=False, shplinewidth=1, ylabels=None, xlabels=None, cutdata=False, quiverkey=False):
    '''data: list, [data waited for plot{if quiver wind is required, you can put data as
                   [uwnd, vwnd, [uwind space(int), vwnd space(int))]] to control the density of wind quiver.}


    ,
                    plot method(i.e., contour),
                    level method (list, 'zero', 'hgt')]

    colorbar: if you need a colorbar beside each axes, input 'v' or 'h' as the position of colorbar.

    '''
    import cartopy.crs as ccrs
    import matplotlib.pyplot as plt
    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
    from matplotlib import colors
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader


    # function for classification, choosed by key in data.
    def separate_zero(data, half_level_number=6):
        vmax = np.nanmax(data, axis=None)
        vmin = np.nanmin(data, axis=None)
        if (vmax > 0) & (vmin < 0):
            level_min = np.linspace(vmin, 0, half_level_number)
            level_max = np.linspace(0, vmax, half_level_number)
            anomalylevels = np.hstack((level_min, level_max[1:]))

        else:
            anomalylevels = np.linspace(vmin, vmax, half_level_number*2+1)

        levels = np.unique([round(i, 2) for i in anomalylevels])
        return levels

    def find_hgt_levels(data, space):

        # only works under space == 10,5,2
        vmax = np.nanmax(data, axis=None)
        vmin = np.nanmin(data, axis=None)
        roundmin = round(vmin)
        roundmax = round(vmax)
        if space != 10:
            roundmin = roundmin - roundmin % space
            roundmax = roundmax + space - roundmax % space
            levels = np.arange(roundmin, roundmax + 1, space)
        else:
            roundmin = roundmin - roundmin % 5
            roundmax = roundmax + 5 - roundmax % 5

            levels = np.arange(roundmin, roundmax + 10, space)
        return levels

    # judge whether data is one layer.


    for sdata in data:
        plot_data = sdata[0]
        # if ranges is not None:
        #     slat, slon, plot_data = del_ll(ranges[0], plot_data, oslat, oslon)
        # else:
        slat = oslat
        slon = oslon

        if (cutdata == True) & (ranges is not None) & ((sdata[1] == 'contour') | (sdata[1] == 'contourf')):
            plot_data, slat, slon = del_ll(ranges[0], sdata[0], oslat, oslon)
        plot_method = sdata[1]
        level_method = sdata[2]
        # print(plot_data)
        # judge level
        if isinstance(level_method, str):
            if level_method == 'hgt':
                slevel = find_hgt_levels(plot_data, sdata[3])
            elif level_method == 'zero':
                slevel = separate_zero(plot_data)
            else:
                print('''sdata[1] is not required method. choose 'hgt' or 'zero' ''')
        else:
            slevel = level_method

        # judge plot method
        if plot_method == 'contour':
            confill = ax.contour(slon, slat,plot_data, levels=slevel)
            ax.clabel(confill, fmt='%i')

        elif plot_method == 'contourf':
            from matplotlib import cm
            # cmap = cm.get_cmap('RdYlBu')
            # tmap = cmap.reversed()
            if level_method == 'zero':
                colormap = ['#334a9e', '#466fb5', '#72b0d5','#94cae4', '#b4e0ef',
                            '#ffd483','#ff9158', '#f76b42','#e74630', '#b40c26']
                fill = ax.contourf(slon, slat, plot_data, levels=slevel
                                   , colors=colormap)
            else:
                cmap = cm.get_cmap('RdYlBu')
                tmap = cmap.reversed()
                fill = ax.contourf(slon, slat, plot_data, levels=slevel ,cmap=tmap)



        elif plot_method == 'quiver2':
            udata = plot_data[0]
            vdata = plot_data[1]
            if len(plot_data) == 3:
                spacewind = plot_data[2]
                # scale越大，线会越长，也就是同样的比例，线更长。而width则会让线变粗，越大越粗。
                qui = ax.quiver(slon[::spacewind[0]], slat[::spacewind[1]], udata[::spacewind[1], ::spacewind[0]], vdata[::spacewind[1], ::spacewind[0]]
                                , transform=ccrs.PlateCarree())
            else:
                qui = ax.quiver(slon, slat, udata, vdata
                                , transform=ccrs.PlateCarree())
            if quiverkey is True:
                qk = ax.quiverkey(qui, 1, 1.05, 5, r'$5 \frac{m}{s}$', labelpos='E',
                                  coordinates='axes')

        elif plot_method == 'quiver1':
            udata = plot_data
            try:
                udata = np.ma.masked_less(udata, 30)
                # print(udata[np.where(udata>30)])
                bwfill = ax.contourf(slon, slat, udata, 3, transform=ccrs.PlateCarree(), cmap="Greys")
            except:
                print('no')

    ax.format(coast=True, lonlabels='b', latlabels='l', lonlim=(ranges[0][2], ranges[0][3]), latlim=(ranges[0][0], ranges[0][1]))

    # # #添加shp文件进入图片中
    if clipshape == True:
        pathss = shp2clip(r'D:\basis\data\shp\shengjie\all_china.shp')
        plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
        upath = PathPatch(pathss, transform=plate_carre_data_transform)

        for collection in fill.collections:
            collection.set_clip_path(upath)

    shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\shengjie\bou2_4p.shp').geometries(),
                                   ccrs.PlateCarree(),
                                   edgecolor='k', facecolor='none', linewidths=0.55)
    ax.add_feature(shape_feature)
    shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\jiuduanxian\jiuduanxian.shp').geometries(),
                                   ccrs.PlateCarree(),
                                   edgecolor='k', facecolor='none', linewidths=shplinewidth)
    ax.add_feature(shape_feature)
    shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\jiuduanxian\nanhai.shp').geometries(),
                                   ccrs.PlateCarree(),
                                   edgecolor='k', facecolor='none', linewidths=shplinewidth)
    ax.add_feature(shape_feature)


    return fill



def shp2clip(shpfile):

    import shapefile
    from matplotlib.path import Path
    from matplotlib.patches import PathPatch
    sf = shapefile.Reader(shpfile)
    vertices = []
    codes = []
    for shape_rec in sf.shapeRecords():
        pts = shape_rec.shape.points
        prt = list(shape_rec.shape.parts) + [len(pts)]
        for i in range(len(prt) - 1):
            for j in range(prt[i], prt[i + 1]):
                vertices.append((pts[j][0], pts[j][1]))
            codes += [Path.MOVETO]
            codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
            codes += [Path.CLOSEPOLY]
        clip = Path(vertices, codes)
    return clip

def del_ll(range, data, lat, lon):
    from current_function_in_fd import ll_del
    del_area = ll_del(lat, lon, range)
    # 人为重新排序
    if del_area[0] > del_area[1]:
        del_area = [del_area[1], del_area[0], del_area[2], del_area[3]]
    if del_area[2] > del_area[3]:
        del_area = [del_area[0], del_area[1], del_area[3], del_area[2]]
    lat = lat[del_area[0]:del_area[1]+1]
    lon = lon[del_area[2]:del_area[3] + 1]
    data = np.array(data)
    data = data[del_area[0]:del_area[1]+1, del_area[2]:del_area[3] + 1]
    return data, lat, lon




import scipy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt


class MidpointNormalize(mpl.colors.Normalize):
    def __init__(self, vmin, vmax, midpoint=0, clip=False):
        self.midpoint = midpoint
        mpl.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        normalized_min = max(0, 1 / 2 * (1 - abs((self.midpoint - self.vmin) / (self.midpoint - self.vmax))))
        normalized_max = min(1, 1 / 2 * (1 + abs((self.vmax - self.midpoint) / (self.midpoint - self.vmin))))
        normalized_mid = 0.5
        x, y = [self.vmin, self.midpoint, self.vmax], [normalized_min, normalized_mid, normalized_max]
        return sp.ma.masked_array(sp.interp(value, x, y))