import numpy as np
import pandas as pd
from pandas import DataFrame
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False

def plot_variable_figure(data, fdtimeindex, areaname):
    import matplotlib.pyplot as plt
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    from pylab import mpl
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False

    plotdata = data.iloc[fdtimeindex[0] - 5:fdtimeindex[1] + 5]

    fig = plt.figure(figsize=(9,4))
    fig, ax = plt.subplots(1,1)
    fig.subplots_adjust(hspace=0)
    # Remove horizontal space between axes
    import matplotlib.dates as mdates
    days = mdates.DayLocator(bymonthday=range(1, 32, 5))
    monthsFmt = mdates.DateFormatter('%m-%d')
    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_major_formatter(monthsFmt)

    ax.axvline(data.index[fdtimeindex[0]], color='black', linestyle='--', alpha=0.2)
    ax.axvline(data.index[fdtimeindex[1]], color='black', linestyle='--', alpha=0.2)

    ax.plot(plotdata.index, percentile_in_array(data['sm'], fdtimeindex), color='red', marker='.', label='sm')
    ax.plot(plotdata.index, percentile_in_array(data['et'], fdtimeindex), color='black', marker='.', label='et')
    ax.plot(plotdata.index, percentile_in_array(data['pet'], fdtimeindex), color='orange', marker='.', label='pet')
    ax.plot(plotdata.index, percentile_in_array(data['tem'] , fdtimeindex), color='blue', marker='.', label='tem')
    # ax.plot(plotdata.index, percentile_in_array(data['mpre'], fdtimeindex), color='green', linestyle='--', marker='.', label='mpre')
    # ax.plot(plotdata.index, percentile_in_array(data['qair'], fdtimeindex), color='blue', linestyle='--', marker='.', label='qair')
    # ax.plot(plotdata.index, percentile_in_array(data['qle'], fdtimeindex), color='purple', marker='.', label='qle')
    ax.plot(plotdata.index, percentile_in_array(data['swdown'], fdtimeindex), color='yellow', linestyle='--', marker='.', label='swdown')
    # ax.plot(plotdata.index, percentile_in_array(data['rootsoil'], fdtimeindex), color='red', linestyle='--', marker='.', label ='rootsoil')
    # ax.plot(plotdata.index, percentile_in_array(data['qh'], fdtimeindex), color='green', marker='.', label='qh')


    # ax.plot(plotdata.index, np.array(plotdata['qle']), color='purple', marker='.', label='qle '+str(100*percentile_in_array(data['qle'], fdtimeindex))+'%')
    ax.set_ylabel('factor in flash drought')
    ax.set_xlabel('%i' % plotdata.index.year[0])

    ax.spines['top'].set_color('none')

    plt.setp(ax.get_xticklabels(), rotation=30, ha='right')
    # fig.tight_layout()
    plt.legend(loc='upper right')
    # plt.show()
    plt.savefig('%s %s.png' % (str(data.index[fdtimeindex[0]])[:10], areaname), dpi=800)
    plt.close()
    return

def percentile_in_array(data, spep):
    # sp = spep[0]-5
    # ep = spep[1]+4
    sp = spep[0]
    ep = spep[1]
    def calc_percentile(a, method='min'):
        # 获得传入序列的百分位值
        from scipy.stats import rankdata
        import numpy as np
        if isinstance(a, list):
            a = np.asarray(a)
        s = rankdata(a, method=method) / float(len(a))
        result = [round(i, 3) for i in s]
        return result
    osesr_reshapeinyear = np.array(data).reshape([40, 73])
    etbegin = (sp + 1) % 73 - 1
    etend = (ep + 1) % 73 - 1

    percentile =[]
    for i in range(etbegin, etend+1):
        begin = osesr_reshapeinyear[:, i]
        sbegin = calc_percentile(begin)[int(sp / 73)]
        percentile.append(sbegin)

    return np.array(percentile).flatten()

def mean_all_fd_into_one_fd(data, variable):
    from current_function_in_fd import time_del
    def percentile_in_array(data):
        def calc_percentile(a, method='min'):
            # 获得传入序列的百分位值
            from scipy.stats import rankdata
            import numpy as np
            if isinstance(a, list):
                a = np.asarray(a)
            s = rankdata(a, method=method) / float(len(a))
            result = [round(i, 3) for i in s]
            return result

        osesr_reshapeinyear = np.array(data).reshape([19, 73])
        percentile = np.ones([19, 73])

        for i in range(73):
            begin = osesr_reshapeinyear[:, i]
            sbegin = calc_percentile(begin)
            percentile[:, i] = sbegin
        return np.array(percentile).flatten()
    for i in data.columns[1:]:
        data[i] = percentile_in_array(data[i])
    sfd = np.array(time_del(data.index[(data['mask'] == 1)]))-1
    efd = time_del(data.index[data['mask'] == 3])

    data_mean = np.zeros([len(sfd), 18])
    for i in range(len(sfd)):
        data_mean[i, 0:6] = data[variable].iloc[sfd[i]-6:sfd[i]]
        data_mean[i, 6:11] = data[variable].iloc[sfd[i]:sfd[i]+5]
        data_mean[i, 11:12] = np.mean(data[variable].iloc[sfd[i]+5:efd[i]+1])
        data_mean[i, 12:18] = data[variable].iloc[sfd[i]+1:sfd[i]+7]
    return np.mean(data_mean, axis=0)

def mean_fd_into_one_fd(data, variable, time):
    from current_function_in_fd import time_del
    def percentile_in_array(data):
        def calc_percentile(a, method='min'):
            # 获得传入序列的百分位值
            from scipy.stats import rankdata
            import numpy as np
            if isinstance(a, list):
                a = np.asarray(a)
            s = rankdata(a, method=method) / float(len(a))
            result = [round(i, 3) for i in s]
            return result

        osesr_reshapeinyear = np.array(data).reshape([19, 73])
        percentile = np.ones([19, 73])

        for i in range(73):
            begin = osesr_reshapeinyear[:, i]
            sbegin = calc_percentile(begin)
            percentile[:, i] = sbegin
        return np.array(percentile).flatten()
    for i in data.columns[1:]:
        data[i] = percentile_in_array(data[i])
    sfd = time[0]
    efd = time[1]
    data_mean = np.zeros([len(sfd), 18])
    for i in range(len(sfd)):
        data_mean[i, 0:6] = data[variable].iloc[sfd[i]-6:sfd[i]]
        data_mean[i, 6:11] = data[variable].iloc[sfd[i]:sfd[i]+5]
        data_mean[i, 11:12] = np.mean(data[variable].iloc[sfd[i]+5:efd[i]+1])
        data_mean[i, 12:18] = data[variable].iloc[sfd[i]+1:sfd[i]+7]
    return np.mean(data_mean, axis=0)

def plot_single_fd(ax, data, color, legend, labels=None):
    x = np.arange(18)
    ax.plot(x, data, color=color, label=legend)
    ax.grid(axis='y')
    ax.axvline(5, color='grey', alpha=0.8)
    ax.axvline(11, color='grey', alpha=0.8)
    ax.axhline(0.5, color='black', alpha=0.5)
    ax.set_xticks(x)
    ax.set_yticks([0,0.5,1])
    ax.set_xticklabels(['-6', '', '-4', '', '', '', 'begin', '','', '', '', 'end', '', '','','+4', '','+6'])

    if labels is not None:
        ax.text(ax.get_xlim()[0], ax.get_ylim()[1], labels)
if __name__ == '__main__':
    area_name = ['xinan', 'dongbei', 'xinjiang']
    allarea = [[24.125, 33.125, 102.125, 108.125],
               [40.125, 50.125, 122.125, 128.125],
               [36.125,47.125,87.125,98.125]]
    data1 = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[0], index_col=0, parse_dates=True)
    data2 = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[1], index_col=0, parse_dates=True)
    data3 = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[2], index_col=0, parse_dates=True)

    import matplotlib.pyplot as plt
    def main(type, time=None, area=None, number=None):
        if type == 1:
            namelist = ['et', 'pet', 'sm', 'swdown', 'wind', 'tem', 'qair', 'mpre', 'rootsoil', 'lw', 'sw', 'qh', 'qle']
            newlist = ['a) ET', 'b) PET', 'c) SM', 'd) SWD', 'e) WIN', 'f) TEMP', 'g) SH', 'h)PRCP',
                       'i) RS', 'j) LW', 'k) SW', 'l) QH', 'm) QLE']
            fig = plt.figure(figsize = [9,8])
            plt.subplots_adjust(top=0.945,
                                bottom=0.08,
                                left=0.04,
                                right=0.99,
                                hspace=0.3,
                                wspace=0.21)
            for i in range(13):
                # a = mean_all_fd_into_one_fd(data1, namelist[i])
                # plot_single_fd(fig.add_subplot(2,4,i+1), a, 'tab:blue', 'Yzr', newlist[i])

                a = mean_all_fd_into_one_fd(data1, namelist[i])
                plot_single_fd(fig.add_subplot(4,4,i+1), a, 'blue', '西南', newlist[i])

                a = mean_all_fd_into_one_fd(data2, namelist[i])
                plot_single_fd(fig.add_subplot(4,4,i+1), a, 'green', '东北')

                a = mean_all_fd_into_one_fd(data3, namelist[i])
                plot_single_fd(fig.add_subplot(4,4,i+1), a, 'black', '新疆')
            plt.show()

            plt.savefig(r'D:/basis/fd/figure5.png', dpi=800)

        if type == 2:
            namelist = ['et', 'pet', 'sm', 'swdown', 'wind', 'tem', 'qair', 'mpre', 'rootsoil', 'lw', 'sw', 'qh', 'qle']
            newlist = ['a) 实际蒸散发', 'b) 潜在蒸散发', 'c) 0-10cm土壤水分含量', 'd) 向下短波辐射', 'e) 风速', 'f) 温度',
                       'g) 比湿度', 'h)降水',
                       'i) 根区土壤水分含量', 'j) 净长波辐射', 'k) 净短波辐射', 'l) 感热通量', 'm) 潜热通量']
            fig = plt.figure(figsize=[9, 8])
            plt.subplots_adjust(top=0.945,
                                bottom=0.08,
                                left=0.04,
                                right=0.99,
                                hspace=0.3,
                                wspace=0.21)
            for i in range(13):
                # a = mean_all_fd_into_one_fd(data1, namelist[i])
                # plot_single_fd(fig.add_subplot(2,4,i+1), a, 'tab:blue', 'Yzr', newlist[i])
                if area == 'Southwest':
                    a = mean_fd_into_one_fd(data1, namelist[i], time)
                    plot_single_fd(fig.add_subplot(4, 4, i + 1), a, 'blue', '西南', newlist[i])
                if area == 'Northeast':
                    a = mean_fd_into_one_fd(data2, namelist[i], time)
                    plot_single_fd(fig.add_subplot(4, 4, i + 1), a, 'green', '东北', newlist[i])
                if area == 'Northwest':
                    a = mean_fd_into_one_fd(data3, namelist[i], time)
                    plot_single_fd(fig.add_subplot(4, 4, i + 1), a, 'black', '新疆', newlist[i])
            fig.text(0.95, 0.96, number, fontsize=12)
            plt.annotate(area,
                         [0.6, 0.18], xycoords='figure fraction', fontsize=15)
            plt.annotate('闪旱开始时间: '+str(data1.index[time[0][0]])[:10],
                         [0.53, 0.13], xycoords='figure fraction', fontsize=15)
            plt.annotate('闪旱结束时间: '+str(data1.index[time[1][0]]+ np.timedelta64(5, 'D'))[:10],
                         [0.53, 0.08], xycoords='figure fraction', fontsize=15)
            # plt.show()
            plt.savefig('%s.png'%(area+str(data1.index[time[0][0]])[:10]))


    # main(type=1)
    main(2, [[190],[195]], 'Southwest', 'A2')
    main(2, [[475], [481]], 'Southwest', 'B2')

    main(2, [[167], [175]], 'Northeast', 'C2')
    main(2, [[418], [424]], 'Northeast', 'D2')
    main(2, [[548], [553]], 'Northeast', 'E2')
    main(2, [[1113], [1118]], 'Northeast', 'F2')

    # fig = plt.figure(figsize=[9, 0.3])
    # plt.plot([0,1],[0,0], alpha=0)
    # plt.plot([0.0,0.1],[0,0], color='blue')
    # plt.text(0.12, -0.03, "西南"
    #            )
    #
    # plt.plot([0.45,0.55],[0,0], color='green')
    # plt.text(0.57, -0.03, "东北"
    #            )
    #
    # plt.plot([0.9,1],[0,0], color='black')
    # plt.text(1.02, -0.03, "新疆"
    #            )
    #
    # sax = plt.gca()
    # sax.get_yaxis().set_visible(False)  # 不显示y轴
    # sax.get_xaxis().set_visible(False)
    # sax.spines['top'].set_visible(False)  # 去掉上边框
    # sax.spines['bottom'].set_visible(False)  # 去掉下边框
    # sax.spines['left'].set_visible(False)  # 去掉左边框
    # sax.spines['right'].set_visible(False)
    # plt.show()
    # # plt.savefig('figure5table.png', dpi=800)
    #
    #
    #
    # # plt.savefig('figure5lable.svg')
    # # for j in range(0, 3):
    # #     data = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[j], index_col=0, parse_dates=True)
    # #     sfd = data.index[(data['mask'] == 1)]
    # #     efd = data.index[data['mask'] == 3]
    # #     fdtime = []
    # #     from current_function_in_fd import time_del
    # #     for i in range(len(sfd)):
    # #         plot_variable_figure(data, time_del([sfd[i], efd[i]]), area_name[j])




