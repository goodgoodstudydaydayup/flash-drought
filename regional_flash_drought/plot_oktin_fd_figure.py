import numpy as np
import pandas as pd


def plot_allyear_figure(plotdata, fdtimeindex, varible_name):
    import current_function_in_fd as cf
    import matplotlib.patheffects as pe
    import matplotlib.pyplot as plt
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    from pylab import mpl
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False

    fig = plt.figure(figsize=[12, 8])
    ax = fig.add_subplot(111)
    plt.subplots_adjust(left=0.11, bottom=0.15, right=0.98, top=0.95, wspace=0.2, hspace=0.2)

    try:
        for i in fdtimeindex:
            ax.axvline(i, color='black', linestyle='--', alpha=0.2)
    except:
        print('sss')

    ax.axhline(0, color='black', alpha=0.2)
    # ax.plot(plotdata.index, np.array(plotdata['rses']), color='red', marker='.')
    ax.plot(plotdata.index, np.array(plotdata['sm']), color='red', marker='.', label='sm')
    ax.plot(plotdata.index, np.array(plotdata['et']), color='black', marker='.', label='et')
    ax.plot(plotdata.index, np.array(plotdata['pet']), color='orange', marker='.', label='pet')
    ax.plot(plotdata.index, np.array(plotdata['tem']), color='blue', marker='.', label='tem')
    ax.plot(plotdata.index, np.array(plotdata['qh']), color='green', marker='.', label='qh')
    ax.plot(plotdata.index, np.array(plotdata['qle']), color='purple', marker='.', label='qle')
    # ax.plot(plotdata.index, np.array(plotdata['pre']), color='grey', marker='.', label='pre')
    # ax.plot(plotdata.index, np.array(plotdata['rses']), color='red', marker='.')

    ax.set_xlim(plotdata.index[0], plotdata.index[-1])

    plt.legend()
    ax.set_ylabel('SESR')
    ax.set_xlabel('%i' % plotdata.index.year[0])
    plt.show()
    plt.savefig('%s %s.png' % (str(plotdata.index.year[0]), varible_name))
    plt.close()


def plot_oktin_figure(data, fdtimeindex):
    import current_function_in_fd as cf
    import matplotlib.patheffects as pe
    aaaname = ['rses', 'percentile_rses', 'orses', 'percentile_orses']
    fdtimeindex = [fdtimeindex[0]-1, fdtimeindex[1]]
    plotdata = data.iloc[fdtimeindex[0] - 2:fdtimeindex[1] + 3]

    import matplotlib.pyplot as plt
    from pandas.plotting import register_matplotlib_converters

    register_matplotlib_converters()
    from pylab import mpl

    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False

    fig = plt.figure(figsize=[15, 8])
    ax = fig.add_subplot(111)
    plt.subplots_adjust(left=0.11, bottom=0.15, right=0.98, top=0.95, wspace=0.2, hspace=0.2)

    ax.axvline(data.index[fdtimeindex[0]], color='black', linestyle='--', alpha=0.2)
    ax.axvline(data.index[fdtimeindex[1]], color='black', linestyle='--', alpha=0.2)
    ax.axhline(0, color='black', alpha=0.2)
    ax.plot(plotdata.index, np.array(plotdata['rses']), color='red')
    ax.plot([plotdata.index[2], plotdata.index[-3]], np.array(plotdata['rses'])[[2, -3]], linestyle='--', color='red',
            label='mean OSESR')

    anglex = ax.get_xticks()
    xyl2 = np.array([1, 2])
    trans_angle = plt.gca().transData.transform_angles(
        np.array((np.rad2deg(np.arctan2(plotdata['rses'][-3] - plotdata['rses'][2], anglex[-3] - anglex[2])),)),
        xyl2.reshape((1, 2)))[0]
    ax.text(plotdata.index[int(len(plotdata) / 2)], np.nanmean((plotdata['rses'][2], plotdata['rses'][-3]))
            , str(int(plotdata['percentile25'][3] * 100)) + 'th', rotation=trans_angle, ha='center',
            path_effects=[pe.withStroke(linewidth=3, foreground='w')])

    import matplotlib.dates as mdates
    days = mdates.DayLocator(bymonthday=range(1, 32, 5))
    monthsFmt = mdates.DateFormatter('%m-%d')
    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_major_formatter(monthsFmt)

    # 设置标签
    x = plotdata.index[0:-1]
    y = np.array(plotdata['rses'])[0:-1]
    z = np.array(plotdata['percentile_orses'])[1:] * 100
    anglex = ax.get_xticks()
    for i in range(len(x)):
        xylist = [anglex[i], anglex[i + 1], np.array(plotdata['rses'])[i], np.array(plotdata['rses'])[i + 1]]
        xyl2 = np.array([anglex[i], np.array(plotdata['rses'])[i + 1]])
        trans_angle = plt.gca().transData.transform_angles(
            np.array((np.rad2deg(np.arctan2(xylist[-1] - xylist[2], xylist[1] - xylist[0])),)),
            xyl2.reshape((1, 2)))[0]

        text = ax.text(x[i] + np.timedelta64(2, 'D'),
                       (np.array(plotdata['rses'])[i + 1] + np.array(plotdata['rses'])[i]) / 2,
                       str(int(round(z[i], 0))) + 'th', rotation=trans_angle, ha='center',
                       path_effects=[pe.withStroke(linewidth=3, foreground='w')])

    yposition = ax.get_ylim()[0] + (ax.get_ylim()[1] - ax.get_ylim()[0]) / 6
    ax.text(data.index[fdtimeindex[0]], yposition, 'Rapid Drought Intensification Begins'
            , rotation=90)
    ax.text(data.index[fdtimeindex[1]], yposition, 'Rapid Drought Intensification Ends'
            , rotation=90)
    ax.set_ylabel('SESR')
    ax.set_xlabel('%i' % plotdata.index.year[0])
    plt.setp(ax.get_xticklabels(), rotation=30, ha='right')
    # fig.tight_layout()
    plt.legend(loc='upper right')
    plt.show()
    # plt.savefig('%s %s.png' % (str(fdtimeindex), varible_name))
    plt.close()


def plot_variable_figure(data, fdtimeindex, areaname):
    import matplotlib.pyplot as plt
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    from pylab import mpl
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False

    plotdata = data.iloc[fdtimeindex[0] - 5:fdtimeindex[1] + 5]

    fig = plt.figure(figsize=(9,4))
    fig, axs = plt.subplots(2,1, sharex=True)
    fig.subplots_adjust(hspace=0)
    # Remove horizontal space between axes
    import matplotlib.dates as mdates
    # days = mdates.DayLocator(bymonthday=range(1, 32, 5))
    # monthsFmt = mdates.DateFormatter('%m-%d')
    # axs[1].xaxis.set_major_locator(days)
    # axs[1].xaxis.set_major_formatter(monthsFmt)
    #
    # axs[0].plot(plotdata.index, np.array(plotdata['qle']), color='purple', marker='.', label='qle '+str(100*percentile_in_array(data['qle'], fdtimeindex))+'%')
    # axs[0].plot(plotdata.index, np.array(plotdata['swdown']), color='yellow', linestyle='--', marker='.',  label='swdown '+str(100 * percentile_in_array(data['swdown'], fdtimeindex))+'%')
    # axs[0].plot(plotdata.index, np.array(plotdata['rootsoil']), color='red', linestyle='--', marker='.', label='rootsoil '+str(100 * percentile_in_array(data['rootsoil'], fdtimeindex))+'%')
    # axs[0].plot(plotdata.index, np.array(plotdata['qh']), color='green', marker='.', label='qh '+str(100 * percentile_in_array(data['qh'], fdtimeindex))+'%'+'  qle '+str(100*percentile_in_array(data['qle'], fdtimeindex))+'%')
    #
    # axs[0].axvline(data.index[fdtimeindex[0]], color='black', linestyle='--', alpha=0.2)
    # axs[0].axvline(data.index[fdtimeindex[1]], color='black', linestyle='--', alpha=0.2)
    # axs[0].legend(loc='upper right')
    #
    #
    # ax = axs[1]
    # ax.axvline(data.index[fdtimeindex[0]], color='black', linestyle='--', alpha=0.2)
    # ax.axvline(data.index[fdtimeindex[1]], color='black', linestyle='--', alpha=0.2)
    #
    # ax.plot(plotdata.index, np.array(plotdata['sm']), color='red', marker='.', label='sm '+str(100*percentile_in_array(data['sm'], fdtimeindex))+'%')
    # ax.plot(plotdata.index, np.array(plotdata['et']), color='black', marker='.', label='et '+str(100 * percentile_in_array(data['et'], fdtimeindex))+'%')
    # ax.plot(plotdata.index, np.array(plotdata['pet']), color='orange', marker='.', label='pet '+str(100 * percentile_in_array(data['pet'], fdtimeindex))+'%')
    # ax.plot(plotdata.index, np.array(plotdata['tem'] - 273.15), color='blue', marker='.', label='tem '+str(100 * percentile_in_array(data['tem'], fdtimeindex))+'%')
    # ax.plot(plotdata.index, np.array(plotdata['mpre']), color='green', linestyle='--', marker='.', label='mpre '+str(100 * percentile_in_array(data['mpre'], fdtimeindex))+'%')
    # ax.plot(plotdata.index, np.array(plotdata['qair']), color='blue', linestyle='--', marker='.', label='qair '+str(100 * percentile_in_array(data['qair'], fdtimeindex))+'%')
    #
    # # ax.plot(plotdata.index, np.array(plotdata['qle']), color='purple', marker='.', label='qle '+str(100*percentile_in_array(data['qle'], fdtimeindex))+'%')
    # ax.set_ylabel('factor in flash drought')
    # ax.set_xlabel('%i' % plotdata.index.year[0])
    #
    # ax.spines['top'].set_color('none')
    # axs[0].spines['bottom'].set_color('none')
    #
    # axs[0].text(axs[0].get_xlim()[0], axs[0].get_ylim()[1], areaname)
    # plt.setp(ax.get_xticklabels(), rotation=30, ha='right')
    # # fig.tight_layout()
    # plt.legend(loc='upper right')
    # # plt.show()
    # plt.savefig('%s %s.png' % (str(data.index[fdtimeindex[0]])[:10], areaname), dpi=800)
    # plt.close()

    percentile = []
    for i in ['sm', 'et', 'pet', 'tem', 'qh', 'qle', 'mpre', 'qair', 'rootsoil', 'swdown']:
        percentile.append(percentile_in_array(data[i], fdtimeindex, decline=True))
    print(percentile_in_array(data['et'], fdtimeindex, decline=True))
    return percentile



def percentile_in_array(data, spep, decline=False):
    sp = spep[0]
    ep = spep[1]
    def calc_percentile(a, method='min'):
        # 获得传入序列的百分位值
        from scipy.stats import rankdata
        import numpy as np
        if isinstance(a, list):
            a = np.asarray(a)
        s = rankdata(a, method=method) / float(len(a))
        result = [round(i, 3) for i in s]
        return result
    osesr_reshapeinyear = np.array(data).reshape([40, 73])
    etbegin = (sp + 1) % 73 - 1
    etend = (ep + 1) % 73 - 1

    if decline == False:
        meandata = np.nanmean(osesr_reshapeinyear[:, etbegin: etend + 1], axis=1)
    else:
        begin = osesr_reshapeinyear[:, etbegin]
        end = osesr_reshapeinyear[:, etend]
        meandata = end-begin
        # print(meandata.shape)
        sbegin = calc_percentile(begin)[int(sp / 73)]
        send = calc_percentile(end)[int(sp / 73)]
    percentile = calc_percentile(meandata)[int(sp / 73)]
    return [sbegin, send]

    # meandata = np.nanmean(osesr_reshapeinyear[:, etbegin: etend + 1], axis=1)[int(sp / 73)]
    # return meandata

if __name__ == '__main__':
    area_name = ['xinan', 'dongbei', 'xinjiang']
    allarea = [[26.125, 36.125, 110.125, 120.125],
               [24.125, 33.125, 100.125, 110.125],
               [40.125, 50.125, 122.125, 134.125],
               [40.125, 48.125, 80.125, 90.125]]

    for j in range(0, 3):
        todf = []
        data = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[j], index_col=0, parse_dates=True)
        sfd = data.index[(data['mask'] == 1)]
        efd = data.index[data['mask'] == 3]
        fdtime = []
        from current_function_in_fd import time_del
        for i in range(len(sfd)):
            plot_oktin_figure(data, time_del([sfd[i], efd[i]]))




    # for j in range(0, 3):
    #     todf = []
    #     data = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[j], index_col=0, parse_dates=True)
    #     sfd = data.index[(data['mask'] == 1)]
    #     efd = data.index[data['mask'] == 3]
    #     fdtime = []
    #     from current_function_in_fd import time_del
    #     for i in range(len(sfd)):
    #         todf.append(plot_variable_figure(data, time_del([sfd[i], efd[i]]), area_name[j]))
    #     from pandas import DataFrame
    #     todf = np.array(todf)
    #     indexss = pd.read_csv(r'D:\basis\fd\data\fd_time_%s.csv'%area_name[j], index_col=0, parse_dates=True)
    #     fd = DataFrame(index=indexss[indexss == 1].dropna(how='all').index)
    #     for number, i in enumerate(['sm', 'et', 'pet', 'tem', 'qh', 'qle', 'mpre', 'qair', 'rootsoil', 'swdown']):
    #         fd['begin %s'%i] = todf[:, number][:,0]
    #         fd['end %s' % i] = todf[:, number][:,1]
    #         fd['compare %s' % i] = todf[:, number][:,1] - todf[:, number][:,0]

        # spring = []
        # summer = []
        # fall = []
        # for i in fd.index:
        #     if ((i.month == 4) | (i.month == 5)):
        #         spring.append(np.array(fd.loc[i]).flatten())
        #     elif ((i.month == 6) | (i.month == 7) | (i.month == 8)):
        #         summer.append(np.array(fd.loc[i]).flatten())
        #     elif ((i.month == 9) | (i.month == 10)):
        #         fall.append(np.array(fd.loc[i]).flatten())
        # print(np.array(summer).shape)
        # fds = DataFrame()
        # fds['spring'] = np.mean(np.array(spring), axis=0)
        # fds['summer'] = np.mean(np.array(summer), axis=0)
        # fds['fall'] = np.mean(np.array(fall), axis=0)
        # fd.to_excel('%s_rocal_varibles.xlsx'%area_name[j])
        # fds.to_excel('fds %s_rocal_varibles.xlsx'%area_name[j])


    # ### 绘制all year figure时使用。##
    # sfd = data.index[(data['mask'] == 1) | (data['mask'] == 3)]
    #
    # efd = data.index[data['mask'] == 3]
    # fdtime = []
    # data['tem'] = data['tem'] - 273.15
    # data['tem'][data['tem'] < 0] = 0
    # from current_function_in_fd import time_del
    # data.groupby(data.index.year).apply(lambda x: plot_allyear_figure(x, sfd, 'all'))
