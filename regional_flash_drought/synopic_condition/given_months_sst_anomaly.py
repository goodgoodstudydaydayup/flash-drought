import numpy as np
import pandas as pd
import xarray as xr
from pandas import DataFrame

'''计算给定月份1979-2016年给定海区的海温异常'''
def find_month_and_area(choose_m, area):
    def is_amj(sstmonth, month):
        if len(month) == 3:
            if month[1] > 12:
                return (sstmonth >= month[0]) | (sstmonth <= month[1] - 12)
            return (sstmonth >= month[0]) & (sstmonth <= month[1])& (sstmonth <= month[2])
        else:
            return (sstmonth >= month[0]) & (sstmonth <= month[1])

    def select_bytime(year, month):
        if month[1] > 12:
            obs_data = sst.sel(time=slice('%i-%i' % (year, month[0]), '%i-%i' % (year + 1, month[1] - 12))).variables['sst'].mean(dim='time')
        else:
            obs_data = sst.sel(time=slice('%i-%i' % (year, month[0]), '%i-%i' % (year, month[1]))).variables[
                'sst'].mean(dim='time')

        def is_amj(sstmonth, month):
            if month[1] > 12:
                return (sstmonth >= month[0]) | (sstmonth <= month[1] - 12)
            return (sstmonth >= month[0]) & (sstmonth <= month[1])

        fano_data = sst.sel(time=is_amj(sst['time.month'], month)).variables['sst'].mean(dim='time')
        return obs_data, obs_data - fano_data

    # onset_sst, data = select_bytime(2010, [2,4])
    all_sst = xr.open_dataset(r'/data/sst.mnmean.v4.nc')
    fd = []
    for i in range(1979, 2017):
        sstin2000 = all_sst.sel(time='%i' %i)
        sstin2000 = sstin2000.sel(time=is_amj(sstin2000['time.month'],choose_m), lat=slice(area[0], area[1]),
                                  lon = slice(area[2], area[3])).variables['sst'].mean()
        sst = all_sst.sel(time=is_amj(all_sst['time.month'],choose_m), lat=slice(area[0], area[1]),
                                  lon = slice(area[2], area[3])).variables['sst'].mean()
        data = sstin2000 - sst
        fd.append(data)
    return fd
# fd = DataFrame()
# for i in range(4, 11):
#     s = find_month_and_area([i-3,i-2,i-1], [45,30,140,155])
#     fd['%i月' %i] = np.array(s).flatten()
# fd.to_excel('sea_anomaly_months_yx_spring.xlsx')

# fd = DataFrame()
# for i in range(4, 6):
#     s = find_month_and_area([i-3,i-2,i-1], [60,45,140,155])
#     fd['%i月' %i] = np.array(s).flatten()
# fd.to_excel('sea_anomaly_months_dongbei_spring.xlsx')

fd = DataFrame()
for i in range(6, 9):
    s = find_month_and_area([i-3,i-2,i-1], [50,30,230,240])
    fd['%i月' %i] = np.array(s).flatten()
fd.to_excel('sea_anomaly_months_yzr_summer.xlsx')

fd = DataFrame()
for i in range(6,9):
    s = find_month_and_area([i-3,i-2,i-1], [40,30,230,240])
    fd['%i月' %i] = np.array(s).flatten()
fd.to_excel('sea_anomaly_months_xinan_summer.xlsx')