import pandas as pd
import numpy as np
from pandas import DataFrame

yzr = pd.read_excel('yzr_rocal_varibles.xlsx', index_col=0, parse_dates=True)
xinan = pd.read_excel('xinan_rocal_varibles.xlsx', index_col=0, parse_dates=True)
dongbei = pd.read_excel('dongbei_rocal_varibles.xlsx', index_col=0, parse_dates=True)

yzr_s_wp1 = ['2001-05-06', '2014-05-01']
yzr_s_wp0 = ['1992-04-01']

yzr_sum_wp1 = ['2001-08-09', '2003-06-30', '2012-07-10']
yzr_sum_wp0 = ['2009-06-10', '2011-08-09']

xinan_s_wp1 = ['1988-05-16','2004-04-11','2014-04-01']

xinan_sum_wp1 = ['2002-08-14', '2005-08-24','2006-07-10','2012-07-20']
xinan_sum_wp0 = ['2011-08-09']

dongbei_s_wp1 = ['1979-04-21', '2002-04-26', '2006-04-21', '2007-05-21', '2012-05-01']
dongbei_s_wp0 = ['2009-04-26']

dongbei_sum_wp1 = ['2001-08-24', '2002-06-15', '2002-08-29']

yzr_n = [yzr_s_wp1, yzr_s_wp0, yzr_sum_wp1, yzr_sum_wp0]
xinan_n =  [xinan_s_wp1, xinan_sum_wp1, xinan_sum_wp0]
dongbei_n = [dongbei_s_wp1, dongbei_s_wp0, dongbei_sum_wp1]

ac = DataFrame()
for i, va in enumerate(yzr_n):
    ac['%s yzr'%i] = yzr.loc[yzr.index.isin(va)].mean()

for i, va in enumerate(xinan_n):
    ac['%s xinan'%i] = xinan.loc[xinan.index.isin(va)].mean()

for i, va in enumerate(dongbei_n):
    ac['%s dongbei'%i] = dongbei.loc[dongbei.index.isin(va)].mean()

ac.to_excel('ac.xlsx')