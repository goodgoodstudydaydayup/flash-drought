import numpy as np
import pandas as pd
from current_function_in_fd import time_del
from nc4 import read_nc4, save_nc4
import matplotlib.pyplot as plt
def plot_rocal_varibles_in_area(time,choose_varible=None):
    def plothgt(data, levels, labels):
        import cartopy.crs as ccrs
        from nc4 import read_nc4
        import plot_in_given_levels as pl

        lats = read_nc4(r'D:\basis\fd\data\china lat')
        lons = read_nc4(r'D:\basis\fd\data\china lon')
        axes = pl.plotcors(data, lons, lats, levels, clipshape=False,
                           ranges=[[55, 15, 75, 135], list(range(55, 15, -5)), list(range(75, 135, 10))],
                           labels=labels)
        return

    def plot_varible_data_in_choose_time(variblename):

        # get data in selected time
        varible = read_nc4(r'D:\basis\fd\data\china1979 %s' % variblename)
        varible[np.where(varible == -9999)] = np.nan
        if len(time) == 1:
            varibledata_in_choose_time = varible[time].reshape([144, 252])
            print(np.argwhere(np.isnan(varibledata_in_choose_time)))
        else:
            varibledata_in_choose_time = np.nanmean(varible[time[0]:time[1] + 1], axis=0)
            # show the plot
        plothgt(varibledata_in_choose_time, varible_list[variblename], variblename)
        plt.show()
        plt.savefig('%s.png' % variblename)
        plt.close()
        return

    time = time_del(time)
    varible_list = {'et': range(-5, 26, 5),
                    'pet': range(0, 161, 10),
                    'qair': np.arange(0, 0.04, 0.005),
                    'qh': range(-175, 303, 50),
                    'qle': range(-19, 300, 50),
                    'rootsoil': range(2, 924, 80),
                    'sm': range(1, 50, 4),
                    'swdown': range(5, 581, 40),
                    'tem': range(263, 308, 2),
                    'rpre': range(0, 1123, 100)}
    if choose_varible is None:
        # varible_list = {'et':[-4, 52], 'pet':[-4, 160], 'qair':[-0.01, 0.04], 'qh':[-175, 303], 'qle':[-19,300],
        #                'rootsoil':[2,924], 'sm':[1, 48], 'swdown':[5,581], 'tem':[226,317], 'rpre':[0, 1123]}

        for i in varible_list.keys():
            plot_varible_data_in_choose_time(i)
    else:
        plot_varible_data_in_choose_time(choose_varible)


def plot_rocal_varibles_in_lines(calendartime, areaname):
    import matplotlib.pyplot as plt
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    from pylab import mpl
    def percentile_in_array(data, spep):
        sp = spep[0]
        ep = spep[1]
        data = np.array(data)
        def calc_percentile(a, method='min'):
            # 获得传入序列的百分位值
            from scipy.stats import rankdata
            import numpy as np
            if isinstance(a, list):
                a = np.asarray(a)
            s = rankdata(a, method=method) / float(len(a))
            result = [round(i, 3) for i in s]
            return result
        osesr_reshapeinyear = data.reshape([40, 73])
        etbegin = (sp + 1) % 73 - 1
        etend = (ep + 1) % 73 - 1
        meandata = np.nanmean(osesr_reshapeinyear[:, etbegin: etend + 1], axis=1)
        percentile = calc_percentile(meandata)[int(sp / 73)]
        return percentile
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False

    fdtimeindex = time_del(calendartime)
    data = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% areaname, index_col=0, parse_dates=True)
    plotdata = data.iloc[fdtimeindex[0] - 5:fdtimeindex[1] + 5]

    # fig = plt.figure(figsize=(9,4))
    fig, axs = plt.subplots(2,1, sharex=True)
    fig.subplots_adjust(hspace=0)
    # Remove horizontal space between axes
    import matplotlib.dates as mdates
    days = mdates.DayLocator(bymonthday=range(1, 32, 5))
    monthsFmt = mdates.DateFormatter('%m-%d')
    axs[1].xaxis.set_major_locator(days)
    axs[1].xaxis.set_major_formatter(monthsFmt)

    axs[0].plot(plotdata.index, np.array(plotdata['qle']), color='purple', marker='.', label='qle '+str(100*percentile_in_array(data['qle'], fdtimeindex))+'%')
    axs[0].axvline(data.index[fdtimeindex[0]], color='black', linestyle='--', alpha=0.2)
    axs[0].axvline(data.index[fdtimeindex[1]], color='black', linestyle='--', alpha=0.2)
    axs[0].legend(loc='upper right')


    ax = axs[1]
    ax.axvline(data.index[fdtimeindex[0]], color='black', linestyle='--', alpha=0.2)
    ax.axvline(data.index[fdtimeindex[1]], color='black', linestyle='--', alpha=0.2)

    ax.plot(plotdata.index, np.array(plotdata['sm']), color='red', marker='.', label='sm '+str(100*percentile_in_array(data['sm'], fdtimeindex))+'%')
    ax.plot(plotdata.index, np.array(plotdata['et']), color='black', marker='.', label='et '+str(100 * percentile_in_array(data['et'], fdtimeindex))+'%')
    ax.plot(plotdata.index, np.array(plotdata['pet']), color='orange', marker='.', label='pet '+str(100 * percentile_in_array(data['pet'], fdtimeindex))+'%')
    ax.plot(plotdata.index, np.array(plotdata['tem'] - 273.15), color='blue', marker='.', label='tem '+str(100 * percentile_in_array(data['tem'], fdtimeindex))+'%')
    ax.plot(plotdata.index, np.array(plotdata['qh']), color='green', marker='.', label='qh '+str(100 * percentile_in_array(data['qh'], fdtimeindex))+'%')
    ax.plot(plotdata.index, np.array(plotdata['pre']), color='green', linestyle='--', marker='.', label='pre '+str(100 * percentile_in_array(data['pre'], fdtimeindex))+'%')
    # ax.plot(plotdata.index, np.array(plotdata['swdown']), color='yellow', linestyle='--', marker='.', label='swdown')
    # ax.plot(plotdata.index, np.array(plotdata['rootsoil']), color='red', linestyle='--', marker='.', label='soil')
    # ax.plot(plotdata.index, np.array(plotdata['qle']), color='purple', marker='.', label='qle '+str(100*percentile_in_array(data['qle'], fdtimeindex))+'%')
    ax.set_ylabel('factor in flash drought')
    ax.set_xlabel('%i' % plotdata.index.year[0])

    ax.spines['top'].set_color('none')
    axs[0].spines['bottom'].set_color('none')

    axs[0].text(axs[0].get_xlim()[0], axs[0].get_ylim()[1], areaname)
    plt.setp(ax.get_xticklabels(), rotation=30, ha='right')
    # fig.tight_layout()
    axs[1].legend(loc='upper right')
    plt.show()
    plt.savefig('%s %s.png' % (str(data.index[fdtimeindex[0]])[:10], areaname))
    plt.close()



# plot_rocal_varibles_in_area(['2002-08-14'], 'tem')
# aasdfs = plot_rocal_varibles_in_lines(['2005-08-24', '2005-09-13'], 'xinan')

#
# a = pd.read_csv(r'D:\basis\fd\data\M_Oce_Er.txt', delim_whitespace=True)
# print(a)

# a = pd.read_csv(r'D:\basis\fd\data\datavarible1979 xinan.csv', index_col=0, parse_dates=True)
# c = a[    (((a.index.month == 5)) & (a.index.day>15))
#       | ((a.index.month == 6) & (a.index.day<6))]['orses']
# print(c)
# c.groupby(c.index.year).mean().to_excel('ssdd.xlsx')

def mean_in_season(area_name):
    '''计算闪旱的局地数据百分数的季节平均'''
    a = pd.read_excel(r'D:\basis\fd\data\%s_rocal_varibles_in_percentile.xlsx'%area_name, index_col=0, parse_dates=True)
    spring = a[(a.index.month == 4)| (a.index.month == 5)].mean()
    print(spring)
    summer = a[(a.index.month == 6)| (a.index.month == 7)| (a.index.month == 8)].mean()
    print(summer)
    fall = a[(a.index.month == 9)| (a.index.month == 10)].mean()
    print(fall)

mean_in_season('dongbei')