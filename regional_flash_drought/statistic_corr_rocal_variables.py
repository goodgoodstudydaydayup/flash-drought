import numpy as np
import pandas as pd
from pandas import DataFrame
import matplotlib

from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
'''try to find relationship between two varibles in fd'''
# data = pd.read_csv(r'D:\basis\fd\data\datavarible1979 yzr.csv', index_col=0, parse_dates=True)
# sfd = data[(data['mask'] > 0)]
#
# corr = []
# corrname = []
# for i in ['sm', 'et', 'pet', 'tem', 'qh', 'qle', 'mpre', 'qair', 'rootsoil', 'swdown']:
#     for j in ['sm', 'et', 'pet', 'tem', 'qh', 'qle', 'mpre', 'qair', 'rootsoil', 'swdown']:
#         if i == j:
#             continue
#         else:
#             corrname.append([i,j])
#             corr.append(sfd[[i, j]].corr()[j].iloc[0])
# corr = np.array(corr).flatten()
# df = DataFrame()
# df['corr'] = corr
# df['corrname'] = corrname
# df.to_excel('corr.xlsx')

'''find correlation in positive et and negative et'''
def percentile_in_array(data, spep):
    # sp = spep[0]-5
    # ep = spep[1]+4
    sp = spep[0]
    ep = spep[1]
    def calc_percentile(a, method='min'):
        # 获得传入序列的百分位值
        from scipy.stats import rankdata
        import numpy as np
        if isinstance(a, list):
            a = np.asarray(a)
        s = rankdata(a, method=method) / float(len(a))
        result = [round(i, 3) for i in s]
        return result
    osesr_reshapeinyear = np.array(data).reshape([19, 73])
    etbegin = (sp + 1) % 73 - 1
    etend = (ep + 1) % 73 - 1

    percentile =[]
    for i in range(etbegin, etend+1):
        begin = osesr_reshapeinyear[:, i]
        sbegin = calc_percentile(begin)[int(sp / 73)]
        percentile.append(sbegin)

    return np.array(percentile).flatten()


def corr_mean_tem_in_given_months(area, months):
    def corr(sfd, name):
        corr = []
        corrname = []
        for i in ['sm', 'et', 'pet', 'tem', 'qair', 'wind', 'qh', 'qle', 'mpre', 'rootsoil', 'swdown', 'lw', 'sw']:
            for j in ['sm', 'et', 'pet', 'tem', 'qair', 'wind', 'qh', 'qle', 'mpre', 'rootsoil', 'swdown', 'lw', 'sw']:
                if i == j:
                    continue
                else:
                    corrname.append([i, j])
                    corr.append(sfd[[i, j]].corr()[j].iloc[0])
        corr = np.array(corr).flatten()
        df['corr %s' % name] = corr
        df['corrname %s' % name] = corrname

    def mean(sfd, name):
        mean = []
        max = []
        min = []
        maxsubtract_min = []
        std = []
        median = []
        skew = []
        corrname = []
        for i in ['sm', 'et', 'pet', 'tem', 'qair', 'wind', 'qh', 'qle', 'mpre', 'rootsoil', 'swdown', 'lw', 'sw']:
            corrname.append(i)
            mean.append(sfd[i].mean())
            max.append(sfd[i].max())
            min.append(sfd[i].min())
            std.append(sfd[i].std())
            median.append(sfd[i].median())
            skew.append(sfd[i].skew())
        df2['mean %s' % name] = np.array(mean).flatten()
        df2['max %s' % name] = np.array(max).flatten()
        df2['min %s' % name] = np.array(min).flatten()
        df2['std %s' % name] = np.array(std).flatten()
        df2['median %s' % name] = np.array(median).flatten()
        df2['skew %s' % name] = np.array(skew).flatten()
        df2['meanname %s' % name] = corrname

    from current_function_in_fd import time_del
    data = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv' % area, index_col=0, parse_dates=True)
    all_sfd = data.index[data['mask'] == 1]
    all_efd = data.index[data['mask'] == 3]

    sfd = data.index[(data['mask'] == 1) &
                     (data.index.month.isin(months)
                      )]

    efd = time_del(all_efd[all_sfd.isin(sfd)])
    sfd = time_del(sfd)
    # print(sfd, efd)
    percentile_data = data.copy()
    negativeet = []
    positiveet = []
    allet = []
    for i, position in enumerate(sfd):
        per_et = percentile_in_array(data['tem'], [position, efd[i]])
        for j in ['sm', 'et', 'pet', 'tem', 'qair', 'wind', 'qh', 'qle', 'mpre', 'rootsoil', 'swdown', 'lw', 'sw']:
            percentile_data[j].iloc[position:efd[i] + 1] = percentile_in_array(data[j], [position, efd[i]])
        allet.append(np.arange(sfd[i], efd[i] + 1))
        # if ((per_et[-1] < 0.5)&(per_et[0] < 0.5)):
        if ((np.mean(per_et) < 0.5)):

            # if ((per_et[-1] - per_et[0] < 0)):
            negativeet.append(np.arange(sfd[i], efd[i] + 1))
        else:
            positiveet.append(list(range(sfd[i], efd[i] + 1)))

    new_all = []
    for i in allet:
        for j in i:
            new_all.append(j)

    new_nege = []
    for i in negativeet:
        for j in i:
            new_nege.append(j)
    new_posi = []
    for i in positiveet:
        for j in i:
            new_posi.append(j)

    df = DataFrame()
    corr(data.iloc[new_all], 'all_corr')

    corr(data.iloc[new_nege], 'nega_corr')
    corr(data.iloc[new_posi], 'posi_corr')

    corr(percentile_data.iloc[new_all], 'pall_corr')
    corr(percentile_data.iloc[new_nege], 'pnega_corr')
    corr(percentile_data.iloc[new_posi], 'pposi_corr')

    # df.to_excel('all_et_beginlessend.xlsx')
    # df.to_excel('all_et_05.xlsx')

    def save_tem(new_all):
        df = DataFrame()
        df['tem1'] = data['et'].iloc[new_all]
        df['tem2'] = percentile_data['et'].iloc[new_all]
        df['mask'] = data['mask'].iloc[new_all]
        df['pet1'] = data['pet'].iloc[new_all]
        df['pet2'] = percentile_data['pet'].iloc[new_all]
        df['sm1'] = data['sm'].iloc[new_all]
        df['sm2'] = percentile_data['sm'].iloc[new_all]
        df['qair1'] = data['qair'].iloc[new_all]
        df['qair2'] = percentile_data['qair'].iloc[new_all]
        df['wind1'] = data['wind'].iloc[new_all]
        df['wind2'] = percentile_data['wind'].iloc[new_all]
        return df

    df2 = DataFrame()
    mean(data.iloc[new_all], 'all_corr')
    mean(data.iloc[new_nege], 'nega_corr')
    mean(data.iloc[new_posi], 'posi_corr')

    mean(percentile_data.iloc[new_all], 'pall_corr')
    mean(percentile_data.iloc[new_nege], 'pnega_corr')
    mean(percentile_data.iloc[new_posi], 'pposi_corr')
    # return [df, df2, save_tem(new_nege), save_tem(new_posi)]
    return percentile_data.iloc[new_all]

def plot_single_fd(ax, data, labels):
    x = np.arange(11)
    colors = ['black','forestgreen', 'darkgreen', 'r', 'lightcoral', 'navy', 'mediumpurple', 'indigo',
              'royalblue', 'dimgray', 'darkorange']
    # ax.hist(x, data, histtype='bar', color=colors, label=['sm', 'et', 'pet', 'tem', 'q', 'win', 'qh', 'qle', 'pre', 'rs', 'sd'])
    ax.bar(x, data, color=colors)
    ax.axhline(0.5, alpha=0.5, color='black', linestyle='--')
    ax.set_xticks(x)
    ax.set_yticks([0,0.5,1])
    ax.set_xticklabels(['sm', 'et', 'pet', 'tem', 'q', 'win', 'qh', 'qle', 'pre', 'rs', 'sd'])


'''用于有yzr得地区绘图'''
# months = [[4, 5], [6, 7, 8], [9, 10], range(4, 11)]
# mnames = ['spring', 'summer', 'fall', 'all']
# areaname = ['yzr', 'xinan', 'dongbei']
# newname = ['Yzr', 'Xinan', 'Dongbei']
# colors = ['black','forestgreen', 'darkgreen', 'r', 'lightcoral', 'navy', 'mediumpurple', 'indigo',
#               'royalblue', 'dimgray', 'darkorange']
# import matplotlib.pyplot as plt
# fig, ax = plt.subplots(3,3,sharey='row',figsize=[9.2, 5])
# fig.subplots_adjust(bottom=0.8, top=0.98,
#                     left=0.02, right=1, wspace=0, hspace=0)
# import numpy as np
# for sarea in range(3):
#     for i in range(3):
#         df = corr_mean_tem_in_given_months(areaname[sarea], months[i])
#         df.columns = ['mask', 'ET', 'PET', 'rses', 'percentile_rses', 'orses',
#        'percentile_orses', 'percentile25', 'SM', 'tem', 'QH', 'QLE', 'pre',
#        'RS', 'SD', 'SH', 'WIN']
#         if sarea*3+i+1 == 9:
#             sax = ax[sarea][i]
#             sax.get_yaxis().set_visible(False)  # 不显示y轴
#             sax.get_xaxis().set_visible(False)
#             sax.spines['top'].set_visible(False)  # 去掉上边框
#             sax.spines['bottom'].set_visible(False)  # 去掉下边框
#             sax.spines['left'].set_visible(False)  # 去掉左边框
#             sax.spines['right'].set_visible(False)  # 去掉右边框
#
#         else:
#             sax = ax[sarea][i]
#             bp_dict = df.drop(['mask', 'rses', 'percentile_rses', 'orses',
#            'percentile_orses', 'percentile25','QH', 'QLE', 'RS'], axis=1).boxplot(ax=sax,patch_artist = True)
#             # bp_dict.findobj(matplotlib.patches.Patch)[10].set_facecolor(colors[10])
#             for j in range(11):
#                 bp_dict.findobj(matplotlib.patches.Patch)[j].set_facecolor(colors[j])
#
#             sax.text(sax.get_xlim()[0], sax.get_ylim()[1], newname[sarea]+' '+mnames[i])
#         # print(df)
#         # dfmean = np.array(df[1]['mean pall_corr'])
#         # if sarea*3+i+1 == 9:
#         #     break
#         # else:
#         #     plot_single_fd(fig.add_subplot(3,3, sarea*3+i+1), dfmean, areaname[sarea]+' '+mnames[i])
# plt.show()


# for area in areaname:
#     with pd.ExcelWriter('win%s alltem.xlsx' % area) as writer:
#         for i in range(2):
#             df = corr_mean_tem_in_given_months(area, months[i])
#             df[2].to_excel(writer, sheet_name='%s nege' % mnames[i])
#             df[3].to_excel(writer, sheet_name='%s posi' % mnames[i])
#             df[0].to_excel(writer, sheet_name='%s corr' % mnames[i])
#             df[1].to_excel(writer, sheet_name='%s mean' % mnames[i])
#     with pd.ExcelWriter('win%s alltem2.xlsx' % area) as writer:
#         for i in range(2, 4):
#             df = corr_mean_tem_in_given_months(area, months[i])
#             df[2].to_excel(writer, sheet_name='%s nege' % mnames[i])
#             df[3].to_excel(writer, sheet_name='%s posi' % mnames[i])
#             df[0].to_excel(writer, sheet_name='%s corr' % mnames[i])
#             df[1].to_excel(writer, sheet_name='%s mean' % mnames[i])



'''no yZR'''
months = [[4, 5], [6, 7, 8], [9, 10], range(4, 11)]
mnames = ['春季', '夏季', '秋季']
areaname = ['xinan', 'dongbei','xinjiang']
newname = ['西南', '东北', '西北']
numbers = ['a)','b)','c)','d)','e)','f)','g)','h)','i)','g)','k)','l)','m)','n)','o)','p)','q)','r)','s)','t)','u)','v)',
          'w)','x)', 'y)', 'z)', 'aa)', 'ab)', 'ac)','ad)','ae)','af)', 'ag)', 'ah)', 'ai)', 'ag)', 'ak)',
          'al)', 'am)', 'an)', 'ao)','ap)']


colors = ['black','forestgreen', 'darkgreen', 'r', 'lightcoral', 'navy', 'mediumpurple', 'indigo',
              'royalblue', 'dimgray', 'darkorange', 'red', 'tab:blue']
# ['ET', 'PET','SM', 'TEM', 'QH', 'QLE', 'PRCP',
#        'RS', 'SWD', 'SH', 'WIN','LW', 'SW']
colors = ['black','forestgreen', 'dodgerblue','saddlebrown', 'lightgreen', 'lightgreen', 'dodgerblue',
          'dodgerblue', 'tab:red', 'saddlebrown', 'saddlebrown', 'tab:red', 'tab:red']
import matplotlib.pyplot as plt
fig, ax = plt.subplots(3,3,sharey='row',figsize=[10, 7])
fig.subplots_adjust(top=0.93,
bottom=0.085,
left=0.05,
right=0.985,
hspace=0.3,
wspace=0.125)
import numpy as np
aaaa =[]
for sarea in range(3):
    for i in range(3):
        df = corr_mean_tem_in_given_months(areaname[sarea], months[i])
        aaaa.append(np.array(df.drop(['mask', 'rses', 'percentile_rses', 'orses',
       'percentile_orses', 'percentile25'], axis=1).median(axis=0)))
        df.columns = ['mask', 'ET', 'PET', 'rses', 'percentile_rses', 'orses',
       'percentile_orses', 'percentile25', 'SM', 'TEM', 'QH', 'QLE', 'PRCP',
       'RS', 'SWD', 'SH', 'WIN','LW', 'SW']

        sax = ax[sarea][i]

        bp_dict = df.drop(['mask', 'rses', 'percentile_rses', 'orses',
       'percentile_orses', 'percentile25'], axis=1).boxplot(ax=sax,patch_artist = True)
        # bp_dict.findobj(matplotlib.patches.Patch)[10].set_facecolor(colors[10])
        for j in range(13):
            bp_dict.findobj(matplotlib.patches.Patch)[j].set_facecolor(colors[j])

        count = sarea*3 +i
        sax.text(sax.get_xlim()[0], sax.get_ylim()[1], numbers[count]+newname[sarea]+' '+mnames[i])
        # print(df)
        # dfmean = np.array(df[1]['mean pall_corr'])
        # if sarea*3+i+1 == 9:
        #     break
        # else:
        #     plot_single_fd(fig.add_subplot(3,3, sarea*3+i+1), dfmean, areaname[sarea]+' '+mnames[i])


plt.show()
plt.savefig('figure6.png', dpi=800)