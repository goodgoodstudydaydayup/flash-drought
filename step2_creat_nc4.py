import numpy as np


def change_to_nc4(lon, lat, time, data, savtname):
    # 传入lon, lat, time, data以及储存时候使用的名字即可。
    import netCDF4 as nc4

    f = nc4.Dataset('%s.nc4' % savtname, 'w', format='NETCDF4')  # 'w' stands for write
    tempgrp = f.createGroup('data')

    tempgrp.createDimension('lon', len(lon))
    tempgrp.createDimension('lat', len(lat))
    tempgrp.createDimension('time', len(time))

    longitude = tempgrp.createVariable('Longitude', 'f4', 'lon')
    latitude = tempgrp.createVariable('Latitude', 'f4', 'lat')
    levels = tempgrp.createVariable('Time', 'i4', 'time')
    mask = tempgrp.createVariable('data', 'f4', ('time', 'lat', 'lon'))

    longitude[:] = lon  # The "[:]" at the end of the variable instance is necessary
    latitude[:] = lat
    levels[:] = time
    print(data.shape)
    mask[:, :, :] = data
    f.close()
    return


# 对于分了区的
# for i in range(4):
#     for j in range(6):
#         lat_partition = [312, 340, 380, 420, 456]
#         lon_partition = [1008, 1060, 1100, 1140, 1180, 1220, 1260]
#         alat = np.loadtxt('lat.csv')
#         alon = np.loadtxt('lon.csv')
#         lat = alat[lat_partition[i]:lat_partition[i+1]]
#         lon = alon[lon_partition[j]:lon_partition[j+1]]
#         a = np.loadtxt(r'shape pevp %i %i.txt' %(i, j))
#         mask_data = np.loadtxt('evp %i %i.txt' %(i, j)).reshape([int(i) for i in a])
#         time = np.arange(mask_data.shape[0])
#         change_to_nc4(lon, lat, time, mask_data, 'evp %i %i' %(i, j))


def select_by_year(varible):
    # 对于分年份的
    for i in range(1961, 2019):
        a = np.loadtxt(r'shape %i.txt' % i)
        mask_data = np.loadtxt('%s %i.txt' % (varible, i)).reshape([int(i) for i in a])
        save_nc4(mask_data, '%s %i' % (varible, i))




from nc4 import read_nc4, save_nc4
#
# for i in ['qh', 'qle', 'sm']:
#     select_by_year(i)

def combine_year():
    def del_china(varible):
        lat_partition = [312, 456]
        lon_partition = [1008, 1260]
        alat = np.loadtxt('lat.csv')
        alon = np.loadtxt('lon.csv')
        lat = alat[lat_partition[0]:lat_partition[1]]
        lon = alon[lon_partition[0]:lon_partition[1]]
        for i in range(1961, 2019):
            mask_data = read_nc4('%s %i' % (varible, i))
            mask_data = mask_data[:, 312:456, 1008:1260]
            save_nc4(mask_data, 'trans %s %i' % (varible, i))

    def ues(varible):
        data = read_nc4('trans %s 1961' % (varible))
        for year in range(1962, 2019):
            a = read_nc4('trans %s %i' % (varible, year))
            # 本处73为限定了长度必须为73
            data = np.vstack((data, a[:73, :, :]))
        save_nc4(data, 'china %s' % varible)

    for i in ['qh', 'qle', 'sm']:
        del_china(i)
        ues(i)


combine_year()
