import numpy as np
import pandas as pd
import xarray as xr
from nc4 import read_nc4, save_nc4
from scipy.stats import pearsonr
from scipy.stats import skew
from plot_new_method import plot_single_ax
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

def main(choose, showorsave):
    if choose == 'hgt':
        number = ['a)','b)','c)','d)','e)','f)','g)','h)', '','i)','g)','k)','l)','m)','n)',
                  'o)','p)','q)','r)','s)','t)','u)','v)','w)','x)']

        # '''读取环流数据'''
        def get_data(areaname, varible_level, savenamemonth, space, area, labels, axs):
            data = read_nc4(r'D:\basis\fd\sdfdss\%s_%s_%s' % (areaname, varible_level, savenamemonth))[:,1,:,:]
            lats = read_nc4(r'D:\basis\fd\data\noaa lat')
            lons = read_nc4(r'D:\basis\fd\data\noaa lon')
            ax = fig.add_subplot(2, 3, axs, projection=ccrs.PlateCarree())
            from scipy.stats import zscore

            if axs % 3 == 0:
                quiverkey = True
            else:
                quiverkey = False

            if varible_level == 'hgt200':
                fill = plot_single_ax(ax,
                                      [
                                          [data[0], 'contour', 'hgt', space],
                                          [data[1], 'contourf', list(range(-190,210,10))],
                                          [data[2], 'quiver1', list(np.arange(0, 1.1, 0.1))]
                                      ]
                                      , lons, lats, labels=number[axs-1]+' '+labels + ' %s %s' % (varible_level, savenamemonth),
                                      ranges=[[60, 0, 60, 140], [60,40, 20,0], [80, 120]])
            else:
                fill = plot_single_ax(ax,
                                      [
                                          [data[0], 'contour', 'hgt', space],
                                          [data[1], 'contourf', list(range(-190,210,10))],
                                          [[data[2], data[3], [1, 2]], 'quiver2', list(np.arange(0, 1.1, 0.1))]
                                      ]
                                      , lons, lats, labels=number[axs-1]+' '+labels + ' %s %s' % (varible_level, savenamemonth),
                                      ranges=[[60, 0, 60, 140], [60,40, 20,0], [70,90,110,130]], quiverkey=quiverkey)

            x1 = area[0]
            x2 = area[1]
            y1 = area[2]
            y2 = area[3]
            area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
            ax.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color='red')

            if axs%3 !=1:
                ax.get_yaxis().set_visible(False)  # 不显示y轴
            if (axs < 4):
                ax.get_xaxis().set_visible(False)
            # plt.show()
            return fill

#         fig = plt.figure(figsize=[9,9.2])
#         fig.subplots_adjust(top=0.97,
# bottom=0.045,
# left=0.055,
# right=0.95,
# hspace=0.1,
# wspace=0.075)
        fig = plt.figure(figsize=[9,4.5])
        fig.subplots_adjust(top=0.97,
        bottom=0.4,
        left=0.055,
        right=0.95,
        hspace=0.1,
        wspace=0.075)
        area_name = ['xinan', 'dongbei', 'xinjiang']
        allarea = [[24.125, 33.125, 102.125, 108.125],
                   [40.125, 50.125, 122.125, 128.125],
                   [36.125, 47.125, 87.125, 98.125]]

        varible_levels = ['hgt200', 'hgt500']
        savename = ['AM', 'JJA', 'SO']


        axs = 0
        new_name = ['西南', '东北', '西北']
        spaces = [10, 5, 2]
        for i in range(2,3):
            for j in range(2):
                for s in savename:
                    axs = axs+1
                    fill = get_data(area_name[i], varible_levels[j], s, axs=axs, labels=new_name[i], space=spaces[j], area=allarea[i])


        cb_ax = fig.add_axes([0.1, 0.06, 0.8, 0.02])
        # cb_ax = fig.add_axes([0.94, 0.1, 0.02, 0.8])
        cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
        cb_ax.set_title('hPa')


        if showorsave == 'show':
            plt.show()
        if showorsave == 'save':
            plt.savefig('figure7.png', dpi=800)


    if choose == 'sst':
        '''读取SST，以及SLP'''
        def get_data(areaname, varible_level, savenamemonth, space, area, labels, axs, season):
            data = read_nc4(r'D:\basis\fd\sdfdss\%s_%s_%s' %
                            (areaname, varible_level, savenamemonth))[:,1,:,:]
            print(data.shape)
            if varible_level == 'slp':
                da = xr.open_dataset(r'D:\basis\fd\data\slp.mon.mean.nc')
            else:
                da = xr.open_dataset(r'D:\basis\fd\data\sst.mnmean.v4.nc')
            lats = da['lat'].values
            lons = da['lon'].values
            print(labels, axs, varible_level, savenamemonth)

            ax = fig.add_subplot(6,3, axs, projection=ccrs.PlateCarree(central_longitude=180))
            from scipy.stats import zscore
            data1 = zscore(np.ma.masked_invalid(data[1]))
            fill = plot_single_ax(ax,
                                  [
                                      [data[0], 'contour', 'hgt', space],
                                      [data1, 'contourf', np.arange(-2.3,3.1,0.5)]
                                  ]
                                  , lons, lats, labels=number[axs-1]+' '+labels + ' %s %s' % (varible_level.upper(), season),
                                  ranges=[[80, 0, 100, 280], [ 60, 20], [140,190,240]])

            x1 = area[0]
            x2 = area[1]
            y1 = area[2]
            y2 = area[3]
            area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
            ax.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color='red')

            if axs%3 !=1:
                ax.get_yaxis().set_visible(False)  # 不显示y轴
            # if (axs != 6) & (axs < 10):
            #     ax.get_xaxis().set_visible(False)
            if axs <16:
                ax.get_xaxis().set_visible(False)

            # plt.show()
            return fill

        fig = plt.figure(figsize=[9,10])
        fig.subplots_adjust(top=0.99,
        bottom=0.1,
        left=0.055,
        right=0.99,
        hspace=0.05,
        wspace=0.05)

        area_name = ['xina', 'dong', 'xinj']
        allarea = [[24.125, 33.125, 102.125, 108.125],
                   [40.125, 50.125, 122.125, 128.125],
                   [36.125, 47.125, 87.125, 98.125]]

        varible_levels = ['sst', 'slp']
        savename = ['AM', 'JJA', 'SO']
        seasons = ['春季', '夏季', '秋季']
        number = ['a)','b)','c)','d)','e)','f)','g)','h)','i)','g)','k)','l)','m)','n)','o)','p)','q)','r)','s)','t)','u)','v)','w)','x)']

        axs = 0
        new_name = ['西南', '东北', '西北']
        spaces = [10, 5, 2]
        for i in range(3):
            for j in range(2):
                for s in range(3):
                    axs = axs+1
                    fill = get_data(area_name[i], varible_levels[j], savename[s], axs=axs, labels=new_name[i],
                                    space=spaces[j],
                                    area=allarea[i], season=seasons[s])

        cb_ax = fig.add_axes([0.1, 0.06, 0.8, 0.01])
        # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
        cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
        if showorsave == 'show':
            plt.show()
        if showorsave == 'save':
            plt.savefig('figure8.png', dpi=800)

    if choose == 'cor':
        '''读取SST，以及SLP'''
        def get_data(name, labels, axs):
            data = read_nc4(r'D:\basis\fd\sdfdss\%s'%name)

            da = xr.open_dataset(r'D:\basis\data\sst.mnmean.nc')
            lats = da['lat'].values
            lons = da['lon'].values

            data1 = np.ma.masked_where((data<0.27)&(data>-0.27), data)
            ax = fig.add_subplot(3,2, axs, projection=ccrs.PlateCarree(central_longitude=180))
            fill = plot_single_ax(ax,
                                  [[data, 'contour', [-0.27,-0.1,0,0.1,0.27]],
                                      [data1, 'contourf', [-0.7,-0.6,-0.5,-0.4,-0.3,-0.27,0.27,0.3,0.4,0.5,0.6,0.7]]
                                  ]
                                  , lons, lats, labels=labels,
                                  ranges=[[89.5, -89.5, 0.5, 359.5], [50,0,-50], [50,170,290]])

            if axs%2 !=1:
                ax.get_yaxis().set_visible(False)  # 不显示y轴
            if axs <5:
                ax.get_xaxis().set_visible(False)
            # plt.show()
            return fill

        fig = plt.figure(figsize=[8,7])
        fig.subplots_adjust(top=0.98,
        bottom=0.13,
        left=0.06,
        right=0.99,
        hspace=0.115,
        wspace=0.05)


        area_name = ['xina', 'dong', 'xinj']
        allarea = [[24.125, 33.125, 102.125, 108.125],
                   [40.125, 50.125, 122.125, 128.125],
                   [36.125, 47.125, 87.125, 98.125]]

        area_name = ['xin', 'don']
        varible_levels = ['sst', 'slp']
        savename = ['AM', 'JJA', 'SO']
        axs = 0
        names = ['xinandongbei0', 'xinandongbei1', 'xinjiang','spring1', 'summer1', 'fall0']
        newlist = ['a) 西南', 'b) 东北','c) 西北', 'd) 春季', 'e) 夏季', 'f) 秋季']
        for i in range(6):
            axs = axs+1
            fill = get_data(names[i], axs=axs, labels=newlist[i])

        cb_ax = fig.add_axes([0.1, 0.07, 0.8, 0.02])
        # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
        cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
        cbar.set_ticks([-0.7,-0.6,-0.5,-0.4,-0.3,-0.27,0.27,0.3,0.4,0.5,0.6,0.7])
        cbar.set_ticklabels([-0.7,-0.6,-0.5,-0.4,-0.3,-0.27,0.27,0.3,0.4,0.5,0.6,0.7])
        if showorsave == 'show':
            plt.show()
        if showorsave == 'save':
            plt.savefig('figure9.png', dpi=800)

main('sst', 'show')
# main('hgt','show')
