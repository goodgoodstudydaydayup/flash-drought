import numpy as np
from nc4 import save_nc4, read_nc4


def choose_month(sp, ep, mask):
    # 对于mask进行选择对应月份。
    def find_allsp_allep():
        allsp = list(range(sp, 0, -73))
        allsp.extend(list(range(sp + 73, len(mask) + 1, 73)))
        allep = list(range(ep, 0, -73))
        allep.extend(list(range(ep + 73, len(mask) + 1, 73)))
        all = []
        for i in range(len(allsp)):
            all.extend(list(range(allsp[i], allep[i] + 1)))
        all = np.array(all).flatten()
        return np.sort(all)

    def choose(smask):
        return smask[all]

    mask_4_10 = np.zeros([2552, mask.shape[1], mask.shape[2]])
    all = find_allsp_allep()
    for ilat in range(len(mask[0, :, 0])):
        for ilon in range(len(mask[0, 0, :])):
            mask_4_10[:, ilat, ilon] = choose(mask[:, ilat, ilon])
    return mask_4_10


def fd_frequency(mask, pointshape, freq='year'):
    # 对频率进行计算，，可分为年或者全序列, 仅针对年份为58， 且
    def point_frequency(point_mask, freq='year'):
        if freq != 'year':
            fd_data_number = len(np.argwhere(point_mask[:] > 0).flatten())
            point_freq = fd_data_number / (pointshape[0] * pointshape[1])
        else:
            point_mask = point_mask.reshape(pointshape[0] * pointshape[1])
            point_freq = np.zeros(pointshape[0])
            for i in range(pointshape[0]):
                point_freq[i] = len(np.argwhere(point_mask[i, :] > 0).flatten()) / pointshape[1]
        return point_freq

    if freq == 'year':
        fd_freq = np.zeros([pointshape[0], mask.shape[1], mask.shape[2]])
        for ilat in range(len(mask[0, :, 0])):
            for ilon in range(len(mask[0, 0, :])):
                fd_freq[:, ilat, ilon] = point_frequency(mask[:, ilat, ilon].flatten(), freq=freq)
    else:
        fd_freq = np.zeros([mask.shape[1], mask.shape[2]])
        for ilat in range(len(mask[0, :, 0])):
            for ilon in range(len(mask[0, 0, :])):
                fd_freq[ilat, ilon] = point_frequency(mask[:, ilat, ilon].flatten(), freq=freq)
    return fd_freq


# 计算一下频率，可分为年或者全序列
# for i in range(4):
#     for j in range(6):
#         mask = read_nc4('mask %i %i' % (i, j))
#         mask = choose_month(19, 62, mask)
#         fd_freq = fd_frequency(mask, freq='all')
#         save_nc4(fd_freq, 'fd_freq %i %i' % (i, j))


# mask = read_nc4('mask')
# save_nc4(choose_month(19, 62, mask), 'mask410')


def fd_day_index(mask, freq):
    # 在给定的时间范围内，只要出现了fd，则该点变为1
    smask = []
    for i in range(0, int(mask.shape[0]) - freq, freq):
        ssmask = np.zeros(mask.shape[1:3])
        for j in range(freq):
            ssmask[np.where(mask[i + j] > 0)] = 1
        smask.append(ssmask)
    return np.array(smask)


def select_area(mask):
    def cal_select_area(mask_all):
        from scipy.ndimage import generic_filter
        cal_select_zoom = np.zeros(mask_all.shape)
        ''''# 20 和 15 都可以调整！！！！请参考着使用！！！'''
        images = generic_filter(mask_all, np.sum, size=10)
        cal_select_zoom[np.where(images > 7)] = 1
        return cal_select_zoom

    all_cal_select = np.zeros(mask[0].shape)
    for i in range(mask.shape[0]):
        cal_select_zoom = cal_select_area(mask[i])
        all_cal_select = all_cal_select + cal_select_zoom
    return all_cal_select


def select_single_to_find_size_and_images(smask):
    def cal_select_area(mask_all):
        from scipy.ndimage import generic_filter
        cal_select_zoom = np.zeros(mask_all.shape)
        ''''# 20 和 15 都可以调整！！！！请参考着使用！！！'''
        images = generic_filter(mask_all, np.sum, size=10)
        cal_select_zoom[np.where(images > 7)] = 1
        return cal_select_zoom

    cal = cal_select_area(smask)
    return cal


def plot_select_area(data, labels=' '):
    # 开始绘图
    import plot as pl
    import matplotlib.pyplot as plt
    import cartopy.crs as ccrs
    ax = plt.subplot(111, projection=ccrs.PlateCarree())
    pl.plotcors(ax, data, slon, slat,
                ranges=[[18.125, 53.125, 72.125, 134.125], [15, 25, 35, 45, 55], [70, 90, 110, 130]], labels=labels)


# 不动这一块，联系到了函数

def
slat = read_nc4('china lat')
slon = read_nc4('china lon')

freq = 42

mask = read_nc4('mask410')
from current_function_in_fd import china_del, time_del

area_name = ['华中', '西南', '东北', '新疆']
allarea = [[26.125, 36.125, 110.125, 120.125],
           [24.125, 33.125, 100.125, 110.125],
           [40.125, 50.125, 122.125, 134.125],
           [40.125, 48.125, 80.125, 90.125]]
index = china_del(allarea[2])
fdtime = ['2001-05-26', '2001-08-09']
timeindex = time_del(fdtime, month410=True)
slat = slat[index[0]:index[1]]
slon = slon[index[2]:index[3]]

mask = mask[timeindex[0]:timeindex[1], index[0]:index[1], index[2]:index[3]]
mask[np.where(mask > 0)] = 1
# mask = fd_frequency(mask, pointshape=[58,44], freq='all')
for i in range(mask.shape[0]):
    plot_select_area(mask[i], '区域闪旱期间格点闪旱信息 %s %i' % (fdtime[0] + 'to' + fdtime[1], i))
    import matplotlib.pyplot as plt
    # plt.show()
    plt.savefig('年百分率 %i.png' % i)
    plt.close()

# mask = read_nc4('58年分年百分率')
# print(mask.shape)
# year = np.arange(1961, 2019)
# for i in range(58):
#     plot_select_area(mask[i])
#     import matplotlib.pyplot as plt
#     # plt.show()
#     plt.savefig('年百分率 %i.png' %year[i])
#     plt.close()
