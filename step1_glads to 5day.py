# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from netCDF4 import Dataset
from pandas import DataFrame

def tran_day(name, year):
    sm = []
    evp = []
    pevp = []
    qh = []
    qle = []
    # 将3h的转为日值（平均处理）
    for i in name:
        ncin = Dataset(r'/data1/GLDAS/%i/%s' %(year, i), 'r')
        sm.append(ncin.variables['SoilMoi0_10cm_inst'][0])
        evp.append(ncin.variables['Evap_tavg'][0])
        pevp.append(ncin.variables['PotEvap_tavg'][0])
        qh.append(ncin.variables['Qh_tavg'][0])
        qle.append(ncin.variables['Qle_tavg'][0])
        ncin.close()
    day = []
    day.append(np.mean(sm, axis=0))
    day.append(np.mean(evp, axis=0))
    day.append(np.mean(pevp, axis=0))
    day.append(np.mean(qh, axis=0))
    day.append(np.mean(qle, axis=0))
    return day

### 从数据分布出发，按照年份，提出每日数据，放入csv文件中，以年份命名。
def change_period(year):
    address_txt = pd.read_csv(r'/data1/GLDAS/SourceFile/2.0/%i.txt' % (year), header=None)
    # address_txt = pd.read_csv(r'D:\basis\fd\GLDAS\%i.txt' % (year), header=None)
    # address_txt = pd.read_csv(r'%i.txt' % (year), header=None)
    sm = []
    evp = []
    pevp = []
    qh = []
    qle = []
    for i in range(0, len(address_txt), 40):
        five_day_add = []
        if i <= len(address_txt-40):
            for j in range(0,40):
                    five_day_add.append(address_txt.iloc[i+j][0][-39:])

        day = tran_day(five_day_add, year)
        sm.append(day[0])
        evp.append(day[1])
        pevp.append(day[2])
        qh.append(day[3])
        qle.append(day[4])
    np.savetxt('shape %i.txt' % year, np.array(sm).shape)
    np.savetxt('sm %i.txt'%year, np.array(sm).flatten())
    np.savetxt('evp %i.txt' % year, np.array(evp).flatten())
    np.savetxt('pevp %i.txt' % year, np.array(pevp).flatten())
    np.savetxt('qh %i.txt' % year, np.array(qh).flatten())
    np.savetxt('qle %i.txt' % year, np.array(qle).flatten())
    return

# 切割中国的维度，替换成
def del_china(varibel_year):
    slats = [312, 340, 380, 420, 456]
    slons = [1008, 1060, 1100, 1140, 1180, 1220, 1260]
    shape = np.loadtxt('shape %s.txt' % varibel_year[-4:], dtype=float)
    shape = [int(i) for i in shape]
    data = np.loadtxt('%s.txt' % varibel_year)\
        .reshape(shape)
    for i in range(len(slats)-1):
        for j in range(len(slons)-1):
            ss = data[:, slats[i]:slats[i+1], slons[j]:slons[j+1]]
            np.savetxt('shape %s %i %i.txt' % (varibel_year[-4:], i, j), ss.shape)
            np.savetxt('%s %i %i.txt'%(varibel_year, i, j), ss.flatten())
    return


def ues(varible):
    for i in range(4):
        for j in range(6):
            sa = np.loadtxt(r'shape 1961 %i %i.txt' % (i, j))
            sa = [int(i) for i in sa]
            data = np.loadtxt(r'%s 1961 %i %i.txt'% (varible, i, j)).reshape(sa)
            for year in range(1962, 2019):
                sa = np.loadtxt(r'shape %i %i %i.txt' %(year, i, j))
                sa = [int(i) for i in sa]
                a = np.loadtxt(r'%s %i %i %i.txt' %(varible, year, i, j)).reshape(sa)
                # 本处73为限定了长度必须为73
                data = np.vstack((data, a[:73, :, :]))
            np.savetxt('shape %s %i %i.txt' % (varible, i, j), data.shape)
            np.savetxt('%s %i %i.txt' % (varible, i, j), data.flatten())



# varibles = ['evp', 'pevp']
# for i in varibles:
#     ues(i)





