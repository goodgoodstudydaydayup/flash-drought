# 快速储存和提取nc文件，传入data即可。
# 快速储存和提取nc文件，传入data即可。
def save_nc4(data, savtname):
    # 传入lon, lat, time, data以及储存时候使用的名字即可。
    import netCDF4 as nc4
    f = nc4.Dataset('%s.nc4' % savtname,'w', format='NETCDF4') #'w' stands for write

    tempgrp = f.createGroup('data')
    shape = data.shape
    shapename = []

    for i in range(len(shape)):
        tempgrp.createDimension('%i' %i, shape[i])
        shapename.append('%i' %i)

    mask = tempgrp.createVariable('data', 'f4', shapename)
    try:
        mask[:] = data
    except:
        print(data)
    f.close()
    return

def read_nc4(savtname):
    # 传入lon, lat, time, data以及储存时候使用的名字即可。
    from netCDF4 import Dataset
    f = Dataset('%s.nc4' %savtname, 'r')
    tempgrp = f.groups['data']
    data = tempgrp.variables['data'][:]
    return data

def read_nc411(savtname, variablename):
    import numpy as np
    read_name = ['SWdown_f_tavg', 'Wind_f_inst', 'Tair_f_inst', 'Rainf_f_tavg', 'RootMoist_inst',
                 'Lwnet_tavg', 'Swnet_tavg', 'SoilMoi0_10cm_inst', 'Evap_tavg',
                 'PotEvap_tavg', 'Qh_tavg', 'Qle_tavg', 'Qair_f_inst']
    save_name = ['swdown', 'wind', 'tem', 'pre', 'root', 'lw', 'sw', 'sm', 'et', 'pet', 'qh', 'qle', 'qair']
    names = {}
    for i in range(len(read_name)):
        names[save_name[i]] = read_name[i]
    import xarray as xr
    data = xr.open_dataset('%s.nc4' %savtname).sel(time=slice('2000-01-01', '2018-12-27'))
    variable_name = names[variablename]
    return np.array(data.variables[variable_name])




