def ll_del(lat, lon, area):
    # data 必须是中国范围
    import numpy as np
    lat0 = lat - area[0]
    slat0 = int(np.argmin(abs(lat0)))

    lat1 = lat - area[1]
    slat1 = int(np.argmin(abs(lat1)))

    lon0 = lon - area[2]
    slon0 = int(np.argmin(abs(lon0)))

    lon1 = lon - area[3]
    slon1 = int(np.argmin(abs(lon1)))

    return [slat0, slat1, slon0, slon1]


def china_del(area):
    # data 必须是中国范围
    import numpy as np
    from nc4 import read_nc4, save_nc4
    lat = read_nc4(r'D:\basis\fd\data\china lat')
    lon = read_nc4(r'D:\basis\fd\data\china lon')

    # slat0
    lat0 = lat - area[0]
    slat0 = int(np.argmin(abs(lat0)))

    lat1 = lat - area[1]
    slat1 = int(np.argmin(abs(lat1)))

    lon0 = lon - area[2]
    slon0 = int(np.argmin(abs(lon0)))

    lon1 = lon - area[3]
    slon1 = int(np.argmin(abs(lon1)))

    return [slat0, slat1, slon0, slon1]



def time_del(time, month410=False, beginyear='2000'):
    # 对58年的时间序列进行
    import pandas as pd
    time_all = pd.read_csv(r'D:\basis\fd\data\time_58year.csv', index_col=0, parse_dates=True)
    time_all = time_all[beginyear:]
    if month410 == True:
        time_all = time_all[time_all.index.month.isin(list(range(4,11)))]
    argindex = []

    for i in time:
        argindex.append(time_all.index.get_loc(i))
    return argindex

