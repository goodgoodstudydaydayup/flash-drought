import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from pandas import Series
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import numpy as np
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False


def get_before_after_period_data(fd_timeindex, varible_name, anomaly=False):
    begin = fd_timeindex[0]
    end = fd_timeindex[1]
    et = read_nc4(r'D:\basis\fd\data\china %s' % varible_name)
    et = np.ma.masked_equal(et, -9999.0)
    et[np.where(et == 32766)] = np.nan
    et[np.where(et == -32767)] = np.nan
    et = np.ma.masked_invalid(et)

    mth_b_fd = np.nanmean(et[begin - 6:begin, :, :], axis=0)
    mth_fd = np.nanmean(et[begin:end + 1, :, :], axis=0)
    mth_a_fd = np.nanmean(et[end + 1: end + 7], axis=0)
    if anomaly is True:
        reet = et.reshape([int(et.shape[0] / 73), 73, et.shape[1], et.shape[2]])

        etbegin = begin % 73 - 1
        etend= end % 73 - 1
        mth_b_fd_mean = np.nanmean(reet[:, etbegin - 6:etbegin, :, :], axis=(0, 1))
        mth_fd_mean = np.nanmean(reet[:, etbegin: etend+1, :, :], axis=(0, 1))
        mth_a_fd_mean = np.nanmean(reet[:, etend + 1: etend + 7, :, :], axis=(0, 1))
        mth_fd = mth_fd - mth_fd_mean
        mth_b_fd = mth_b_fd - mth_b_fd_mean
        mth_a_fd = mth_a_fd - mth_a_fd_mean
        return [mth_b_fd, mth_fd, mth_a_fd]
    return [mth_b_fd/10, mth_fd/10, mth_a_fd/10]


def plots(data, area, labels, quiver=None):
    import cartopy.crs as ccrs
    from nc4 import read_nc4
    import plot as pl
    x1 = area[0], x2 = area[1],    y1 = area[2],    y2 = area[3]
    area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])

    lats = read_nc4(r'D:\basis\fd\data\noaa lat')
    lons = read_nc4(r'D:\basis\fd\data\noaa lon')

    axes = pl.plotcors((1, 3), data, lons, lats, linestyle='contourf', quiver=quiver, clipshape=False,
                       ranges=[[0, 60, 60, 140], [60, 40, 20, 0], [60, 80, 100, 120,140]],
                       labels=labels, figsize=(9, 3),subplotsizes=[0.07,0.11,0.96,1,0.2,0.2])
    for i in axes:
        i.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color='red')
    return

def plothgt(data, area, space, labels):
    import cartopy.crs as ccrs
    from nc4 import read_nc4
    import plot_hgt as pl
    x1 = area[0]
    x2 = area[1]
    y1 = area[2]
    y2 = area[3]
    area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])

    lats = read_nc4(r'D:\basis\fd\data\noaa lat')
    lons = read_nc4(r'D:\basis\fd\data\noaa lon')

    axes = pl.plotcors((1, 3), data, lons, lats, rowlevels=space, clipshape=False,
                       ranges=[[60, 0, 60, 140], [60, 40, 20, 0], [60, 80, 100, 120,140]],
                       labels=labels, figsize=(9, 3),subplotsizes=[0.07,0.11,0.96,1,0.2,0.2])
    for i in axes:
        i.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color='red')
    return

def cal_plot_hgt(maskdata_name, varible, area, space):
    import pandas as pd

    allmask = pd.read_csv(r'D:\basis\fd\data\mask1979 %s.csv' % maskdata_name, index_col=0, parse_dates=True)
    mask = np.array(allmask['mask'])

    begin = np.argwhere(mask[:] == 1).flatten()
    end = np.argwhere(mask[:] == 3).flatten()

    for i in range(len(begin)):
        if ((allmask.index[begin[i]].month >= 4) & (allmask.index[begin[i]].month <= 10)) & ((
                                                                                                     allmask.index[end[
                                                                                                         i]].month >= 4) & (
                                                                                                     allmask.index[end[
                                                                                                         i]].month <= 10)):

            #######################修改处#############################

            # 前期绘图
            data0 = get_before_after_period_data([begin[i], end[i]], varible, anomaly=False)
            data1 = get_before_after_period_data([begin[i], end[i]], varible, anomaly=True)
            data3= get_before_after_period_data([begin[i], end[i]], 'vwnd ' + varible[-3:], anomaly=False)

            labels = ['-1M %s %s' % (varible, ((str(allmask.index[begin[i] - 6]))[0:10])),
                      'O %s %s to %s' % (
                          varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10]),
                      '+1M %s %s' % (varible, (str(allmask.index[end[i] + 7]))[0:10])]

            if varible[-3:] != '200':
                data4 = get_before_after_period_data([begin[i], end[i]], 'uwnd ' + varible[-3:], anomaly=False)
                plothgt([data0, data1, data3, data4], area, space, labels)
            else:
                plothgt([data0, data1, data3], area, space, labels)

            plt.show()
            plt.savefig(r'D:\basis\fd\%s %s.png' % (maskdata_name, '%s %s to %s' % (
                varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10])))
            plt.close()

def plot_area(maskdata_name, varible, area, anomaly=False, quiver=None):
    import pandas as pd

    allmask = pd.read_csv(r'D:\basis\fd\data\mask1979 %s.csv' % maskdata_name, index_col=0, parse_dates=True)
    mask = np.array(allmask['mask'])

    begin = np.argwhere(mask[:] == 1).flatten()
    end = np.argwhere(mask[:] == 3).flatten()

    for i in range(len(begin)):
        if ((allmask.index[begin[i]].month >= 4) & (allmask.index[begin[i]].month <= 10)) & ((
                                                                                                     allmask.index[end[
                                                                                                         i]].month >= 4) & (
                                                                                                     allmask.index[end[
                                                                                                         i]].month <= 10)):

            #######################修改处#############################

            # 前期绘图

            data = get_before_after_period_data([begin[i], end[i]], varible, anomaly=anomaly)

            labels = ['-1M %s %s' % (varible, ((str(allmask.index[begin[i] - 6]))[0:10])),
                      'O %s %s to %s' % (
                          varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10]),
                      '+1M %s %s' % (varible, (str(allmask.index[end[i] + 7]))[0:10])]

            if varible[1:4] == 'wnd':
                quiver = get_before_after_period_data([begin[i], end[i]], 'vwnd ' + varible[-3:], anomaly=anomaly)
            plots(data, area, labels, quiver=quiver)
            # plt.show()
            if anomaly == True:
                plt.savefig(r'D:\basis\fd\ano %s %s.png' % (maskdata_name, '%s %s to %s' % (
                    varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10])))

            else:
                plt.savefig(r'D:\basis\fd\%s %s.png' % (maskdata_name, '%s %s to %s' % (
                    varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10])))
            plt.close()

if __name__ == '__main__':
    from nc4 import read_nc4, save_nc4

    area_name = ['华中', '西南', '东北', '新疆']
    allarea = [[26.125, 36.125, 110.125, 120.125],
               [24.125, 33.125, 100.125, 110.125],
               [40.125, 50.125, 122.125, 134.125],
               [40.125, 48.125, 80.125, 90.125]]
    varibles = ['hgt', 'uwnd', 'omega']


    cal_plot_hgt(area_name[0], 'hgt' + ' 200', allarea[0], space=[10,10,10])
    cal_plot_hgt(area_name[0], 'hgt' + ' 500', allarea[0], space=[5, 5, 5])
    cal_plot_hgt(area_name[0], 'hgt' + ' 850', allarea[0], space=[2, 2, 2])


    # for i in range(4):
    #     for j in varibles:
    #         plot_area(area_name[i], j+' 200', allarea[i])
    #         plot_area(area_name[i], j+' 500', allarea[i])
    #         plot_area(area_name[i], j+' 850', allarea[i])

