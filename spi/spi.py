import xarray as xr


# da = xr.open_dataset(r'D:\basis\cmorph_lowres_daily_conus_prcp.nc')['prcp']
# print(da)


# a = xr.open_dataset(r'D:\basis\cmorph_lowres_daily_conus_spi_pearson_90.nc')
# print(a['spi_pearson_90'].sel(time='2000').values)


'''investigated the error in nco.ncpdq
the error came from output para'''
# from nco import Nco
# nco = Nco()
# netcdf_file = nco.ncpdq(input=r'D:\basis\try.nc',
#                                 options=[f'-a "{"lat,lon,time"}"', "-O"], output=r'D:\basis\try1.nc')
# import xarray as xr
# da = xr.open_dataset('china pre.nc4')
# da['Rainf_f_tavg'] = da['Rainf_f_tavg']* 10800 * 8
# da.to_netcdf('china_pre_sum.nc')


'''partition china gridded point data into regional data'''
# area_name = ['xinan', 'dongbei', 'xinjiang']
# allarea = [[24.125, 33.125, 102.125, 108.125],
#            [40.125, 50.125, 122.125, 128.125],
#            [36.125, 47.125, 87.125, 98.125]]
# lat = [24.125,40.125,42.125]
# lon = [102,103,104]
# data = []
# griddata = xr.open_dataset('pet.nc4')
# for range in allarea:
#     areadata = griddata.sel(lat=slice(range[0], range[1]), lon=slice(range[2], range[3])).mean(dim=['lat', 'lon'])
#     # data.append((areadata['Rainf_f_tavg'].values)*10800*8)
#     data.append((areadata['PotEvap_tavg'].values)*(0.408 * (10 ** -6)) * 10800 * 8 * 5)
# foo = xr.DataArray([[data[0],data[1],data[2]],[data[0],data[1],data[2]],[data[0],data[1],data[2]]], coords=[lat, lon, areadata['time']], dims=['lat', 'lon','time'])
# foo = xr.Dataset({'pet':foo})
# foo['pet'].attrs['units'] = 'mm'
# foo.to_netcdf('pet_daily.nc')


'''plot spi'''
import numpy as np
from pylab import mpl
import matplotlib.pyplot as plt
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
# def plotsci(name, types):
#     data = xr.open_dataset('ex4_spi_%s.nc'%name)
#     data1 = data.isel(lat=0, lon=0).variables['spi_%s'%name].values
#     def plots(ax,data1, title):
#         ax.plot(data['time'], data1)
#         ax.axhline(0, color='black', alpha=0.8)
#         ax.axhline(-1, color='yellow', linestyle='--', label='中旱临界值')
#         ax.axhline(-1.5, color='green', linestyle='--', label='重旱临界值')
#         ax.axhline(-2, color='red', linestyle='--', label='特旱临界值')
#         ax.set_ylabel('SPI')
#         ax.set_xlabel('年份')
#         ax.set_title(title)
#         plt.legend(loc='upper left')
#     fig,ax = plt.subplots(3,1,sharex='col', figsize=(9,8))
#     plt.subplots_adjust(top=0.97,
#     bottom=0.05,
#     left=0.055,
#     right=0.99,
#     hspace=0.23,
#     wspace=0.2)
#     plots(ax[0], data1, '西南 %s'%types)
#     plots(ax[1], data.isel(lat=0, lon=1).variables['spi_%s'%name].values,'东北')
#     plots(ax[2], data.isel(lat=0, lon=2).variables['spi_%s'%name].values,'新疆')
#     plt.show()
# plotsci('gamma_90', 'gamma scale=90d')
# plotsci('gamma_30', 'gamma scale=30d')
# plotsci('pearson_30', 'pearson scale=30d')
# plotsci('pearson_90', 'pearson scale=90d')



'''change into monthly data'''
# daily = xr.open_dataset('foo.nc')
# print(daily)
# monthly = daily.resample(time='m').mean()
# print(monthly)
# monthly.to_netcdf('foo_monthly.nc')
#
# lat = [24.125,40.125,42.125]
# lon = [102,103,104]
# alldata = xr.open_dataset('foo_monthly.nc')
# data = [alldata.isel(lat=0, lon=0).variables['prcp'].values
#         , alldata.isel(lat=0, lon=1).variables['prcp'].values
#         , alldata.isel(lat=0, lon=2).variables['prcp'].values]
# foo = xr.DataArray([[data[0],data[1],data[2]],[data[0],data[1],data[2]],[data[0],data[1],data[2]]]
#                    , coords=[lat, lon, alldata['time']], dims=['lat', 'lon','time'])
# foo = xr.Dataset({'prcp':foo})
# foo['prcp'].attrs['units'] = 'mm'
# # print(foo)
# foo.to_netcdf('foo_monthly_new.nc')
# netcdf_file = nco.ncpdq(input='D:/basis/fd/spi/foo_monthly.nc',
#                                 options=[f'-a "{"lat,lon,time"}"', "-O"], output=r'D:/basis/fd/spi/foo_monthly_re.nc')


daily = xr.open_dataset('pet_daily.nc')
alldata = daily.resample(time='m').mean()

lat = [24.125,40.125,42.125]
lon = [102,103,104]
data = [alldata.isel(lat=0, lon=0).variables['pet'].values
        , alldata.isel(lat=0, lon=1).variables['pet'].values
        , alldata.isel(lat=0, lon=2).variables['pet'].values]
foo = xr.DataArray([[data[0],data[1],data[2]],[data[0],data[1],data[2]],[data[0],data[1],data[2]]]
                   , coords=[lat, lon, alldata['time']], dims=['lat', 'lon','time'])
foo = xr.Dataset({'pet':foo})
foo['pet'].attrs['units'] = 'mm'
# print(foo)
foo.to_netcdf('pet_monthly.nc')

