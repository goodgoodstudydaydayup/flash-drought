'''create daily smi nc'''
import xarray as xr
import numpy as np
# for i in range(2000, 2019):
#     da = xr.open_dataset(r'/data1/GLDAS/data_new/daily/2.1/GLDAS_NOAH025_3H.A%i.nc4'%i)['SoilMoi0_10cm_inst']
#     da = da.sel(lat=slice(15,54), lon=slice(73, 136))
#     da.to_netcdf('smi_%s.nc4'%i)
#
#
# data_name = []
# for i in range(2000, 2019):
#     data_name.append('/home/zhj/zhgong/smi_%s.nc4' %i)
# dataset = xr.open_mfdataset([data_name],combine='by_coords')
# dataset.to_netcdf('smi.nc4')
from statsmodels.distributions.empirical_distribution import ECDF

'''create percentile'''
area_name = ['xinan', 'dongbei', 'xinjiang']
allarea = [[24.125, 33.125, 102.125, 108.125],
           [40.125, 50.125, 122.125, 128.125],
           [36.125, 47.125, 87.125, 98.125]]
lat = [24.125,40.125,42.125]
data = []
griddata = xr.open_dataset(r'D:\basis\fd\spi\smi.nc4')
for range in allarea:
    areadata = griddata.sel(lat=slice(range[0], range[1]), lon=slice(range[2], range[3]))\
        .mean(dim=['lat', 'lon'])
    data.append(areadata['SoilMoi0_10cm_inst'].values)

foo = xr.DataArray([data[0],data[1],data[2]], coords=[lat, areadata['time']], dims=['lat','time'])
foo = xr.Dataset({'smi':foo})
foo.to_netcdf('daily_regional_smi.nc4')



def main(data):
    def is_amj(sstmonth, sstday, need_time):
        return (sstmonth == need_time[0]) & (sstday == need_time[1])



    percentile = np.zeros([19, 365])
    percentile[np.where(percentile == 0)] = np.nan



    a = data1.sel(time='2001')
    count=-1
    # print(a['time.month'].values,a['time.day'].values.shape)
    for i in np.arange(1,13):
        # print('2001-%i'%i)
        # print(a.sel(time='2001-%i'%i)['time.day'].values)
        for j in a.sel(time='2001-%i'%i)['time.day'].values:
            sample = []
            result_data = []

            count = count+1
            # print(count)
            sdata = data.sel(time=is_amj(data['time.month'], data['time.day'], [i, j]))
            for ss in sdata['time'].values:

                trans_data = data.sel(time=slice(str(ss - np.timedelta64(15, 'D')),
                                                 str(ss + np.timedelta64(15, 'D'))))

                sample.append(trans_data.variables['smi'].values)
            result_data.append(data.sel(time=is_amj(data['time.month'], data['time.day'], [i, j])).variables['smi'].values)

            # begin to calculate
            sample = np.concatenate(sample).ravel()
            # sample = np.array(sample).reshape([16+18*31,])
            model = ECDF(sample)
            result = []
            for ssi in np.array(result_data).flatten():
                result.append(model(ssi))
            # print(count)
            percentile[:, count] = result

    return np.array(percentile).flatten()

def pentad_main(data):
    def is_amj(sstmonth, sstday, need_time):
        return (sstmonth == need_time[0]) & (sstday == need_time[1])

    percentile = np.zeros([19, 73])
    percentile[np.where(percentile == 0)] = np.nan

    a = data1.sel(time='2001')
    count=-1
    # print(a['time.month'].values,a['time.day'].values.shape)
    for step in range(len(a['time.month'])):
        i = a['time.month'].values[step]
        j = a['time.day'].values[step]
        sample = []
        result_data = []

        count = count+1
        # print(count)
        sdata = data.sel(time=is_amj(data['time.month'], data['time.day'], [i, j]))
        for ss in sdata['time'].values:

            trans_data = data.sel(time=slice(str(ss - np.timedelta64(15, 'D')),
                                             str(ss + np.timedelta64(15, 'D'))))

            sample.append(trans_data.variables['smi'].values)
        result_data.append(data.sel(time=is_amj(data['time.month'], data['time.day'], [i, j])).variables['smi'].values)

        # begin to calculate
        sample = np.concatenate(sample).ravel()
        # sample = np.array(sample).reshape([16+18*31,])
        model = ECDF(sample)
        result = []
        for ssi in np.array(result_data).flatten():
            result.append(model(ssi))
        # print(count)
        percentile[:, count] = result

    return np.array(percentile).flatten()

def monthly_main(data):
    def is_amj(sstmonth, need_time):
        return (sstmonth == need_time[0])

    percentile = np.zeros([19, 12])
    percentile[np.where(percentile == 0)] = np.nan

    a = data1.sel(time='2001')
    count=-1
    # print(a['time.month'].values,a['time.day'].values.shape)
    for step in np.arange(len(a['time.month'])):
        i = a['time.month'].values[step]
        result_data = []

        count = count+1
        # print(count)
        print(data.sel(time=is_amj(data['time.month'], [i])))
        result_data.append(data.sel(time=is_amj(data['time.month'], [i])).variables['smi'].values)
        sample = result_data
        # begin to calculate
        sample = np.concatenate(sample).ravel()
        # sample = np.array(sample).reshape([16+18*31,])
        model = ECDF(sample)
        result = []
        for ssi in np.array(result_data).flatten():
            result.append(model(ssi))
        # print(count)
        percentile[:, count] = result

    return np.array(percentile).flatten()
# daily percentile
# data = xr.open_dataset('daily_regional_smi.nc4')
# data_without_leap_day = data.sel(time=~((data.time.dt.month == 2) & (data.time.dt.day == 29)))
# all_per = []
# for i in np.arange(3):
#     data1 = data.isel(lat=i)
#     percentile = main(data1)*100
#     all_per.append(percentile)
# lat = [20,30,40]
# foo = xr.DataArray([all_per[0],all_per[1],all_per[2]]
#                    , coords=[lat, data_without_leap_day['time']], dims=['lat','time'])
# foo = xr.Dataset({'smi':foo})
# # print(foo)
# foo.to_netcdf('daily_smi_noaa.nc4')

# pentad percentile

# data = xr.open_dataset(r'pentad_regional_smi.nc4')
# data = data.sel(time=slice('2000','2018'))
# all_per = []
# for i in np.arange(3):
#     data1 = data.isel(lat=i)
#     percentile = pentad_main(data1)*100
#     all_per.append(percentile)
#
data = xr.open_dataset(r'monthly_regional_smi.nc4')
data = data.sel(time=slice('2000','2018'))
all_per = []
for i in np.arange(3):
    data1 = data.isel(lat=i)
    percentile = monthly_main(data1)*100
    all_per.append(percentile)
# #
lat = [20,30,40]
foo = xr.DataArray([all_per[0],all_per[1],all_per[2]]
                   , coords=[lat, data['time']], dims=['lat','time'])
foo = xr.Dataset({'smi':foo})
print(foo)
# foo.to_netcdf('daily_smi_noaa.nc4')



'''plot'''
# from pylab import mpl
# import matplotlib.pyplot as plt
# mpl.rcParams['font.sans-serif'] = ['SimHei']
# mpl.rcParams['axes.unicode_minus'] = False
# from pandas.plotting import register_matplotlib_converters
# register_matplotlib_converters()
#
# def plotsci(data):
#     def plots(ax,data1, title):
#         ax.plot(data['time'], data1)
#         ax.axhline(30, color='#ffff00', linestyle='--', label='D0')
#         ax.axhline(20, color='#fdd37e', linestyle='--', label='D1')
#         ax.axhline(10, color='#ffab00', linestyle='--', label='D2')
#         ax.axhline(5, color='#e70000', linestyle='--', label='D3')
#         ax.axhline(2, color='#720000', linestyle='--', label='D4')
#
#         # ax.axhline(21, color='#ffff00', linestyle='--', label='D0')
#         # ax.axhline(11, color='#fdd37e', linestyle='--', label='D1')
#         # ax.axhline(6, color='#ffab00', linestyle='--', label='D2')
#         # ax.axhline(3, color='#e70000', linestyle='--', label='D3')
#         # ax.axhline(0, color='#720000', linestyle='--', label='D4')
#
#         ax.set_ylabel('SMI')
#         ax.set_xlabel('年份')
#         ax.set_title(title)
#         plt.legend(loc='upper left')
#
#     fig,ax = plt.subplots(3,1,sharex='col', figsize=(9,8))
#     plt.subplots_adjust(top=0.97,
#     bottom=0.05,
#     left=0.055,
#     right=0.99,
#     hspace=0.23,
#     wspace=0.2)
#     plots(ax[0], data.isel(lat=0).variables['smi'].values, '西南')
#     plots(ax[1], data.isel(lat=1).variables['smi'].values,'东北')
#     plots(ax[2], data.isel(lat=2).variables['smi'].values,'新疆')
#     plt.show()

# print(xr.open_dataset('smi_noaa.nc4'))
# plotsci(xr.open_dataset('smi_noaa.nc4'))



