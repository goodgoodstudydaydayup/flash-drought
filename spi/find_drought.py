import xarray as xr
import numpy as np
from pylab import mpl
import matplotlib.pyplot as plt
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

# get SPEI
# spei_1m= xr.open_dataset('monthly_spei_gamma_01.nc')

# classification

def plot_spi(ax, x, data, color, begin, end, labels=None):
    ax.plot(x, data, color=color)
    ax.grid(axis='y')
    ax.axvline(begin, color='grey', alpha=0.8)
    ax.axvline(end, color='grey', alpha=0.8)

    ax.axhline(-1, color='yellow', linestyle='--', label='中旱临界值')
    ax.axhline(-1.5, color='green', linestyle='--', label='重旱临界值')
    ax.axhline(-2, color='red', linestyle='--', label='特旱临界值')

    # ax.set_xticks(x)
    # ax.set_yticks([0,0.5,1])
    # ax.set_xticklabels(['-6', '', '-4', '', '', '', 'begin', '', '', '', 'end', '', '','','+4', '','+6'])

    ax.text(begin-np.timedelta64(16, 'D'), ax.get_ylim()[1], 'fd begin')
    ax.text(end-np.timedelta64(6, 'D'), ax.get_ylim()[1], 'fd end')
    ax.legend(loc='upper right')
    if labels is not None:
        ax.text(ax.get_xlim()[0], ax.get_ylim()[1], 'a) '+labels)
    ax.set_ylabel('SPEI-1')


def plot_smi(ax, x, data, color, begin, end, labels=None):
    ax.plot(x, data, color=color)
    ax.grid(axis='y')
    ax.axvline(begin, color='grey', alpha=0.8)
    ax.axvline(end, color='grey', alpha=0.8)

    ax.axhline(30, color='#ffff00', linestyle='--', label='D0')
    ax.axhline(20, color='#fdd37e', linestyle='--', label='D1')
    ax.axhline(10, color='#ffab00', linestyle='--', label='D2')
    ax.axhline(5, color='#e70000', linestyle='--', label='D3')
    ax.axhline(2, color='#720000', linestyle='--', label='D4')

    # ax.set_xticks(x)
    # ax.set_yticks([0,0.5,1])
    # ax.set_xticklabels(['-6', '', '-4', '', '', '', 'begin', '', '', '', 'end', '', '','','+4', '','+6'])
    ax.legend(loc='upper right')
    ax.text(begin-np.timedelta64(16, 'D'), ax.get_ylim()[1], 'fd begin')
    ax.text(end-np.timedelta64(6, 'D'), ax.get_ylim()[1], 'fd end')
    if labels is not None:
        ax.text(ax.get_xlim()[0], ax.get_ylim()[1], 'b) '+labels)
    ax.set_ylabel('pentad SM percentile')

def plot_ab_smi(ax, x, data, color, begin, end, labels=None):
    ax.plot(x, data, color=color)

    ax.grid(axis='y')
    ax.axvline(begin, color='grey', alpha=0.8)
    ax.axvline(end, color='grey', alpha=0.8)

    # ax.axhline(30, color='#ffff00', linestyle='--', label='D0')
    # ax.axhline(20, color='#fdd37e', linestyle='--', label='D1')
    # ax.axhline(10, color='#ffab00', linestyle='--', label='D2')
    # ax.axhline(5, color='#e70000', linestyle='--', label='D3')
    # ax.axhline(2, color='#720000', linestyle='--', label='D4')

    # ax.set_xticks(x)
    # ax.set_yticks([0,0.5,1])
    # ax.set_xticklabels(['-6', '', '-4', '', '', '', 'begin', '', '', '', 'end', '', '','','+4', '','+6'])

    ax.text(begin-np.timedelta64(16, 'D'), ax.get_ylim()[1], 'fd begin')
    ax.text(end-np.timedelta64(6, 'D'), ax.get_ylim()[1], 'fd end')
    if labels is not None:
        ax.text(ax.get_xlim()[0], ax.get_ylim()[1], 'c) '+labels)
    ax.set_ylabel('daily absolute SM')
# get SPI


import pandas as pd
import matplotlib.pyplot as plt
area_name = ['xinan', 'dongbei', 'xinjiang']
data1 = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[0], index_col=0, parse_dates=True)
data2 = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[1], index_col=0, parse_dates=True)
data3 = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv'% area_name[2], index_col=0, parse_dates=True)

spi_1d = xr.open_dataset('monthly_spei_gamma_01.nc')
smi = xr.open_dataset('pentad_smi_noaa.nc4')
ab_smi = xr.open_dataset('smi_regional.nc')

def main(area):
    if area == 1:
        sfd = data1[data1['mask'] == 1].index
        efd = data1[data1['mask'] == 3].index
        for i in range(len(sfd)):
            fig = plt.figure(figsize=(9, 8))
            plt.subplots_adjust(top=0.96,
                                bottom=0.06,
                                left=0.075,
                                right=0.97,
                                hspace=0.29,
                                wspace=0.2)
            a = spi_1d.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                           lat=24.125,
                           lon=102)
            plot_spi(fig.add_subplot(311), a.time, a.variables['spei_gamma_03'].values,
                     'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Southeast')

            a = smi.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                        lat=20)
            plot_smi(fig.add_subplot(312), a.time, a.variables['smi'].values,
                     'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Southeast')
            # plt.show()

            a = ab_smi.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                           lat=24.125,
                           lon=102)
            plot_ab_smi(fig.add_subplot(313), a.time, a.variables['smi'].values,
                        'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Southeast')
            # plt.text(0.4,0.9, number, xycoords='figure points')
            plt.show()
            plt.savefig('%s.png' % str(sfd[i])[:10])
            plt.close()
    if area == 2:
        sfd = data2[data2['mask'] == 1].index
        efd = data2[data2['mask'] == 3].index
        for i in range(len(sfd)):
            fig = plt.figure(figsize=(9, 8))
            plt.subplots_adjust(top=0.96,
                                bottom=0.06,
                                left=0.075,
                                right=0.97,
                                hspace=0.29,
                                wspace=0.2)
            a = spi_1d.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                           lat=40.125,
                           lon=103)
            plot_spi(fig.add_subplot(311), a.time, a.variables['spei_gamma_03'].values,
                     'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Northeast')

            a = smi.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                        lat=30)
            plot_smi(fig.add_subplot(312), a.time, a.variables['smi'].values,
                     'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Northeast')
            # plt.show()

            a = ab_smi.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                           lat=40.125,
                           lon=103)
            plot_ab_smi(fig.add_subplot(313), a.time, a.variables['smi'].values,
                        'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Northeast')
            # plt.show()
            plt.savefig('%s.png' % str(sfd[i])[:10])
            plt.close()
    if area == 3:
        sfd = data3[data3['mask'] == 1].index
        efd = data3[data3['mask'] == 3].index
        for i in range(len(sfd)):
            fig = plt.figure(figsize=(9, 8))
            plt.subplots_adjust(top=0.96,
                                bottom=0.06,
                                left=0.075,
                                right=0.97,
                                hspace=0.29,
                                wspace=0.2)
            a = spi_1d.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                           lat=42.125,
                           lon=104)
            plot_spi(fig.add_subplot(311), a.time, a.variables['spei_gamma_03'].values,
                     'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Northwest')

            a = smi.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                        lat=40)
            plot_smi(fig.add_subplot(312), a.time, a.variables['smi'].values,
                     'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Northwest')
            # plt.show()

            a = ab_smi.sel(time=slice(str(sfd[i] - np.timedelta64(100, 'D')), str(sfd[i] + np.timedelta64(365, 'D'))),
                           lat=42.125,
                           lon=104)
            plot_ab_smi(fig.add_subplot(313), a.time, a.variables['smi'].values,
                        'blue', sfd[i]- np.timedelta64(5, 'D'), end=efd[i]+ np.timedelta64(5, 'D'), labels='Northwest')
            # plt.show()
            plt.savefig('%s.png' % str(sfd[i])[:10])
            plt.close()
main(1)
main(2)
main(3)
from datetime import datetime
import pandas as pd


'''calculate absolute soil moisture'''

# convert into regional data
# area_name = ['xinan', 'dongbei', 'xinjiang']
# allarea = [[24.125, 33.125, 102.125, 108.125],
#            [40.125, 50.125, 122.125, 128.125],
#            [36.125, 47.125, 87.125, 98.125]]
# lat = [24.125,40.125,42.125]
# lon = [102,103,104]
# data = []
# griddata = xr.open_dataset(r'D:\basis\fd\china sm 2000.nc4')
# for range in allarea:
#     areadata = griddata.sel(lat=slice(range[0], range[1]), lon=slice(range[2], range[3])).mean(dim=['lat', 'lon'])
#     # data.append((areadata['Rainf_f_tavg'].values)*10800*8)
#     data.append((areadata['SoilMoi0_10cm_inst'].values))
# foo = xr.DataArray([[data[0],data[1],data[2]],[data[0],data[1],data[2]],[data[0],data[1],data[2]]], coords=[lat, lon, areadata['time']], dims=['lat', 'lon','time'])
# foo = xr.Dataset({'smi':foo})
# foo['smi'].attrs['units'] = 'mm'
# foo.to_netcdf('smi_regional.nc')

