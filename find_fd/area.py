import plot as pl
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
import pandas as pd
from nc4 import read_nc4


def plots(ax, data, area, labels=None, zscore=False, levels=None):
    from nc4 import read_nc4
    lats = read_nc4(r'D:\basis\fd\data\china lat')
    lons = read_nc4(r'D:\basis\fd\data\china lon')

    if zscore == True:
        from scipy.stats.mstats import zscore
        data = zscore(data, axis=0)
    fill = pl.plotcors(ax, data, lons, lats,
                       ranges=[[15, 55, 70, 140], [15, 25, 35, 45, 55], [70, 90, 110, 130]],
                       labels=labels)
    ax.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color='red')
    return fill


def plot_area(maskdata_name, varible, area):
    x1 = area[0]
    x2 = area[1]
    y1 = area[2]
    y2 = area[3]
    area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
    allmask = pd.read_csv(r'D:\basis\fd\data\mask %s.csv' % maskdata_name, index_col=0, parse_dates=True)
    mask = np.array(allmask['mask'])
    et = read_nc4(r'D:\basis\fd\data\china %s' % varible)
    et = np.ma.masked_equal(et, -9999.0)

    from scipy.stats.mstats import zscore
    # et = zscore(et)
    begin = np.argwhere(mask[:] == 1).flatten()
    end = np.argwhere(mask[:] == 3).flatten()

    for i in range(len(begin)):
        if ((allmask.index[begin[i]].month >= 4) & (allmask.index[begin[i]].month <= 10)) & ((
                                                                                                     allmask.index[end[
                                                                                                         i]].month >= 4) & (
                                                                                                     allmask.index[end[
                                                                                                         i]].month <= 10)):
            fig = plt.figure(figsize=[9, 2.5])
            plt.subplots_adjust(left=0.07, bottom=0.04, right=0.97, top=0.99, wspace=0.2, hspace=0.2)

            levels = np.array([-3.0, -1.5, 0, 1.5, 3, 4.5, 6, 7.5])

            # 前期绘图
            fill = plots(fig.add_subplot(1, 3, 1, projection=ccrs.PlateCarree()), et[begin[i] - 1], area,
                         zscore=False, labels='%s %s' % (varible, ((str(allmask.index[begin[i] - 1]))[0:10])),
                         levels=levels)

            # 闪旱期绘图（平均）
            fill = plots(fig.add_subplot(1, 3, 2, projection=ccrs.PlateCarree()),
                         np.nanmean(et[begin[i]:end[i]], axis=0), area,
                         zscore=False, labels='%s %s to %s' % (
                    varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i]]))[0:10]),
                         levels=levels)

            # 后期绘图
            fill = plots(fig.add_subplot(1, 3, 3, projection=ccrs.PlateCarree()), et[end[i] + 1], area,
                         zscore=False, labels='%s %s' % (varible, (str(allmask.index[end[i] + 1]))[0:10]),
                         levels=levels)
            fig.tight_layout()
            # plt.show()
            plt.savefig(r'D:\basis\fd\%s %s.png' % (maskdata_name, '%s %s to %s' % (
                varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i]]))[0:10])))
            plt.close()


if __name__ == '__main__':

    area_name = ['华中', '西南', '东北', '新疆']
    allarea = [[26.125, 36.125, 110.125, 120.125],
               [24.125, 33.125, 100.125, 110.125],
               [40.125, 50.125, 122.125, 134.125],
               [40.125, 48.125, 80.125, 90.125]]

    for i in range(0, 4):
        area = allarea[i]
        dataname = area_name[i]

        from netCDF4 import Dataset

        plot_area(dataname, 'et', area)
