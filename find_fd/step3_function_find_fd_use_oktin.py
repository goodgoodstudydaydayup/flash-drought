import numpy as np
import pandas as pd

np.set_printoptions(threshold=np.inf)

def oktin_standard(et, pet):
    sesr, percentile_sesr, osesr, percentile_osesr = do_cal_in_onepoint(np.array(et).flatten(),
                                                                        np.array(pet).flatten())
    smask, percentile25 = do_cri_in_onepoint(sesr, percentile_sesr, osesr, percentile_osesr)
    return smask, percentile25

def do_cal_in_onepoint(et, pet):
    from pandas import DataFrame
    def szscore(data):
        from scipy.stats.mstats import zscore
        data = data.reshape(19, 73)
        # DataFrame(data).to_csv('esi.csv')
        percentile_data = np.zeros([19, 73])
        for i in range(73):
            data[:, i] = zscore(data[:, i])
            percentile_data[:, i] = calc_percentile(data[:, i])
        # DataFrame(data).to_csv('resi.csv')
        return data.flatten(), percentile_data.flatten()

    def calc_percentile(a, method='min'):
        # 获得传入序列的百分位值
        from scipy.stats import rankdata
        import numpy as np
        if isinstance(a, list):
            a = np.asarray(a)
        s = rankdata(a, method=method) / float(len(a))
        return [round(i, 3) for i in s]


    # 获得rses
    sesr = et / pet
    sesr, percentile_sesr = szscore(sesr)
    # 获得orses
    a0 = np.insert(sesr[:-1], 0, 0, axis=0)
    osesr = sesr - a0
    osesr, percentile_osesr = szscore(osesr)
    return sesr, percentile_sesr, osesr, percentile_osesr

def do_cri_in_onepoint(sesr, percentile_sesr, osesr, percentile_osesr):
    # rses 只为最后的per
    def find5con(percentile_osesr, sesr):
        # 找到变化率在40以下的orses，设定为1
        percentile_osesr_in01 = np.array(percentile_osesr)
        sesr = np.array(sesr)
        percentile_osesr_in01[np.where((percentile_osesr_in01[:] > 0.4))] = 9
        percentile_osesr_in01[np.where(percentile_osesr_in01[:] <= 0.4)] = 1
        percentile_osesr_in01[np.where(percentile_osesr_in01[:] == 9)] = 0
        s = pd.Series(percentile_osesr_in01)
        percentile_osesr_incum = s.groupby(s.eq(0).cumsum()).cumsum().tolist()
        # 找到单独的0，符合前后的rese更小的，单独存储位置。
        def find_zero(percentile_osesr_inmask, sesr):
            # 传入data为小于orses40%的1111序列累加型。也就是1，2，3，0，1类似形势。
            # 返回其中满足 仅有一个0 且前后sesr减小的0值。
            percentile_osesr_inmask = np.array(percentile_osesr_inmask)
            sesr = np.array(sesr)
            mask_single_zero = np.zeros(len(percentile_osesr_inmask))
            c = np.argwhere(percentile_osesr_inmask[:] == 1).flatten()
            c = c[np.argwhere((c - 1) >= 0)].flatten()
            if len(c) != 0:
                d = percentile_osesr_inmask[(c - 2)].flatten()
                zero_arg = (c[np.argwhere(d[:] != 0)].flatten()) - 1
                mask_single_zero[zero_arg] = 1
                s = sesr[zero_arg + 1] - sesr[zero_arg - 1]
                mask_single_zero[zero_arg[np.argwhere(s[:] > 0)]] = 0
            return mask_single_zero

        mask_single_zero_in_posesrin01 = find_zero(percentile_osesr_incum, sesr)
        percentile_osesr_in01[np.where(mask_single_zero_in_posesrin01 == 1)] = 1
        s = pd.Series(percentile_osesr_in01)
        percentile_osesr_in01 = np.array(s.groupby(s.eq(0).cumsum()).cumsum().tolist())

        # 将低于30天的删除
        mask = np.zeros(percentile_osesr_in01.shape)
        cs = np.argwhere(percentile_osesr_in01[:] > 5)
        cc = np.argwhere(percentile_osesr_in01[:] == 5).flatten()
        cs = np.append(cs, cc)
        for i in range(1, 5):
            cs = np.append(cs, cc - i)
        mask[[int(i) for i in cs]] = 1
        percentile_osesr_in01[np.where(mask[:] == 0)] = 0
        return percentile_osesr_in01, mask_single_zero_in_posesrin01

    def del_two_zero(percentile_osesr_in01, mask_single_zero_in_posesrin01):

        def ssdel_two_zero(mask_ranges, mask_zero_ranges):
            ## 此函数中mask为单条mask，相当于mask_ranges，只是当初变量设定不够严格，所以暂时不改，在此提示。
            # 本函数需要传入每一条单独的闪旱序列，用处为删除有两个0的闪旱。
            mask_ranges[np.where(mask_ranges[:] > 0)] = 1
            mask_ranges[np.where(mask_zero_ranges[:] == 1)] = 0

            # 将有两个0的给mask了。

            def find_OK5(sp):
                for i, imask_ranges in enumerate(mask_ranges[sp:-5]):
                    ssum = np.sum(mask_ranges[sp:-5][i:i + 5])
                    if ssum == 4:
                        zero_number = 1
                        return sp + i, zero_number
                    if ssum == 5:
                        zero_number = 0
                        return sp + i, zero_number
                return -999, 0

            def find_ep(sp, zero_number):
                for i, imask_ranges in enumerate(mask_ranges[sp + 5:]):
                    if zero_number == 1:
                        if imask_ranges == 0:
                            if mask_ranges[sp + 4 + i] != 0:
                                mask[sp] = 1
                                mask[sp + 4 + i] = 3
                                mask[sp + 1: sp + 4 + i] = 2
                                return (sp + 6 + i)
                            else:

                                mask[sp] = 1
                                mask[sp + 3 + i] = 3
                                mask[sp + 1: sp + 3 + i] = 2
                                return (sp + 6 + i)
                    else:
                        if imask_ranges == 0:
                            zero_number = 1

                return sp + len(mask_ranges[sp:])

            mask = np.zeros(mask_ranges.shape)
            sp = 0
            lenth = len(mask_ranges) - 5
            for i in range(10):
                if sp <= lenth:
                    sp, zero_number = find_OK5(sp)
                    if sp == -999:
                        break
                    sp = find_ep(sp, zero_number)
                else:
                    break
            return mask

        # 如果只有一个闪旱，则无法循环，所以做一个if
        d1 = np.argwhere(percentile_osesr_in01[:] == 1).flatten()
        if len(d1) == 1:
            percentile_osesr_in01[int(d1):] = ssdel_two_zero(percentile_osesr_in01[int(d1):], mask_single_zero_in_posesrin01[int(d1):])
        else:
            for i in range(len(d1) - 1):
                percentile_osesr_in01[d1[i]:d1[i + 1]] = ssdel_two_zero(percentile_osesr_in01[d1[i]:d1[i + 1]], mask_single_zero_in_posesrin01[d1[i]:d1[i + 1]])
            percentile_osesr_in01[d1[-1]:] = ssdel_two_zero(percentile_osesr_in01[d1[-1]:], mask_single_zero_in_posesrin01[d1[-1]:])
        return percentile_osesr_in01

    def final_del_less_20(percentile_osesr_in01, percentile_sesr):
        ## 此处data 为 mask
        percentile_osesr_in01[np.where(percentile_osesr_in01[:] > 0)] = 1
        s = pd.Series(percentile_osesr_in01)
        percentile_osesr_in01 = s.groupby(s.eq(0).cumsum()).cumsum().tolist()
        percentile_osesr_in01 = np.array(percentile_osesr_in01)

        m = np.argwhere(percentile_osesr_in01[:] >= 5).flatten()
        c = percentile_sesr[m].flatten()
        a_in_rses = m[np.argwhere(c[:] >= 0.2).flatten()]
        percentile_osesr_in01[a_in_rses] = 0

        d1 = np.argwhere(percentile_osesr_in01[:] == 1).flatten()
        if len(d1) != 0:
            # 将最后不满足rses<20% 以及 两个0的闪旱全部归为0
            def del_only15(ranges):
                if np.nanmax(ranges) <= 4:
                    ranges[:] = 0
                elif np.nanmax(ranges) > 4:
                    a = np.nanargmax(ranges)
                    ranges[1:a] = 2
                    ranges[0] = 1
                    ranges[a] = 3
                return ranges

            # 如果只有一个闪旱，则无法循环，所以做一个if
            if len(d1) == 1:
                percentile_osesr_in01[int(d1):] = del_only15(percentile_osesr_in01[int(d1):])
            else:
                for i in range(len(d1) - 1):
                    percentile_osesr_in01[d1[i]:d1[i + 1]] = del_only15(percentile_osesr_in01[d1[i]:d1[i + 1]])
                percentile_osesr_in01[d1[-1]:] = del_only15(percentile_osesr_in01[d1[-1]:])
        return percentile_osesr_in01

    def per_less_25(osesr, mask):
        ## 此函数中data为 osesr
        def per(osesr, sp, ep, mask, percentile25):
            osesr = np.array(osesr)
            sp = int(sp)
            ep = int(ep)
            mask = np.array(mask)
            imean = np.nanmean(osesr[sp:ep+1])

            osesr_reshapeinyear = osesr.reshape([19, 73])

            etbegin = (sp + 1) % 73 - 1
            etend = (ep + 1) % 73 - 1
            meandata = np.nanmean(osesr_reshapeinyear[:, etbegin: etend + 1], axis= 1)

            if np.count_nonzero(meandata != meandata) > 0:
                mask[sp:ep + 1] = 0
                return mask, percentile25
            a = np.percentile(meandata, 25)
            if imean > a:
                mask[sp:ep + 1] = 0
            else:
                percentile25[sp:ep + 1] = calc_percentile(meandata)[int(sp/73)]
            return mask, percentile25

        def calc_percentile(a, method='min'):
            # 获得传入序列的百分位值
            from scipy.stats import rankdata
            import numpy as np
            a = np.array(a)
            if isinstance(a, list):
                a = np.asarray(a)
            s = rankdata(a, method=method) / float(len(a))
            result = [round(i, 3) for i in s]
            return np.array(result)

        percentile25 = np.zeros(mask.shape)
        sp = np.argwhere(mask[:] == 1).flatten()-1

        ep = np.argwhere(mask[:] == 3).flatten()
        for i in range(len(sp)):
            mask, percentile25 = per(osesr, sp[i], ep[i], mask, percentile25)
        return mask, percentile25
    # def per_less_25(data, mask):
    #     ## 此函数中data为 rses
    #     def per(data, sp, ep, mask, percentile25):
    #         data = np.array(data)
    #         sp = int(sp)
    #         ep = int(ep)
    #         mask = np.array(mask)
    #
    #         imean = data[ep] - data[sp]
    #         reet = data.reshape([int(et.shape[0] / 73), 73])
    #         meandata = np.nanmean(reet[:, sp % 73 - 1:ep % 73], axis=1)
    #         a = np.percentile(meandata, 25)
    #         if imean > a:
    #             mask[sp:ep + 1] = 0
    #         else:
    #             percentile25[sp:ep + 1] = calc_percentile(meandata)[int(np.argwhere(allsp == sp).flatten())]
    #         return mask, percentile25
    #
    #     def calc_percentile(a, method='min'):
    #         # 获得传入序列的百分位值
    #         from scipy.stats import rankdata
    #         import numpy as np
    #         if isinstance(a, list):
    #             a = np.asarray(a)
    #         s = rankdata(a, method=method) / float(len(a))
    #         result = [round(i, 3) for i in s]
    #         return result
    #
    #     percentile25 = np.zeros(mask.shape)
    #     sp = np.argwhere(mask[:] == 1).flatten()
    #     ep = np.argwhere(mask[:] == 3).flatten()
    #     for i in range(len(sp)):
    #         mask, percentile25 = per(data, sp[i], ep[i], mask, percentile25)
    #     return mask, percentile25




    mask, mask_zero = find5con(percentile_osesr, sesr)
    mask = del_two_zero(mask, mask_zero)
    mask = final_del_less_20(mask, percentile_sesr)

    allmask, percentile25 = per_less_25(osesr, mask)
    return allmask, percentile25

def del_ocean_fd(mask):
    from nc4 import read_nc4
    slat = read_nc4('china lat')
    slon = read_nc4('china lon')
    lon_grid, lat_grid = np.meshgrid(slon, slat)
    # 将海洋上错误的闪旱判断设为0
    from global_land_mask import globe
    globe_land_mask = globe.is_ocean(lat_grid, lon_grid)
    for i in range(mask.shape[0]):
        mask[i][globe_land_mask] = 0
    return mask


def choose_month(mask, month, method='both'):
    # 对于mask进行选择对应月份。
    def del_fd_not410(smask):
        sfd = np.argwhere(smask == 1).flatten()
        efd = np.argwhere(smask == 3).flatten()
        for i in range(len(sfd)):
            if method == 'both':
                if (((sfd[i] + 1) % 73 - 1) < month[0] ) | (((efd[i] + 1) % 73 - 1) > month[1]):
                    smask[sfd[i]:efd[i] + 1] = 0
            elif method == '1':
                if (((sfd[i] + 1) % 73 - 1) < month[0]) | (((sfd[i] + 1) % 73 - 1) > month[1]):
                    smask[sfd[i]:efd[i] + 1] = 0
            elif method == '3':
                if (((efd[i] + 1) % 73 - 1) < month[0]) | (((efd[i] + 1) % 73 - 1) > month[1]):
                    smask[sfd[i]:efd[i]+1] = 0
        return smask

    mask_4_10 = np.zeros(mask.shape)
    for ilat in range(mask.shape[1]):
        for ilon in range(mask.shape[2]):
            mask_4_10[:, ilat, ilon] = del_fd_not410(mask[:, ilat, ilon])
    return mask_4_10

if __name__ == '__main__':
    from nc4 import read_nc4, save_nc4
    import xarray as xr
    import numpy as np


    # et = np.array(xr.open_dataset(r'china et 2000.nc4').sel(time=slice('2001-01-01', '2018-12-26')).variables['Evap_tavg'])
    #
    #
    # pet = np.array(xr.open_dataset(r'china pet 2000.nc4').sel(time=slice('2001-01-01', '2018-12-26')).variables['PotEvap_tavg'])
    # et[np.where(et == -9999.0)] = np.nan
    # pet[np.where(pet == -9999.0)] = np.nan
    # et = np.ma.masked_invalid(et)
    # pet = np.ma.masked_invalid(pet)
    #
    # et = et
    # pet = pet
    # #
    # mask = np.zeros(et.shape)
    # for ilat in range(len(et[0, :, 0])):
    #     for ilon in range(len(et[0, 0, :])):
    #         sset = et[:, ilat, ilon]
    #         spet = pet[:, ilat, ilon]
    #         # 判断缺失数目
    #         if (sset[np.isnan(sset)].shape[0] > (sset.shape[0]-50)) | (spet[np.isnan(spet)].shape[0] > (spet.shape[0]-50)):
    #             print('NO')
    #         else:
    #             smask, percentile25 = oktin_standard(sset, spet)
    #             mask[:, ilat, ilon] = smask
    #
    # save_nc4(mask, 'maskdata')


    # from nc4 import read_nc4, save_nc4
    '''mask 除了4和10月以外的闪旱。'''
    # mask = read_nc4(r'D:\basis\fd\data\maskdata')
    # # print(choose_month(mask))
    # save_nc4(choose_month(mask,[18,60]), 'mask410')
    '''划分为四季的闪旱。'''
    # hs = [[18, 30],[31,48],[49,60]]
    # # mask = read_nc4(r'D:\basis\fd\data\type1 et')
    # # # save_nc4(choose_month(mask, hs[0], '1'), r'D:\basis\fd\data\type1spring')
    # # # save_nc4(choose_month(mask, hs[1], '1'), r'D:\basis\fd\data\type1summer')
    # # save_nc4(choose_month(mask, hs[2], '1'), r'D:\basis\fd\data\type1fall')
    #
    #
    # mask = read_nc4(r'D:\basis\fd\data\type2 et')
    # print(np.argwhere(mask))
    # # save_nc4(choose_month(mask, hs[0], '1'), r'D:\basis\fd\data\type2spring')
    # # save_nc4(choose_month(mask, hs[1], '1'), r'D:\basis\fd\data\type2summer')
    # save_nc4(choose_month(mask, hs[2], '1'), r'D:\basis\fd\data\type2fall')


    # save_nc4(choose_month(mask, hs[0], '1'), r'D:\basis\fd\data\maskspring')
    # save_nc4(choose_month(mask, hs[1], '1'), r'D:\basis\fd\data\masksummer')
    # save_nc4(choose_month(mask, hs[2], '1'), r'D:\basis\fd\data\maskfall')

    # a = choose_month(mask, [31,48], '1')
    # print(choose_month(mask, hs[1], '1'))


    # 存储一下坐标信息。
    # import xarray as xr
    # a = xr.open_dataset(r'D:\basis\fd\data\china et 2000.nc4')
    # save_nc4(np.array(a.lat), 'china lat')
    # save_nc4(np.array(a.lon), 'china lon')
    def cal_area_mean(data, area):
        # lat_partition = [312, 340, 380, 420, 456]
        # lon_partition = [1008, 1060, 1100, 1140, 1180, 1220, 1260]
        # lat = np.loadtxt(r'D:\basis\fd\data\lat.csv')
        # lon = np.loadtxt(r'D:\basis\fd\data\lon.csv')
        lat = read_nc4(r'D:\basis\fd\data\china lat')
        lon = read_nc4(r'D:\basis\fd\data\china lon')
        lat0 = int(np.argwhere(lat[:] == area[0]))
        lat1 = int(np.argwhere(lat[:] == area[1]))
        lon0 = int(np.argwhere(lon[:] == area[2]))
        lon1 = int(np.argwhere(lon[:] == area[3]))

        # 将缺省值mask
        data = np.array(data)
        # data = del_ocean_fd(data)
        data[np.where(data == -9999.0)] = np.nan
        data = np.ma.masked_invalid(data)

        # 将海洋值mask
        meandata = np.nanmean(data[:, lat0:lat1, lon0:lon1], axis=(1, 2))
        return meandata


    et = read_nc4(r'D:\basis\fd\data\china et 2000')
    pet = read_nc4(r'D:\basis\fd\data\china pet 2000')
    et = np.ma.masked_equal(et, -9999.0)
    pet = np.ma.masked_equal(pet, -9999.0)
    area = [40.125, 50.125, 122.125, 128.125]
    set = cal_area_mean(et, area)
    spet = cal_area_mean(pet, area)

    rses, percentile_rses, orses, percentile_orses = do_cal_in_onepoint(set, spet)
    aaa = [rses, percentile_rses, orses, percentile_orses]
    mask, percentile25 = do_cri_in_onepoint(rses, percentile_rses, orses, percentile_orses)
    masks = pd.DataFrame(mask)
    print(masks)
    masks.to_excel('sksks.xlsx')