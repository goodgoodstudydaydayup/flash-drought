from nc4 import read_nc4, save_nc4
import numpy as np
import matplotlib.pyplot as plt
import sys

sys.path.append(r'D:\basis\fd\find_fd')
sys.path.append(r'D:\basis\fd\composite_analysis')

'''step1 挑出格点闪旱， 存为mask
   step2 根据mask[4-10月] 绘出 年频率图以及总年份闪旱fd_day图（同期闪旱发生图）
   step3 切割区域，按照区域范围+区域名字的方法，分别绘出
                 年际分布图  、 闪旱的合成分析图  、 oktin图'''

''' 被允许存下的文件：
     china 特征变量名.nc4 等 （中国各特征变量的原始数据值， 在做空间和时间分割时，可以参考current function）
     mask.nc4  (格点闪旱数据， 1为闪旱开始， 2为闪旱发生中， 3为闪旱结束)
     mask410.nc4 (格点闪旱数据，在4-10月)
     
     对于区域：
     mask 区域名.nc4 （存储有所有需要的数据，mask, et, pet, orses等
                      但是特征变量只有et和pet，不过在实际运行中，我们 也不会抽取这里的特征变量数值。）
     '''

def main_program(begin):
    # 已存在所有数据时，进行绘图分析的统一程序。
    slat = read_nc4(r'D:\basis\fd\data\china lat')
    slon = read_nc4(r'D:\basis\fd\data\china lon')

    if np.isin(1, begin) == True:
        # 1、绘出年频率图
        from step46_fd_partition import fd_frequency, plot_select_area
        mask410 = read_nc4(r'D:\basis\fd\data\mask410')
        fd_freq_all = fd_frequency(mask410, pointshape=(58, 44), freq='all')
        plot_select_area(fd_freq_all, slon, slat, labels='percent of years between 1961 and 2018')
        plt.savefig(r'D:\basis\fd\percent of years between 1961 and 2018.png')
        plt.close()
    if np.isin(2, begin) == True:
        # 2、绘出同期闪旱发生图
        from step46_fd_partition import fd_day_index, plot_select_area
        mask410 = read_nc4(r'D:\basis\fd\data\mask410')
        mask_day = fd_day_index(mask410, freq=freq)
        plot_select_area(np.sum(mask_day, axis=0), slon, slat, 'flash drought in the same period')
        plt.savefig(r'D:\basis\fd\flash drought in the same period')
        plt.close()
    if np.isin(3, begin) == True:
        # 3、区域年际分布图
        from plot_mask_series import plot_fd
        import pandas as pd
        for i in area_name:
            result = pd.read_csv(r'D:\basis\fd\data\mask1979 %s.csv' % i, index_col=0, parse_dates=True)
            result['mask'][(result['mask'] == 3)] = 1.25
            plot_fd(result['mask'][(result['mask'] == 1) | (result['mask'] == 1.25)], titlename='%s年际分布.png' % i)
            plt.savefig(r'D:\basis\fd\%s年际分布.png' % i, dpi=800)
            plt.close()
    if np.isin(4, begin) == True:
        # 4、区域闪旱空间特征值分布图
        from area_cor import plot_area
        for j in plot_area_varible:
            for i in range(0, len(allarea)):
                sarea = allarea[i]
                dataname = area_name[i]
                plot_area(dataname, j, sarea, beginyear=beginyear, anomaly=anomaly)
    if np.isin(5, begin) == True:
        # 5、 闪旱oktin线图
        import pandas as pd
        from plot_oktin_fd_figure import plot_oktin_figure
        for name in area_name:
            data = pd.read_csv(r'D:\basis\fd\data\mask%s %s.csv' % (beginyear,name), index_col=0, parse_dates=True)
            sfd = data.index[data['mask'] == 1]
            efd = data.index[data['mask'] == 3]
            fdtime = []
            from current_function_in_fd import time_del
            for i in range(len(sfd)):
                fdtime.append(time_del([str(sfd[i])[:10], str(efd[i])[:10]], beginyear='1979'))
            for j in range(len(sfd)):
                plot_oktin_figure(data, fdtime[j], name)

# 在计算同期闪旱图时，修改freq
freq = 6
area_name = [ '西南', '东北', '新疆']
allarea = [
           [24.125, 33.125, 100.125, 110.125],
           [40.125, 50.125, 122.125, 134.125],
           [40.125, 48.125, 80.125, 90.125]]

area_name = ['华中']
allarea = [[26.125, 36.125, 110.125, 120.125],
           ]
beginyear='1979'
anomaly = True
# 在绘制闪旱的空间特征值分布图时，所绘制的因子
plot_area_varible = ['qet', 'et', 'qh', 'qle', 'sm', 'tem']

main_program([4])





