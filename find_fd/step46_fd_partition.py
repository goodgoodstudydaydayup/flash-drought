import numpy as np
from nc4 import save_nc4, read_nc4
import matplotlib.pyplot as plt


def choose_month(mask):
    # 对于mask进行选择对应月份。
    def del_fd_not410(smask):
        sfd = np.argwhere(smask == 1).flatten()
        efd = np.argwhere(smask == 3).flatten()
        for i in range(len(sfd)):
            if (((sfd[i] + 1) % 73 - 1) < 19 ) | (((efd[i] + 1) % 73 - 1) > 62):
                print(sfd[i],efd[i])
                smask[sfd[i]:efd[i]+1] = 0
        return smask

    mask_4_10 = np.zeros(mask.shape)
    for ilat in range(mask.shape[1]):
        for ilon in range(mask.shape[2]):
            mask_4_10[:, ilat, ilon] = del_fd_not410(mask[:, ilat, ilon])
    return mask_4_10


def fd_frequency(mask, pointshape=(58,73), freq='all'):
    '''!!!!!!!!!!!!!!!!!!!!!!警惕，此处只有all进行了修改，单年的并没有进行修改！！！！！！！！'''
    # 对频率进行计算，，可分为年或者全序列, 仅针对年份为58， 且
    def point_frequency(point_mask, freq='year'):
        if freq != 'year':
            point_mask = point_mask.reshape(pointshape)
            point_freq = len(np.argwhere(np.sum(point_mask, axis=0) > 0).flatten())
            return point_freq/pointshape[0]

        else:
            point_mask = point_mask.reshape(pointshape)
            point_freq = np.zeros(pointshape[0])
            for i in range(pointshape[0]):
                point_freq[i] = len(np.argwhere(point_mask[i, :] > 0).flatten()) / pointshape[1]
        return point_freq

    if freq == 'year':
        fd_freq = np.zeros([pointshape[0], mask.shape[1], mask.shape[2]])
        for ilat in range(len(mask[0, :, 0])):
            for ilon in range(len(mask[0, 0, :])):
                fd_freq[:, ilat, ilon] = point_frequency(mask[:, ilat, ilon].flatten(), freq=freq)
    else:
        fd_freq = np.zeros([mask.shape[1], mask.shape[2]])
        for ilat in range(len(mask[0, :, 0])):
            for ilon in range(len(mask[0, 0, :])):
                fd_freq[ilat, ilon] = point_frequency(mask[:, ilat, ilon].flatten(), freq=freq)
    return fd_freq


def fd_day_index(mask, freq):
    # 在给定的时间范围内，只要出现了fd，则该点变为1
    smask = []
    ssmask = np.zeros(mask.shape[1:3])
    for i in range(0, int(mask.shape[0]) - freq, freq):
        for j in range(freq):
            ssmask[np.where(mask[i + j] > 0)] = 1
        smask.append(ssmask)
        print(ssmask)
    return np.array(smask)


def select_area(mask):
    def cal_select_area(mask_all):
        from scipy.ndimage import generic_filter
        cal_select_zoom = np.zeros(mask_all.shape)
        ''''# 20 和 15 都可以调整！！！！请参考着使用！！！'''
        images = generic_filter(mask_all, np.sum, size=10)
        cal_select_zoom[np.where(images > 7)] = 1
        return cal_select_zoom

    all_cal_select = np.zeros(mask[0].shape)
    for i in range(mask.shape[0]):
        cal_select_zoom = cal_select_area(mask[i])
        all_cal_select = all_cal_select + cal_select_zoom
    return all_cal_select


def select_single_to_find_size_and_images(smask):
    def cal_select_area(mask_all):
        from scipy.ndimage import generic_filter
        cal_select_zoom = np.zeros(mask_all.shape)
        ''''# 20 和 15 都可以调整！！！！请参考着使用！！！'''
        images = generic_filter(mask_all, np.sum, size=10)
        cal_select_zoom[np.where(images > 7)] = 1
        return cal_select_zoom

    cal = cal_select_area(smask)
    return cal


def plot_select_area(data, slon, slat, labels=' '):
    # 开始绘图
    import plot as pl
    import matplotlib.pyplot as plt
    import cartopy.crs as ccrs
    ax = plt.subplot(111, projection=ccrs.PlateCarree())
    pl.splotcors(ax, data, slon, slat,
                ranges=[[18.125, 53.125, 72.125, 134.125], [15, 25, 35, 45, 55], [70, 90, 110, 130]], labels=labels)


# 不动这一块，联系到了函数

def step6pointplot(mask, area, fdtime, month410=False):
    # 此函数作用，绘出给定区域给定时间段的数据的前五侯，后五侯的图，存储。
    # 要求，是58年的数据！！！同时，原始数据mask范围也在中国
    slat = read_nc4('china lat')
    slon = read_nc4('china lon')

    from current_function_in_fd import china_del, time_del

    index = china_del(area)
    timeindex = time_del(fdtime, month410=month410)
    slat = slat[index[0]:index[1]]
    slon = slon[index[2]:index[3]]
    mask = mask[timeindex[0] - 5:timeindex[1] + 5, index[0]:index[1], index[2]:index[3]]
    import pandas as pd
    time_all = pd.read_csv('time_58year.csv', index_col=0, parse_dates=True)
    if month410 == True:
        time_all = time_all[time_all.index.month.isin(list(range(4, 11)))]
    time_all = time_all.iloc[timeindex[0] - 5:timeindex[1] + 5]
    for i in range(5):
        plot_select_area(mask[i], slon, slat, '前%i侯 %s ' % (5 - i, str(time_all.index[i])[0:10]))
        import matplotlib.pyplot as plt
        plt.savefig('闪旱空间每侯数据展现 %i %s.png' % (i, str(time_all.index[i])[0:10]))
        plt.close()

    for i in range(5, timeindex[1] - timeindex[0] + 5):
        plot_select_area(mask[i], slon, slat, '闪旱期间格点闪旱 %s %i' % (str(time_all.index[i])[0:10], i - 4))
        import matplotlib.pyplot as plt
        plt.savefig('闪旱空间每侯数据展现 %i %s.png' % (i, str(time_all.index[i])[0:10]))
        plt.close()

    for nameinpentad, i in enumerate(range(timeindex[1] - timeindex[0] + 5, mask.shape[0])):
        plot_select_area(mask[i], slon, slat, '后%i侯 %s' % (nameinpentad, str(time_all.index[i])[0:10]))
        import matplotlib.pyplot as plt
        plt.savefig('闪旱空间每侯数据展现 %i %s.png' % (i, str(time_all.index[i])[0:10]))
        plt.close()


if __name__  == '__main__':
    slat = read_nc4(r'D:\basis\fd\data\china lat')
    slon = read_nc4(r'D:\basis\fd\data\china lon')

    # 将除了4-10月外其他月份剔除。
    # mask = read_nc4(r'D:\basis\fd\data\maskdata')
    # # print(choose_month(mask))
    # save_nc4(choose_month(mask), 'mask410')
    #
    #
    # mask = read_nc4(r'D:\basis\fd\data\mask410')
    # print(np.count_nonzero(mask != mask))
    # mask_day = fd_day_index(mask, freq=6)
    # print(np.count_nonzero(mask_day != mask_day))
    # mask_day = np.sum(mask_day, axis=0)
    # plot_select_area(mask_day, slon, slat, 'no fileter fd day')
    # import matplotlib.pyplot as plt
    # plt.show()
    # plt.savefig('no fileter fd day.png' )
    # plt.close()



    # area_name = ['华中', '西南', '东北', '新疆']
    allarea = [[24.125, 33.125, 102.125, 108.125],
               [40.125, 50.125, 122.125, 128.125],
               [36,47,87,98]]
    #
    # fdtime = ['2000-04-01', '2002-10-28']
    # mask = read_nc4('mask410')
    # mask[np.where(mask > 0)] = 1
    # allarea = [18.125, 53.125, 72.125, 134.125]
    # step6pointplot(mask, allarea, fdtime, month410=True)

    '''绘出标准的foc'''

    # # foc = select_area(mask)
    # # print(mask.shape)
    def plot_standard_foc(mask, ax, ax_jiuduanxinposition, ranges, title):
        mask = np.ma.masked_where(mask != 1, mask)
        foc = np.sum(mask, axis=0)
        print(np.max(foc))
        from scipy.ndimage import filters
        foc = filters.gaussian_filter(foc, [3,3], mode='constant')

        # foc = read_nc4('ss')
        # print(foc)
        from plot_new_method import plot_single_ax
        import matplotlib.pyplot as plt

        slat = read_nc4(r'D:\basis\fd\data\china lat')
        slon = read_nc4(r'D:\basis\fd\data\china lon')

        fill, cbar = plot_single_ax(ax,
                       [
                        [foc, 'contourf', ranges]
                       ],
                       oslat=slat, oslon=slon, clipshape=True, gridline=True, colorbar='v',
                              ranges=[[55,15, 70, 138],[20,30,40,50],[90,110,130]])
        ax_jiuduanxian = fig.add_axes(ax_jiuduanxinposition, projection=ccrs.PlateCarree())
        plot_single_ax(ax_jiuduanxian,
                       [
                        [foc, 'contourf', np.arange(0,20,2.5)]
                       ],
                       oslat=slat, oslon=slon, clipshape=True, gridline=False, shplinewidth=1.5,
                              ranges=[[25,0, 105, 125],[],[]])

        def areas(area, color, label):
            x1 = area[0]
            x2 = area[1]
            y1 = area[2]
            y2 = area[3]
            area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
            ax.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color=color, label=label)
        # areas(allarea[0], 'red', 'Yangtz-River')
        areas(allarea[0], 'yellow', '西南')
        areas(allarea[1], 'green', '东北')
        areas(allarea[2], 'red', '新疆')
        # ax.legend(loc='upper left')
        ax.set_title(title)

        cbar.set_ticks(ranges[::2])
        cbar.set_ticklabels([int(i) for i in ranges[::2]])
        return fill
        # plt.savefig('figure3.png', dpi=800,bbox_inches='tight')`


    import cartopy.crs as ccrs
    fig, ax = plt.subplots(2,2, sharex='col', sharey='row',figsize=(9,5), subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
    plt.subplots_adjust(top=0.965,
bottom=0.05,
left=0.06,
right=0.94,
hspace=0.2,
wspace=0.2)
    savetype = 'type1'
    dataname = ['%s et'%savetype, '%sspring'%savetype, '%ssummer'%savetype, '%sfall'%savetype]
    ax_position = [[0.362, 0.563,0.08,0.12],
                   [0.845, 0.563,0.08,0.12],
                   [0.362,0.062,0.08,0.12],
                   [0.845,0.062,0.08,0.12]]
    ranges=[np.arange(0,7,0.5),
            np.arange(0,4,0.5),
            np.arange(0,4,0.5),
            np.arange(0,3,0.5)]
    title = ['中国', 'AM', 'JJA', 'SO']
    for i in range(2):
        for j in range(2):
            count = i*2+j
            fill = plot_standard_foc(read_nc4(r'D:\basis\fd\data\%s'%dataname[count]), ax[i][j],
                                     ax_position[count], ranges[count], title[count])

    # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
    # # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
    # cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
    # cb_ax.set_title('次')
    plt.show()


    '''label is ugly , change.'''
    # from pylab import mpl
    # mpl.rcParams['font.sans-serif'] = ['SimHei']
    # mpl.rcParams['axes.unicode_minus'] = False
    # fig = plt.figure(figsize=[9, 0.3])
    # plt.plot([0,1],[0,0], alpha=0)
    #
    # plt.plot([0,0.1],[0,0], color='yellow')
    # plt.text(0.12, -0.03, "西南"
    #            )
    #
    # plt.plot([0.4,0.5],[0,0], color='green')
    # plt.text(0.52, -0.03, "东北"
    #            )
    # plt.plot([0.8,0.9],[0,0], color='red')
    # plt.text(0.92, -0.03, "新疆"
    #            )
    # sax = plt.gca()
    # sax.get_yaxis().set_visible(False)  # 不显示y轴
    # sax.get_xaxis().set_visible(False)
    # sax.spines['top'].set_visible(False)  # 去掉上边框
    # sax.spines['bottom'].set_visible(False)  # 去掉下边框
    # sax.spines['left'].set_visible(False)  # 去掉左边框
    # sax.spines['right'].set_visible(False)
    # plt.show()
    # plt.savefig('figure5table.png', dpi=800)