# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from netCDF4 import Dataset
from nc4 import read_nc4, save_nc4

def tran_day(version, name, year):
    tem = []
    win = []
    pre = []
    # 将3h的转为日值（平均处理）
    for i in name:
        ncin = Dataset(r'/data1/GLDAS/SourceFile/%s/%i/%s' %(version,year, i), 'r')
        # tem.append(ncin.variables['Lwnet_tavg'][0])
        # win.append(ncin.variables['Swnet_tavg'][0])
        # tem.append(ncin.variables['Rainf_f_tavg'][0])
        # win.append(ncin.variables['RootMoist_inst'][0])
        # pre.append(ncin.variables['SWdown_f_tavg'][0])
        # tem.append(ncin.variables['Tair_f_inst'][0])
        # win.append(ncin.variables['Wind_f_inst'][0])
        # pre.append(ncin.variables['Psurf_f_inst'][0])
        # sm.append(ncin.variables['SoilMoi0_10cm_inst'][0])
        # tem.append(ncin.variables['Evap_tavg'][0])
        # win.append(ncin.variables['PotEvap_tavg'][0])
        # qh.append(ncin.variables['Qh_tavg'][0])
        # qle.append(ncin.variables['Qle_tavg'][0])
        ncin.close()
    day = []
    day.append(np.nanmean(tem, axis=0))
    day.append(np.nanmean(win, axis=0))
    return day

### 从数据分布出发，按照年份，提出每日数据，放入csv文件中，以年份命名。
def change_period20(year):
    address_txt = pd.read_csv(r'/data1/GLDAS/SourceFile/2.0/%i.txt' % (year), header=None)
    # address_txt = pd.read_csv(r'D:\basis\fd\GLDAS\%i.txt' % (year), header=None)
    # address_txt = pd.read_csv(r'%i.txt' % (year), header=None)
    sm = []
    evp = []
    pevp = []
    for i in range(0, len(address_txt), 40):
        five_day_add = []
        if i <= len(address_txt)-40:
            for j in range(0,40):
                    five_day_add.append(address_txt.iloc[i+j][0][-39:])
        else:
            for j in range(i, i+2):
                five_day_add.append(address_txt.iloc[j][0][-39:])
        day = tran_day('2.0',five_day_add, year)
        sm.append(day[0])
        evp.append(day[1])
    save_nc4(np.array(sm)[:, 312:456, 1008:1260], 'et %i v2' %year)
    save_nc4(np.array(evp)[:, 312:456, 1008:1260], 'pet %i v2' %year)
    return

def change_period21(year):
    address_txt = pd.read_csv(r'/data1/GLDAS/SourceFile/2.1/%i.txt' % (year), header=None)
    # address_txt = pd.read_csv(r'D:\basis\fd\GLDAS\%i.txt' % (year), header=None)
    # address_txt = pd.read_csv(r'%i.txt' % (year), header=None)
    sm = []
    evp = []
    pevp = []
    for i in range(0, len(address_txt), 40):
        five_day_add = []
        if i <= len(address_txt)-40:
            for j in range(0,40):
                    five_day_add.append(address_txt.iloc[i+j][0][-39:])
        else:
            for j in range(i, i+2):
                five_day_add.append(address_txt.iloc[j][0][-39:])
        day = tran_day('2.1',five_day_add, year)
        sm.append(day[0])
        evp.append(day[1])
    save_nc4(np.array(sm), 'rpre %i' %year)
    save_nc4(np.array(evp), 'swnet %i' %year)
    return

def combine_year():
    def del_china(varible):
        lat_partition = [312, 456]
        lon_partition = [1008, 1260]
        alat = np.loadtxt('lat.csv')
        alon = np.loadtxt('lon.csv')
        lat = alat[lat_partition[0]:lat_partition[1]]
        lon = alon[lon_partition[0]:lon_partition[1]]
        for i in range(1979, 2019):
            mask_data = read_nc4('%s %i' % (varible, i))
            mask_data = mask_data[:, 312:456, 1008:1260]
            save_nc4(mask_data, 'trans %s %i' % (varible, i))

    def ues(varible):
        data = read_nc4('trans %s 1979' % (varible))
        for year in range(1980, 2019):
            a = read_nc4('trans %s %i' % (varible, year))
            # 本处73为限定了长度必须为73
            data = np.vstack((data, a[:73, :, :]))
        save_nc4(data, 'china %s' % varible)

    for i in ['rpre', 'swnet']:
        del_china(i)
        ues(i)

# for i in range(2001, 2019):
#     change_period21(i)
# combine_year()


'''for noaa data'''
# def into_5_day(varible):
#     for year in range(1979,2019):
#         hgt = Dataset(r'/data1/Reanalysis/NCEP2/daily/%s/%s.%i.nc' % (varible, varible, year), 'r')
#         hgt = hgt.variables['%s' % varible][:]
#         hgt[np.where(hgt == 32766)] = np.nan
#         hgt[np.where(hgt == -32767)] = np.nan
#         hgt = np.ma.masked_invalid(hgt)
#
#         hgt100 = hgt[:365, 11, :, :]
#         hgt200 = hgt[:365, 9, :, :]
#         hgt500 = hgt[:365, 5, :, :]
#         hgt850 = hgt[:365, 2, :, :]
#
#         shgt100 = hgt100.reshape((73, 5, hgt100.shape[1], hgt100.shape[2]))
#         shgt200 = hgt200.reshape((73, 5, hgt200.shape[1], hgt200.shape[2]))
#         shgt500 = hgt500.reshape((73, 5, hgt200.shape[1], hgt200.shape[2]))
#         shgt850 = hgt850.reshape((73, 5, hgt200.shape[1], hgt200.shape[2]))
#
#         shgt100 = np.nanmean(shgt100, axis=1)
#         shgt200 = np.nanmean(shgt200, axis=1)
#         shgt500 = np.nanmean(shgt500, axis=1)
#         shgt850 = np.nanmean(shgt850, axis=1)
#
#         save_nc4(shgt100, '%s 100 %i' % (varible, year))
#         save_nc4(shgt200, '%s 200 %i' % (varible,year))
#         save_nc4(shgt500, '%s 500 %i' % (varible,year))
#         save_nc4(shgt850, '%s 850 %i' % (varible,year))
#
#
# def ues(varible):
#     data = read_nc4('%s 1979' % (varible))
#     for year in range(1980, 2019):
#         a = read_nc4('%s %i' % (varible, year))
#         # 本处73为限定了长度必须为73
#         data = np.vstack((data, a[:73, :, :]))
#     save_nc4(data, 'china %s' % varible)

# into_5_day('air')
#
# ues('air 100')
# ues('air 200')
# ues('air 500')
# ues('air 850')
#
#
# from nc4 import read_nc4,save_nc4
# # pre = read_nc4(r'D:\basis\fd\data\china rpre')
# # print(pre.shape)
# et = read_nc4(r'D:\basis\fd\data\china rpre')
# et[np.where(et < 0)] = np.nan
# # et = et * 5* 24*60*60
# print(et[np.where(et>0.002)])
# save_nc4(et, r'D:\basis\fd\data\china1979 rpre')

# print(read_nc4(r'D:\basis\fd\data\china1979 pre'))
#
# a = pd.read_csv(r'D:\basis\fd\datafram.csv', index_col=0)
# print(a[0:3])


# def ss(variablename):
#     data = read_nc4(r'D:\basis\fd\data\china %s' % variablename)
#     data = data[1314:,]
#     print(data.shape)
#     save_nc4(data, r'D:\basis\fd\data\china1979 %s' % variablename)

'''change unit, kg/m2/s into pentad accumulation'''
def accumulation(name):
    import numpy as np
    import pandas as pd
    from netCDF4 import Dataset
    from nc4 import read_nc4, save_nc4
    variable = read_nc4(r'D:\basis\fd\data\china %s 2000' %name, name)
    variable[np.where(variable == -9999)] = np.nan
    variable = variable * 10800 * 8 * 5
    print(np.nanmax(variable), np.nanmin(variable))
    save_nc4(variable, r'china %s 2000' %name)

def wm2_to_mm(name):
    import numpy as np
    import pandas as pd
    from netCDF4 import Dataset
    from nc4 import read_nc4, save_nc4
    variable = read_nc4(r'D:\basis\fd\data\china %s 2000' %name, name)
    variable[np.where(variable == -9999)] = np.nan
    variable = variable * (0.408*(10**-6))* 10800 * 8 * 5
    print(np.nanmax(variable), np.nanmin(variable))
    save_nc4(variable, r'china %s 2000' %name)

def to_before_nc4(name):
    import numpy as np
    import pandas as pd
    from netCDF4 import Dataset
    from nc4 import read_nc4, save_nc4
    variable = read_nc4(r'D:\basis\fd\data\china %s 2000' %name, name)
    variable[np.where(variable == -9999)] = np.nan
    print(np.nanmax(variable), np.nanmin(variable))
    save_nc4(variable, r'china %s 2000' %name)

# read_name = ['SWdown_f_tavg', 'Wind_f_inst', 'Tair_f_inst', 'Rainf_f_tavg', 'RootMoist_inst',
#              'Lwnet_tavg', 'Swnet_tavg', 'SoilMoi0_10cm_inst', 'Evap_tavg',
#              'PotEvap_tavg', 'Qh_tavg', 'Qle_tavg', 'Qair_f_inst']
# save_name = ['swdown', 'wind', 'tem', 'pre', 'root', 'lw', 'sw', 'sm', 'et', 'pet', 'qh', 'qle', 'qair']
# names = {}
# for i in range(len(read_name)):
#     names[save_name[i]] = read_name[i]

accumulation_list = ['et', 'pre']
wm_list = ['lw', 'sw', 'qle', 'qh', 'pet', 'swdown']
row_list = ['wind', 'tem', 'root', 'sm', 'qair']

# for i in accumulation_list:
#     accumulation(i)
# for j in wm_list:
#     fuck(j)
# for i in row_list:
#     to_before_nc4(i)


'''change into percentile'''
def changeinto_percentile(name):
    def percentile_in_array(data):
        def calc_percentile(a, method='min'):
            # 获得传入序列的百分位值
            from scipy.stats import rankdata
            import numpy as np
            if isinstance(a, list):
                a = np.asarray(a)
            s = rankdata(a, method=method) / float(len(a))
            result = [round(i, 3) for i in s]
            return result

        osesr_reshapeinyear = np.array(data).reshape([19, 73])
        percentile = np.ones([19, 73])

        for i in range(73):
            begin = osesr_reshapeinyear[:, i]
            sbegin = calc_percentile(begin)
            percentile[:, i] = sbegin
        return np.array(percentile).flatten()


    mask = read_nc4(r'china %s 2000' %name, name)
    mask = np.ma.masked_invalid(mask)
    mask = np.ma.masked_equal(mask, -9999)
    percentile = np.ones(mask.shape)
    for i in range(mask.shape[1]):
        for j in range(mask.shape[2]):
            percentile[:, i, j] = percentile_in_array(mask[:, i, j])
    save_nc4(percentile, r'spercentile china %s 2000' %name)

# save_name = ['swdown', 'wind', 'tem', 'pre', 'root', 'lw', 'sw', 'sm', 'et', 'pet', 'qh', 'qle', 'qair']
# for i in save_name:
#     changeinto_percentile(i)
# changeinto_percentile('china1979 pre')
# changeinto_percentile('china1979 win')
# for i in ['qair', 'rootsoil', 'swdown']:
#     changeinto_percentile('china1979 %s' %i)



'''change into pentad'''
import xarray as xr

# da = xr.open_dataset(r'D:\basis\fd\spi\smi.nc4').sel(time=['2000','2004', '2008','2012','2016'])
def change_pentad_in_leap_year(year):
    da = xr.open_dataset(r'D:\basis\fd\spi\smi.nc4').sel(time=year)
    # drop leap day
    # print(da.isel(time=59).variables['SoilMoi0_10cm_inst'].values)
    # print(da.isel(time=60).variables['SoilMoi0_10cm_inst'].values)

    # lc = da.coords['time']
    # da.loc[dict(time=lc[(lc>=np.datetime64('2000-02-29')) &(lc<np.datetime64('2000-12-31'))])].variables['SoilMoi0_10cm_inst'].values = \
    #     da.loc[dict(time=lc[(lc>np.datetime64('2000-02-29')) &(lc<=np.datetime64('2000-12-31'))])].variables['SoilMoi0_10cm_inst'].values

    a = da.isel(time=slice(60,366)).variables['SoilMoi0_10cm_inst'].values
    b = da.isel(time=slice(0,59)).variables['SoilMoi0_10cm_inst'].values
    data = np.vstack((b,a))
    da = da.isel(time=slice(0,365))
    foo = xr.DataArray(data
                       , coords=[da.time, da.lat, da.lon], dims=['time','lat','lon'])
    da = xr.Dataset({'smi':foo})

    # foo.to_netcdf('smi_noaa.nc4')
    # da.iloc(time=slice(59,365)).variables['SoilMoi0_10cm_inst'].values = da.iloc(time=slice(60,366)).variables['SoilMoi0_10cm_inst'].values
    #
    # print(da.isel(time=59).variables['SoilMoi0_10cm_inst'].values)

    da = da.resample(time="5D").mean(dim="time")
    da = da.sel(time=~((da.time.dt.month == 12) & (da.time.dt.day == 31)))

    da.to_netcdf('%s.nc4'%year)

# for i in ['2000','2004', '2008','2012','2016']:
#     change_pentad_in_leap_year(i)

# rowpentad = xr.open_dataset(r'D:\basis\fd\china sm 2000.nc4').sel(time=slice('2000', '2018'))
# for i in ['2000','2004', '2008','2012','2016']:
#     data = xr.open_dataset('%s.nc4'%i).variables['smi'].values
#     rowpentad['SoilMoi0_10cm_inst'].loc[dict(time=i)] = data
#
#     # rowpentad['time'].loc[dict(time=i)] = rowpentad['time'].loc[dict(time=i)].assign_coords(time=np.array(time).flatten())
#
#     # print(data)
# time = []
# for i in range(2000,2019):
#     month = [ 1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  3,  3,  3,  3,  3,
#         3,  4,  4,  4,  4,  4,  4,  5,  5,  5,  5,  5,  5,  5,  6,  6,  6,
#         6,  6,  6,  7,  7,  7,  7,  7,  7,  8,  8,  8,  8,  8,  8,  9,  9,
#         9,  9,  9,  9, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 12,
#        12, 12, 12, 12, 12]
#     day = [1,  6, 11, 16, 21, 26, 31,  5, 10, 15, 20, 25,  2,  7, 12, 17, 22,
#        27,  1,  6, 11, 16, 21, 26,  1,  6, 11, 16, 21, 26, 31,  5, 10, 15,
#        20, 25, 30,  5, 10, 15, 20, 25, 30,  4,  9, 14, 19, 24, 29,  3,  8,
#        13, 18, 23, 28,  3,  8, 13, 18, 23, 28,  2,  7, 12, 17, 22, 27,  2,
#         7, 12, 17, 22, 27]
#
#     import datetime
#     for ss in range(73):
#         time.append(datetime.datetime(i, month[ss], day[ss]))
# rowpentad = rowpentad.assign_coords(time=np.array(time).flatten())
# rowpentad.to_netcdf('pentad_sm.nc4')

# da = xr.open_dataset(r'D:\basis\fd\spi\smi.nc4')
# # print(da)
# da = da.resample(time="1M").mean(dim="time")
# print(da)
# da.to_netcdf('monthly_sm.nc4')

rowpentad = xr.open_dataset(r'D:\basis\fd\spi\smi.nc4')
print(rowpentad)