# nc
from netCDF4 import Dataset
a = Dataset(r'D:\basis\fd\GLDAS\GLDAS_NOAH025_3H.A20000102.0000.021.nc4', 'r')
print(a.variables)







from nc4 import save_nc4, read_nc4
# 重新拼接需要的区域

i = 0
data = read_nc4('mask %i %i' % (i, 0))
print(data.shape)
for j in range(0,5):
    sdata = read_nc4('mask %i %i' % (i, j+1))
    data = np.concatenate((data, sdata), axis=2)

i = 1
data1 = read_nc4('mask %i %i' % (i, 0))
for j in range(0,5):
    sdata = read_nc4('mask %i %i' % (i, j+1))
    data1 = np.concatenate((data1, sdata), axis=2)

i = 2
data2 = read_nc4('mask %i %i' % (i, 0))
for j in range(0,5):
    sdata = read_nc4('mask %i %i' % (i, j+1))
    data2 = np.concatenate((data2, sdata), axis=2)

i = 3
data3 = read_nc4('mask %i %i' % (i, 0))
for j in range(0,5):
    sdata = read_nc4('mask %i %i' % (i, j+1))
    data3 = np.concatenate((data3, sdata), axis=2)


data = np.concatenate((data, data1), axis=1)
data = np.concatenate((data, data2), axis=1)
data = np.concatenate((data, data3), axis=1)
save_nc4(data, 'mask')
print(data.shape)




et = read_nc4('china et')
set = cal_area_mean(et, area)
pet = read_nc4('china pet')
spet = cal_area_mean(pet, area)
mask = read_nc4('mask area')

day_index = list(range(0, 365, 5))
daylen = [28,31,30,31,30,31,31,30,31,30,31]
sday = list(range(1,32))
smonth = list(np.tile(1, 31))
for s, i in enumerate(daylen):
    sday.extend(list(range(1, i+1)))
    smonth.extend(np.tile(s+2, i))

sday = np.array(sday)
day = np.tile(sday[day_index], 58)
smonth = np.array(smonth)
month = np.tile(smonth[day_index], 58)
year = np.arange(1961, 2019).repeat(73)
from pandas import DataFrame
import pandas as pd
data = DataFrame({'year':year, 'month':month, 'day':day})
datatime = pd.to_datetime(data['year'] * 10000 + data['month'] * 100 + data['day'], format='%Y%m%d')

datas = DataFrame({'mask': mask, 'et':set, 'pet':spet}, index=datatime)
print(datas)
datas.to_csv('datas.csv')



# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from netCDF4 import Dataset
from pandas import DataFrame

def plotresult(result, varible_name, savename, a=None, ylim=None):
    def cal_anomaly(data):
        ano = data - np.mean(data)
        return ano
    result = result.groupby(pd.Grouper(freq='5D')).mean()
    result[varible_name] = cal_anomaly(np.array(result[varible_name]))
    result[varible_name][result[varible_name]>result[varible_name].std()] = 1
    result[varible_name][result[varible_name] != 1] = 0
    import plot as pl
    import matplotlib.pyplot as plt
    if varible_name == 'pre':
        stds = result[varible_name].std()
        if a is not None:
            a['grey'] = [(0, -stds), (0, stds), (result.index.argmax(), stds), (result.index.argmax(), -stds), (0, -stds)]
    if varible_name == 'sm':
        percentiles = np.percentile(np.array(result[varible_name]),30)
        if a is not None:
            a['grey'] = [(0, percentiles), (0, result[varible_name].max()), (result.index.argmax(), result[varible_name].max()), (result.index.argmax(), percentiles), (0, percentiles)]

    pl.plot_bar(plt.subplot(111), list(range(len(result.index))), result.index.year.map(str)+'-'+result.index.month.map(str)
                , np.array(result[varible_name]),
                savename=savename, set_blank=a, ylim=ylim)


# find 2016 data to plot a temporal map
# varible_name=['pre', 'tem']
# varible = pd.read_csv(r'pre.csv', index_col=0)
# tem = pd.read_csv(r'tem.csv', index_col=0)
# varible['tem'] = np.array(tem['tem'])
# varible = varible.set_index(pd.to_datetime(varible.index))
# result = varible.groupby(varible.index).mean().reset_index().set_index('index')
# del result.index.name

result = pd.read_csv('datas.csv', index_col=0)
result = result.set_index(pd.to_datetime(result.index))
a = None

import plot as pl
import matplotlib.pyplot as plt

varible_name = 'pet'
savename = 'save'
ylim = None
result['pet'][result['mask'] == 0] = 0
pl.plot_bar(plt.subplot(111), list(range(len(result.index))),
            result.index.year.map(str) + '-' + result.index.month.map(str)
            , np.array(result[varible_name]),
            savename=savename, set_blank=a, ylim=ylim)










# 对蒸发数据的处理
import numpy as np
import pandas as pd


def fit_line2(x, y):
    from scipy.stats import linregress
    from scipy.stats.mstats import zscore
    (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(zscore(x), zscore(y))
    return beta_coeff


# data = pd.read_csv(r'D:\basis\data\row\EVP.txt', delim_whitespace=True, header=None,
#                               index_col=None, usecols=[0, 4, 5, 6, 7, 8],
#                               names=['station', 'year', 'month', 'day', 'min', 'max'])
# # data = pd.read_csv('data10000.csv', index_col=None)
# datatime = pd.to_datetime(data['year'] * 10000 + data['month'] * 100 + data['day'], format='%Y%m%d')
# data = data.set_index(datatime)
# data = data.drop(['year','month', 'day'], axis=1)
# data['min'][data['min'] == 32766] = np.nan
# data['max'][data['max'] == 32766] = np.nan
# data.to_csv('data.csv')

# datatime = pd.to_datetime(data.index)
# data = data.set_index(datatime)

#######￥￥####################
# data = pd.read_csv('data.csv', index_col=1)
# def counts(data):
#     names = data.name
#     data = data.dropna()
#     a,b = data.count()
#     if a > 200 and b > 200:
#         return
#     else:
#         dellist.append(names)
# dellist = []
# data.drop('Unnamed: 0',axis=1).groupby(data.index).apply(lambda a: counts(a))
# data = data.drop(dellist)
# np.savetxt('dellist.txt', dellist, fmt='%i')
# data.to_csv('compare.csv')


data = pd.read_csv('compare.csv', index_col=1)
del data.index.name
def change(data):
    names = data.name
    sdata = data.dropna()
    a = fit_line2(np.array(sdata['min']), np.array(sdata['max']))
    data.loc[data['max'].isnull(),'max'] = data[data['max'].isnull()]['min'] * a
    return data['max']

wronglist = []
ss = data.groupby(data['station']).apply(lambda a: change(a)).T.reindex()

np.savetxt('wronglist.txt', wronglist, fmt='%i')
ss.to_csv('result.csv')



def plotcors(subplotsize, data, oslon, oslat, share_levels=False, labels=None, ranges=([89.5, -89.5, 0.5, 359.5], [89.5, -89.5], [0.5, 359.5])):
    import cartopy.crs as ccrs
    import matplotlib.pyplot as plt
    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
    from matplotlib import colors
    from copy import copy
    from cartopy.mpl.gridliner import LATITUDE_FORMATTER, LONGITUDE_FORMATTER
    import shapely.geometry as sgeom
    from scipy.interpolate import Rbf, RegularGridInterpolator
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader

    def plot_single_ax(ax, levels):
        fill = ax.contourf(oslon, oslat, data, levels=levels, transform=ccrs.PlateCarree(), colormaps=tmap)
        cbar = plt.colorbar(fill, orientation='horizontal')
        # cbar.set_ticks(levels)
        # cbar.set_ticklabels(levels)
        # clevels = []
        # filllevels = fill.levels
        # for i in range(0, len(filllevels), 1):
        #     clevels.append(filllevels[i])
        # ax.clabel(fill, clevels, fmt='%6.1f')
        # ax.coastlines()


        # 设置坐标轴经纬度标志
        ax.set_extent((ranges[0][2], ranges[0][3], ranges[0][0], ranges[0][1]), crs=ccrs.PlateCarree())
        ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
        ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        # 添加网格线
        ax.gridlines(color='black', linestyle='--', alpha=0.4)

        if labels is not None:
            ax.text(ranges[0][2],56, labels,
                     transform=ccrs.PlateCarree())

        # # #添加shp文件进入图片中
        pathss = shp2clip(r'D:\basis\data\shp\shengjie\shengjie.shp')
        plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
        upath = PathPatch(pathss, transform=plate_carre_data_transform)
        for collection in fill.collections:
            collection.set_clip_path(upath)
        #
        shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\shengjie\bou2_4p.shp').geometries(), ccrs.PlateCarree(),
                                       edgecolor='k',facecolor='none', linewidths=0.55)
        ax.add_feature(shape_feature)

    # 适用于data画曲线图、r2画显著性的图
    colormap = ['#ab0f45', '#de4c4b', '#f06744', '#f7814c', '#fca55d', '#fec877', '#fee18d','#fdfebb', '#ffffff', '#fdfebb', '#ddf19a', '#bce4a0',
                '#a2d9a4', '#74c7a5', '#5cb7aa', '#3b92b9', '#555aa7']
    tmap = colors.LinearSegmentedColormap.from_list('cmap', colormap)
    fig = plt.figure()
    # plt.subplots_adjust(left=0.07, bottom=0.04, right=0.97, top=0.99, wspace=0.2, hspace=0.2)
    for i in range(1, subplotsize[0]*subplotsize[1]+1):
        ax = fig.add_subplot(subplotsize[0], subplotsize[1], i, projection=ccrs.PlateCarree())
        plot_single_ax(ax)

    return



def plotcors(ax, data, oslon, oslat, levels=None ,labels=None, ranges=[[89.5, -89.5, 0.5, 359.5], [89.5, -89.5], [0.5, 359.5]]
             , Ldensity=8):
    import cartopy.crs as ccrs
    import matplotlib.pyplot as plt
    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
    from matplotlib import colors
    from copy import copy
    from cartopy.mpl.gridliner import LATITUDE_FORMATTER, LONGITUDE_FORMATTER
    import shapely.geometry as sgeom
    from scipy.interpolate import Rbf, RegularGridInterpolator
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader
    # 适用于data画曲线图、r2画显著性的图
    colormap = ['#ab0f45', '#de4c4b', '#f06744', '#f7814c', '#fca55d', '#fec877', '#fee18d','#fdfebb', '#ffffff', '#fdfebb', '#ddf19a', '#bce4a0',
                '#a2d9a4', '#74c7a5', '#5cb7aa', '#3b92b9', '#555aa7']
    tmap = colors.LinearSegmentedColormap.from_list('cmap', colormap)

    fill = ax.contourf(oslon, oslat, data, transform=ccrs.PlateCarree(), colormaps=tmap)
    cbar = plt.colorbar(fill, orientation='horizontal')
    # cbar.set_ticks(levels)
    # cbar.set_ticklabels(levels)
    # clevels = []
    # filllevels = fill.levels
    # for i in range(0, len(filllevels), 1):
    #     clevels.append(filllevels[i])
    # ax.clabel(fill, clevels, fmt='%6.1f')
    # ax.coastlines()


    # 设置坐标轴经纬度标志
    ax.set_extent((ranges[0][2], ranges[0][3], ranges[0][0], ranges[0][1]), crs=ccrs.PlateCarree())
    ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
    ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())
    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()
    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)
    # 添加网格线
    ax.gridlines(color='black', linestyle='--', alpha=0.4)

    if labels is not None:
        ax.text(ranges[0][2],56, labels,
                 transform=ccrs.PlateCarree())

    # # #添加shp文件进入图片中

    pathss = shp2clip(r'D:\basis\data\shp\shengjie\shengjie.shp')
    plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
    upath = PathPatch(pathss, transform=plate_carre_data_transform)
    for collection in fill.collections:
        collection.set_clip_path(upath)
    #
    shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\shengjie\bou2_4p.shp').geometries(), ccrs.PlateCarree(),
                                   edgecolor='k',facecolor='none', linewidths=0.55)
    ax.add_feature(shape_feature)

    return fill


et = read_nc4(r'D:\basis\fd\data\china hgt 100')
from current_function_in_fd import time_del
data = time_del(['2011-08-09', '1980-09-03'])
print(data)
print(et.shape)
plothgt([[et[data[0]], et[data[0]+1], et[data[0]+2]], [et[data[0]+3],et[data[0]+4],et[data[0]+5]]],
        allarea[0], [100,100,100], ['1', '2', '3'])
import matplotlib.pyplot as plt
plt.show()