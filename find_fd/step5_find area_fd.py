import step3_function_find_fd_use_oktin as fff
from nc4 import read_nc4, save_nc4
import numpy as np
import pandas as pd

'''计算全部区域单点的mask'''

def del_ocean_fd(mask):
    lat_partition = [312, 340, 380, 420, 456]
    lon_partition = [1008, 1060, 1100, 1140, 1180, 1220, 1260]
    lat = np.loadtxt('lat.csv')
    lon = np.loadtxt('lon.csv')
    slat = lat[lat_partition[0]:lat_partition[4]]
    slon = lon[lon_partition[0]:lon_partition[6]]
    lon_grid, lat_grid = np.meshgrid(slon, slat)
    # 将海洋上错误的闪旱判断设为0
    from global_land_mask import globe
    globe_land_mask = globe.is_ocean(lat_grid, lon_grid)
    for i in range(mask.shape[0]):
        mask[i][globe_land_mask] = -9999.0
    return mask


def cal_area_mean(data, area):
    # lat_partition = [312, 340, 380, 420, 456]
    # lon_partition = [1008, 1060, 1100, 1140, 1180, 1220, 1260]
    # lat = np.loadtxt(r'D:\basis\fd\data\lat.csv')
    # lon = np.loadtxt(r'D:\basis\fd\data\lon.csv')
    lat = read_nc4(r'D:\basis\fd\data\china lat')
    lon = read_nc4(r'D:\basis\fd\data\china lon')
    lat0 = int(np.argwhere(lat[:] == area[0]))
    lat1 = int(np.argwhere(lat[:] == area[1]))
    lon0 = int(np.argwhere(lon[:] == area[2]))
    lon1 = int(np.argwhere(lon[:] == area[3]))

    # 将缺省值mask
    data = np.array(data)
    # data = del_ocean_fd(data)
    data[np.where(data == -9999.0)] = np.nan
    data = np.ma.masked_invalid(data)

    # 将海洋值mask
    meandata = np.nanmean(data[:, lat0:lat1, lon0:lon1], axis=(1, 2))
    return meandata

def cal_area_sum(data, area):
    # lat_partition = [312, 340, 380, 420, 456]
    # lon_partition = [1008, 1060, 1100, 1140, 1180, 1220, 1260]
    # lat = np.loadtxt(r'D:\basis\fd\data\lat.csv')
    # lon = np.loadtxt(r'D:\basis\fd\data\lon.csv')
    lat = read_nc4(r'D:\basis\fd\data\china lat')
    lon = read_nc4(r'D:\basis\fd\data\china lon')
    lat0 = int(np.argwhere(lat[:] == area[0]))
    lat1 = int(np.argwhere(lat[:] == area[1]))
    lon0 = int(np.argwhere(lon[:] == area[2]))
    lon1 = int(np.argwhere(lon[:] == area[3]))

    # 将缺省值mask
    data = np.array(data)
    # data = del_ocean_fd(data)
    data[np.where(data == -9999.0)] = np.nan
    data = np.ma.masked_invalid(data)

    # 将海洋值mask
    sumdata = np.nansum(data[:, lat0:lat1, lon0:lon1], axis=(1, 2))
    return sumdata


def find_area_fd(area, beginyear=''):
    '''计算区域mask时候使用的
    要求已经有了给定的区域范围，最后给出mask(类似单点mask)'''
    et = read_nc4(r'D:\basis\fd\data\china et 2000')
    pet = read_nc4(r'D:\basis\fd\data\china pet 2000')
    et = np.ma.masked_equal(et, -9999.0)
    pet = np.ma.masked_equal(pet, -9999.0)

    set = cal_area_mean(et, area)
    spet = cal_area_mean(pet, area)

    rses, percentile_rses, orses, percentile_orses = fff.do_cal_in_onepoint(set, spet)
    aaa = [rses, percentile_rses, orses, percentile_orses]
    mask, percentile25 = fff.do_cri_in_onepoint(rses, percentile_rses, orses, percentile_orses)
    return aaa, mask, percentile25

# find_area_fd([24.125, 33.125, 100.125, 110.125])


def del_fd_in410(mask_with_datetime):
    import pandas as pd
    mask = np.array(mask_with_datetime['mask'])
    begin = np.argwhere(mask[:] == 1).flatten()
    end = np.argwhere(mask[:] == 3).flatten()
    for i in range(len(begin)):
        eva_begin = mask_with_datetime.index[begin[i]].month
        eva_end = mask_with_datetime.index[end[i]].month
        if (np.isin(eva_begin, np.array([1,2,3,11,12])) | np.isin(eva_end, np.array([1,2,3,11,12]))) == True:
            mask_with_datetime['mask'].iloc[begin[i]:end[i]+1] = 0
    return mask_with_datetime

### 仅为简化运行
def area_fd(area, areaname, beginyear=''):

    aaa, mask, percentile25 = find_area_fd(area, beginyear=beginyear)

    et = read_nc4(r'D:\basis\fd\data\china et 2000')
    set = cal_area_mean(et, area)
    pet = read_nc4(r'D:\basis\fd\data\china pet 2000')
    spet = cal_area_mean(pet, area)
    sm = read_nc4(r'D:\basis\fd\data\china sm 2000')
    ssm = cal_area_mean(sm, area)
    tem = read_nc4(r'D:\basis\fd\data\china tem 2000')
    stem = cal_area_mean(tem, area)
    qh = read_nc4(r'D:\basis\fd\data\china qh 2000')
    sqh = cal_area_mean(qh, area)
    qle = read_nc4(r'D:\basis\fd\data\china qle 2000')
    sqle = cal_area_mean(qle, area)
    # pressure = read_nc4(r'D:\basis\fd\data\china%s pressure'   )
    # spressure = cal_area_mean(pressure, area)
    soil = read_nc4(r'D:\basis\fd\data\china root 2000')
    ssoil = cal_area_mean(soil, area)
    pre = read_nc4(r'D:\basis\fd\data\china pre 2000')
    spre = cal_area_mean(pre, area)
    swdown = read_nc4(r'D:\basis\fd\data\china swdown 2000')
    sswdown = cal_area_mean(swdown, area)
    qair = read_nc4(r'D:\basis\fd\data\china qair 2000')
    sqair = cal_area_mean(qair, area)
    
    win = read_nc4(r'D:\basis\fd\data\china wind 2000')
    swin = cal_area_mean(win, area)

    lw = read_nc4(r'D:\basis\fd\data\china lw 2000')
    slw = cal_area_mean(lw, area)
    sw = read_nc4(r'D:\basis\fd\data\china sw 2000')
    ssw = cal_area_mean(sw, area)
    import pandas as pd
    # st_pre = pd.read_csv(r'D:\basis\fd\data\pre_%s.csv' % areaname, index_col=0, parse_dates=True)

    day_index = list(range(0, 365, 5))
    daylen = [28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    sday = list(range(1, 32))
    smonth = list(np.tile(1, 31))
    for s, i in enumerate(daylen):
        sday.extend(list(range(1, i + 1)))
        smonth.extend(np.tile(s + 2, i))


    lenyear = 19
    sday = np.array(sday)
    day = np.tile(sday[day_index], lenyear)
    smonth = np.array(smonth)
    month = np.tile(smonth[day_index], lenyear)
    year = np.arange(int(2000), 2019).repeat(73)
    from pandas import DataFrame
    import pandas as pd

    data = DataFrame({'year': year, 'month': month, 'day': day})
    datatime = pd.to_datetime(data['year'] * 10000 + data['month'] * 100 + data['day'], format='%Y%m%d')

    datas = DataFrame({'mask': mask, 'et': set, 'pet': spet, 'rses':aaa[0], 'percentile_rses':aaa[1], 'orses':aaa[2], 'percentile_orses':aaa[3], 'percentile25':percentile25,
                       'sm':ssm, 'tem':stem, 'qh':sqh, 'qle':sqle,
                       'mpre':spre,'rootsoil':ssoil, 'swdown':sswdown, 'qair':sqair, 'wind':swin,
                       'lw':slw, 'sw':ssw}, index=datatime)
    datas = del_fd_in410(datas)
    # st_pre = st_pre.reindex(datas.index)
    # datas = pd.concat([datas, st_pre],axis=1)
    datas.to_csv(r'D:\basis\fd\data\datavarible%s %s.csv' % (beginyear, areaname))

def list_fd_into_season(area_name):
    import pandas as pd
    from pandas import DataFrame
    data = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv' % area_name, index_col=0, parse_dates=True)
    sdata = data['mask'][data['mask'] == 1]

    smonth45 = sdata[(sdata.index.month == 4) | (sdata.index.month == 5)]
    smonth678 = sdata[(sdata.index.month == 6) | (sdata.index.month == 7) | (sdata.index.month == 8)]
    smonth910 = sdata[(sdata.index.month == 9) | (sdata.index.month == 10)]

    sdata = data['mask'][data['mask'] == 3]

    month45 = sdata[(sdata.index.month == 4) | (sdata.index.month == 5)]
    month678 = sdata[(sdata.index.month == 6) | (sdata.index.month == 7) | (sdata.index.month == 8)]
    month910 = sdata[(sdata.index.month == 9) | (sdata.index.month == 10)]
    information = DataFrame({'smonth45':smonth45, 'smonth678':smonth678, 'smonth910':smonth910, 'emonth45':month45, 'emonth678':month678, 'emonth910':month910})
    information.to_csv('fd_time_%s.csv' % area_name)

if __name__ == '__main__':
    area_name = ['xinan', 'dongbei', 'xinjiang']
    allarea = [[24.125, 33.125, 102.125, 108.125],
               [40.125, 50.125, 122.125, 128.125],
               [36.125,47.125,87.125,98.125]]

    for i in range(3):
        # area_fd(allarea[i], area_name[i], beginyear='1979')
        list_fd_into_season(area_name[i])

    # # et = read_nc4(r'D:\basis\fd\data\china1979 et')
    # # set = cal_area_mean(et, allarea[0])
    # # print(set.shape)
    # # from pandas import DataFrame
    # # DataFrame(set).to_csv('ssd .csv')


