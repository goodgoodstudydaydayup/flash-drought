import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from pandas import DataFrame


'''data: np.array, len(data)等于ey-sy+1'''
def mk_analysis(ax, data, sy, ey):
    # 单个z计算
    def mkz(data):
        n = len(data)
        s = 0
        for k in range(n - 1):
            for j in range(k + 1, n):
                if data[j] > data[k]:
                    s = s + 1
                else:
                    s = s + 0
        if s != 0:
            es = n * (n - 1) / 4.0
            var = n * (n - 1) * (2 * n + 5) / 72.0
            z = (s - es) / (var ** 0.5)
        else:
            z = 0
            es = 111111
            var = 111111
        return z

    def plot(ax, time, uf, ub):
        ax.plot(time, uf, label='UF', color='k', linestyle='-.', marker='o')
        ax.plot(time, ub, label='UB', color='k', linestyle='--', marker='v', alpha=0.5)

        ax.axhline(0, color='k')
        ax.axhline(1.96, color='k', linestyle='--', alpha=0.5, label='α=0.05')
        ax.axhline(-1.96, color='k', linestyle='--', alpha=0.5)
        plt.legend()
        ax.text(ax.get_xlim()[0], ax.get_ylim()[1], 'b) MK mutation')
        # ax.set_ylabel('MK test')
        # ax.set_xlabel('年')
        # ax.set_xticks([time[0], range(time[1], time[-1], 2)], time[-1])
        # plt.savefig('%s.png' % (type1 + ' ' + level + ' ' + season))
        return

    n = len(data)
    uf = np.empty(n-1)
    ub = np.empty(n-1)
    ub_data = data[::-1]
    for k in range(1, n):
        uf[k-1] = mkz(data[0:k+1])
        ub[k-1] = mkz(ub_data[0:k+1])
    ub = -ub[::-1]
    a = plot(ax, range(sy+1, ey+1), uf, ub)
    return





if __name__ == "__main__":
    data = pd.read_csv(r'D:\basis\changjiang\c_month[7, 8, 9].csv', index_col=[0])
    ave = data.groupby('year').mean()
    np.savetxt('testdata.txt', np.array(ave['pre']))

    # mk_analysis(plt.subplot(111), np.array(ave['pre']), 1961, 2011)
    # plt.show()


        


