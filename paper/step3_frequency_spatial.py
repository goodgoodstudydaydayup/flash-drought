import numpy as np
import pandas as pd
import xarray as xr
from nc4 import read_nc4, save_nc4
from scipy.stats import pearsonr
from scipy.stats import skew
from plot_new_method import plot_single_ax

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

# def cal_duration(mask):
#     duration = np.ones(mask.shape[1:3])
#     for i in range(mask.shape[1]):
#         for j in range(mask.shape[2]):
#             smask = mask[:, i, j]
#
#             len_of_fd = len(np.argwhere(smask > 0).flatten())
#             number_of_fd = len(np.argwhere(smask == 1).flatten())
#
#             if number_of_fd == 0:
#                 duration[i,j] = np.nan
#             else:
#                 a = len_of_fd/number_of_fd
#                 if a < 5:
#                     print(len_of_fd, number_of_fd)
#                 duration[i, j] = len_of_fd/number_of_fd
#     return duration
#
# def cal_frequency(mask):
#     mask_only_with_fd_begin = np.ma.masked_where(mask != 1, mask)
#     frequency = np.nansum(mask_only_with_fd_begin, axis=0)
#     return frequency
#
# def plot_standard_foc(data, ax, ax_jiuduanxinposition, ranges, title):
#
#     from plot_new_method import plot_single_ax
#     import matplotlib.pyplot as plt
#
#     slat = read_nc4(r'D:\basis\fd\data\china lat')
#     slon = read_nc4(r'D:\basis\fd\data\china lon')
#
#     fill, cbar = plot_single_ax(ax,
#                    [
#                     [data, 'contourf', 'zero']
#                    ],
#                    oslat=slat, oslon=slon, clipshape=True, gridline=True, colorbar='v',
#                           ranges=[[55,15, 70, 138],[20,30,40,50],[80,90,100,110,120,130]])
#     ax_jiuduanxian = fig.add_axes(ax_jiuduanxinposition, projection=ccrs.PlateCarree())
#     plot_single_ax(ax_jiuduanxian,
#                    [
#                     [data, 'contourf', np.arange(0, 20, 2.5)]
#                    ],
#                    oslat=slat, oslon=slon, clipshape=True, gridline=False, shplinewidth=1.5,
#                           ranges=[[25,0, 105, 125],[],[]])
#
#     def areas(area, color, label):
#         x1 = area[0]
#         x2 = area[1]
#         y1 = area[2]
#         y2 = area[3]
#         area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
#         ax.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color=color, label=label)
#     # areas(allarea[0], 'red', 'Yangtz-River')
#     areas(allarea[0], 'blue', 'Southwest')
#     areas(allarea[1], 'green', 'Northeast')
#     areas(allarea[2], 'black', '新疆')
#     # ax.legend(loc='upper left')
#     ax.set_title(title)
#
#     # cbar.set_ticks(ranges[::2])
#     # cbar.set_ticklabels([int(i) for i in ranges[::2]])
#     return fill
#
# lats = read_nc4(r'D:\basis\fd\data\china lat')
# lons = read_nc4(r'D:\basis\fd\data\china lon')
#
#
# fig, ax = plt.subplots(1,1, sharex='col', sharey='row',figsize=(9,5), subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
# plt.subplots_adjust(top=0.965,
# bottom=0.05,
# left=0.06,
# right=0.94,
# hspace=0.2,
# wspace=0.2)
# dataname = ['mask410', 'maskspring', 'masksummer', 'maskfall']
# ax_position = [[0.362, 0.563,0.08,0.12],
#                [0.845, 0.563,0.08,0.12],
#                [0.362,0.062,0.08,0.12],
#                [0.845,0.062,0.08,0.12]]
# # ranges=[np.arange(5,10,0.5),
# #         np.arange(5,10,0.5),
# #         np.arange(5,10,0.5),
# #         np.arange(5,10,0.5)]
#
# ranges=[np.arange(0,6.5,0.5),
#         np.arange(0,4,0.5),
#         np.arange(0,4,0.5),
#         np.arange(0,3,0.5)]
#
# allarea = [[24.125, 33.125, 102.125, 108.125],
#            [40.125, 50.125, 122.125, 128.125],
#            [36, 47, 87, 98]]
#
# fill = plot_standard_foc(cal_frequency(read_nc4(r'D:\basis\fd\paper\edcf_mask410')),
#                          ax, ax_position[0], [0,10], 'frequency')
# title = ['a) duration', 'b) 持续时间(侯) 春季', 'c) 持续时间(侯) 夏季', 'd) 持续时间(侯) 秋季']
# # for i in range(2):
# #     for j in range(2):
# #         count = i*2+j
# #         fill = plot_standard_foc(cal_duration(read_nc4(r'D:\basis\fd\data\%s'%dataname[count])), ax[i][j],
# #                                  ax_position[count], ranges[count], title[count])
# plt.show()


'''绘制单图'''
def plot_standard_foc(mask, ax, ax2, duration=False, ranges=None):

    if duration is False:
        mask = np.ma.masked_where(mask != 1, mask)
        foc = np.sum(mask, axis=0)
    else:
        foc = mask
    from plot_in_proplot import plot_single_ax
    import matplotlib.pyplot as plt
    if ranges is None:
        ranges=np.arange(np.nanmin(foc), np.nanmax(foc),1)
    slat = read_nc4(r'D:\basis\fd\data\china lat')
    slon = read_nc4(r'D:\basis\fd\data\china lon')

    fill = plot_single_ax(ax,
                   [
                    [foc, 'contourf', ranges]
                   ],
                   oslat=slat, oslon=slon, clipshape=True, gridline=True, colorbar=False,
                          ranges=[[57,15, 70, 138],[20,30,40,50],[80, 90,100, 110,120, 130]])

    from plot_new_method import plot_single_ax
    plot_single_ax(ax2,
                   [
                    [foc, 'contourf', ranges]
                   ],
                   oslat=slat, oslon=slon, clipshape=True, gridline=False, shplinewidth=1.5,cutdata=True,
                          ranges=[[25,2.5, 105, 122.5],[],[]])
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.85,0,0.2,0.2])
    ax2.set_axes_locator(ip)
    # ax.format(lonlabels='t')
    cb = ax.colorbar(fill, loc='b', length=0.7, width=0.1, space='1.8em')

    return fill
    # plt.savefig('figure3.png', dpi=800,bbox_inches='tight')`

def plot_standard_foc_smooth(mask, ax, ax2, ranges, title):
    mask = np.ma.masked_where(mask != 1, mask)
    foc = np.sum(mask, axis=0)
    from scipy.ndimage import filters
    from astropy.convolution import convolve
    from astropy.convolution import Gaussian2DKernel
    kernel = Gaussian2DKernel(x_stddev=2, y_stddev=2)
    foc = convolve(foc, kernel)

    # foc = read_nc4('ss')
    # print(foc)
    from plot_in_proplot import plot_single_ax
    import matplotlib.pyplot as plt

    slat = read_nc4(r'D:\basis\fd\data\china lat')
    slon = read_nc4(r'D:\basis\fd\data\china lon')

    fill = plot_single_ax(ax,
                   [
                    [foc, 'contourf', np.arange(0, np.nanmax(foc),1)]
                   ],
                   oslat=slat, oslon=slon, clipshape=True, gridline=True, colorbar=False,
                          ranges=[[57,15, 70, 138],[20,30,40,50],[80, 90,100, 110,120, 130]])

    from plot_new_method import plot_single_ax
    plot_single_ax(ax2,
                   [
                    [foc, 'contourf', np.arange(0, np.nanmax(foc),1)]
                   ],
                   oslat=slat, oslon=slon, clipshape=True, gridline=False, shplinewidth=1.5,cutdata=True,
                          ranges=[[25,2.5, 105, 122.5],[],[]])
    from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
    ip = InsetPosition(ax, [0.85,0,0.2,0.2])
    ax2.set_axes_locator(ip)
    def areas(area, color, label):
        x1,x2,y1,y2 = area
        area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
        le = ax.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2,
                     color=color, label=label, legend='uc', legend_kw={'ncols': 5, 'frame': False})
        return le

    areas(allarea[0], 'black', 'SW')
    areas(allarea[1], 'green', 'NE')
    le1 = areas(allarea[2], 'red', 'NW')
    le = areas(allarea[3], 'brown', 'MRYR')
    # areas([30.125,33.125,117.125,121.125], 'black','LRYR')
    ax.colorbar(fill, loc='b', length=0.7,width=0.1, space='1.8em')
    # cbar.set_ticks([0,1,2,3,4,5])
    # cbar.set_ticklabels([str(i) for i in [0,1,2,3,4,5]])
    return fill
    # plt.savefig('figure3.png', dpi=800,bbox_inches='tight')`
import cartopy.crs as ccrs
import proplot as plot
# Really complex grid
import matplotlib.pyplot as plt


fig, ax = plot.subplots(ncols=2, nrows=1, proj=ccrs.PlateCarree(), figsize=[9,3.5])

allarea = [[27.5, 33.125, 104, 108.125],
           [41.125, 47.625, 120.125, 130.125],
           [36, 40, 90, 98],
               [36.125,40.125,109.125,111.125]]


fill = plot_standard_foc(read_nc4(r'D:\basis\fd\paper\data\mask410'),
                         ax[0],fig.add_axes([0.2,0.2,0.2,0.2], projection=ccrs.PlateCarree()),
                         ranges=[0,1,2,3,4,5])
# # plt.show()
#
# fill = plot_standard_foc_smooth(read_nc4(r'D:\basis\fd\paper\data\mask410'),
#                          ax[1],fig.add_axes([0.2,0.5,0.2,0.2], projection=ccrs.PlateCarree()), np.arange(0,5.1, 0.1),
#                                             'Frequency of Flash Drought Occurrence')
fill = plot_standard_foc(read_nc4(r'D:\basis\fd\paper\mask410_p3'),
                         ax[1],fig.add_axes([0.2,0.3,0.2,0.2], projection=ccrs.PlateCarree()))

# mask = read_nc4(r'D:\basis\fd\paper\mask410_p3')
# duration = np.ones(mask.shape[1:3])
# for i in range(mask.shape[1]):
#     for j in range(mask.shape[2]):
#         smask = mask[:, i, j]
#
#         len_of_fd = len(np.argwhere(smask > 0).flatten())
#         number_of_fd = len(np.argwhere(smask == 1).flatten())
#
#         if number_of_fd == 0:
#             duration[i,j] = np.nan
#         else:
#             a = len_of_fd/number_of_fd
#
#             duration[i, j] = len_of_fd/number_of_fd
#
# fill = plot_standard_foc(duration,
#                          ax[1],fig.add_axes([0.2,0.5,0.2,0.2], projection=ccrs.PlateCarree()),
#                          duration=True, ranges=np.arange(4,6.7,0.1))



title = ['6-p fd', 'smoothing 6-p fd', ]
title = ['6-p fd', '4-p fd']
# ax.colorbar(fill)
for i, sax in enumerate(ax):
    sax.format(title=title[i], labelpad=8)
ax.format(abc=True, abcstyle='(a)', abcloc='ul')
allarea = [
[30.125,33.125,117.125,121.125],
    [30.125,34.125,101.125,107.125],
    [22.125,25.125,111.125,116.125],
    [28.125,32.125,92.125,98.125],
    [47.125,53.125,119.125,127.125],
    [34.125,39.125,81.125,88.125],
    [40.125,42.875,93.125,99.875]
]

def areas(area, color, label):
    x1, x2, y1, y2 = area
    area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
    le = ax[1].plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2,
                 color=color, label=label, legend='uc', legend_kw={'ncols': 4, 'frame': False})
    # le = ax[1].plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2,
    #              color=color, label=label)
    return le


areas(allarea[0], 'black', '1')
areas(allarea[1], 'green', '2')
areas(allarea[2], 'red', '3')
areas(allarea[3], 'brown', '4')
areas(allarea[4], 'yellow', '5')
areas(allarea[5], 'black', '6')
areas(allarea[6], 'purple', '7')

plt.show()
fig.savefig('frequency2.jpg', dpi=800)

# cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
# # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
# cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
# cb_ax.set_title('次')