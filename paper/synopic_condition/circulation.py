import numpy as np
import xarray as xr
from plot_in_proplot import plot_single_ax
import cartopy.crs as ccrs
from nc4 import read_nc4, save_nc4
import matplotlib.pyplot as plt
from q_vint import moisture_flux, find_rea2_data
import pandas as pd

'''Framework:
1) find the time_range of fd
2) find the circulation and moisture flux data of given time_range
3) plot cir_data'''

def time_range_of_fd(area_name):
    allmask = pd.read_csv(r'D:\basis\fd\paper\datavarible1979 %s.csv' % area_name, index_col=0, parse_dates=True)
    mask = np.array(allmask['mask'])

    begin = np.argwhere(mask[:] == 1).flatten()
    end = np.argwhere(mask[:] == 3).flatten()
    begin = allmask.index[begin]
    end = allmask.index[end]

    return begin, end

def the_circulation_and_moisture_data_of_given_time_range(begin_fd, end_fd):
    # input begin and end in one area
    a = []
    b = []
    # for moisture flux
    mf_all = xr.open_dataset(r'D:\basis\data\rea2\mfd_rea2.nc')
    for i in range(len(begin_fd)):
        start = str(begin_fd[i])[:10]
        end = str(end_fd[i])[:10]
        data, lat, lon = find_rea2_data(500, [start, end])
        mf = mf_all.sel(time=slice(start, end)).variables['mfd'].mean(dim='time')
        a.append(data)
        b.append(mf)
    return np.nanmean(np.array(a), axis=0), np.nanmean(np.array(b), axis=0), lat, lon


def plot_data(axs, data, mfdata, lats, lons, level, area):
    # plot a given pentad hgt, omega and rhum
    # data = [[hgt, hgt_anomaly],[uwnd,anomaly],[vwnd ,anomaly],[omega,anomaly],[rhum ,anomaly]]
    quiverkey = True

    # plot circulation data
    if level == '200':
        fill = plot_single_ax(axs[0],
                              [
                                  [data[0][0], 'contour', 'hgt', 10],
                                  [data[0][1], 'contourf', list(range(-190, 210, 10))],
                                  [data[1][1], 'quiver1', list(np.arange(0, 1.1, 0.1))]
                              ]
                              , lons, lats, ranges=[[60, 0, 60, 140], [60, 40, 20, 0], [80, 120]])
    else:
        if level == '500':
            space = 5
        elif level == '850':
            space = 2
        fill = plot_single_ax(axs[0],
                              [
                                  [data[1][0], 'contour', 'hgt', space],
                                  [data[0][1], 'contourf', list(range(-190, 210, 10))],
                                  [[data[1][1], data[2][1], [1,1]], 'quiver2', list(np.arange(0, 1.1, 0.1))]
                              ]
                              , lons, lats,
                              ranges=[[60, 0, 60, 140], [60, 40, 20, 0], [70, 90, 110, 130]], quiverkey=quiverkey)
    # plot moisture flux data
    fill = plot_single_ax(axs[1],
                          [
                              [mfdata, 'contourf', 'zero']                          ]
                          , lons, lats,
                          ranges=[[60, 0, 60, 140], [60, 40, 20, 0], [70, 90, 110, 130]], quiverkey=quiverkey)

    x1,x2,y1,y2 = area
    area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
    axs[0].plot(area[1], area[0], transform=ccrs.PlateCarree(), color='red')
    axs[1].plot(area[1], area[0], transform=ccrs.PlateCarree(), color='red')
    return fill

def main(choose):

    area_name = ['xinan', 'dongbei', 'xinan', 'MRYR']
    allarea = [[27.625, 33.125, 104.125, 108.125],
               [41.125, 47.625, 120.125, 130.125],
               [36.125, 40.125, 90.125, 98.125],
               [36.125, 40.125, 109.125, 111.125]]

    import proplot as plot
    fig, axs = plot.subplots(ncols=2, nrows=4, proj=ccrs.PlateCarree())

    for i, area in enumerate(area_name):
        begin, end = time_range_of_fd(area)
        data, mfd_data, lat, lon = the_circulation_and_moisture_data_of_given_time_range(begin, end)
        fill =plot_data([axs[i*2],axs[i*2+1]], data, mfd_data, lat, lon, '500', allarea[i])
    plt.show()

main('hgt')

