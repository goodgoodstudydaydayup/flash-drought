import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from pandas import Series
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import numpy as np
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False


def plotcors(subplotsize, data, oslon, oslat, rowlevels, clipshape=True
             , ranges=([89.5, -89.5, 0.5, 359.5], [89.5, -89.5], [0.5, 359.5]), colorbar=True,
             labels=None, figsize=None, subplotsizes=None):
    '''data and rowlevels: data[i] is the data needed for the single axes, if the axes is overlapped, data[i] should has two dimension
        rowlevles: such as [2,5,10], shows space in levels

        '''
    import cartopy.crs as ccrs
    import matplotlib.pyplot as plt
    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
    from matplotlib import colors
    from cartopy.feature import ShapelyFeature
    from cartopy.io.shapereader import Reader

    oslat, oslon, data = del_ll(ranges[0], data, oslat, oslon)
    # print(data[3][np.where(data[3]>0)])

    # 调整色条
    colormap = ['#5884ff', '#7c9eff', '#93b0fe', '#abc0ff', '#c2d3ff',
                '#f3ddcf', '#f3d3c4', '#f3b5a0', '#f3896f', '#f27557', '#f46140']

    tmap = colors.LinearSegmentedColormap.from_list('cmap', colormap)

    if figsize is not None:
        fig = plt.figure(figsize=figsize)
    else:
        fig = plt.figure()
    if subplotsizes is not None:
        plt.subplots_adjust(left=subplotsizes[0], bottom=subplotsizes[1], right=subplotsizes[2],
                            top=subplotsizes[3], wspace=subplotsizes[4], hspace=subplotsizes[5])

    vmax = np.nanmax(data[1], axis=None)
    vmin = np.nanmin(data[1], axis=None)
    if (vmax > 0) & (vmin < 0):
        level_min = np.linspace(vmin, 0, 6)
        level_max = np.linspace(0, vmax, 6)
        anomalylevels = np.hstack((level_min, level_max[1:]))
        # levels = np.unique([round(i, level_round) for i in levels])
    else:
        anomalylevels = np.linspace(vmin, vmax, 11)

        # levels = np.insert(levels, np.argwhere(np.array(levels) > 0).flatten()[0], 0)

    axes = []
    def plot_single_ax(ax, contour_line_data,contourf_shade_data,  slevels, slabel,udata=None,vdata=None):
        bwfill = ax.contour(oslon, oslat, contour_line_data, find_hgt_levels(np.nanmax(contour_line_data), np.nanmin(contour_line_data), slevels),
                            colors='k', transform=ccrs.PlateCarree())

        # bwfill = ax.contour(oslon, oslat, contourf_shade_data, anomalylevels,
        #                     colors='k', transform=ccrs.PlateCarree())
        # clevels = []
        # filllevels = fill.levels
        # for i in range(0, len(filllevels), 1):
        #     clevels.append(filllevels[i])
        ax.clabel(bwfill, fmt='%i', inline=1)

        fill = ax.contourf(oslon, oslat, contourf_shade_data, anomalylevels, transform=ccrs.PlateCarree(), colors=colormap)
        from matplotlib import cm
        cmap = cm.get_cmap('RdYlBu')
        cmap_r = cmap.reversed()
        # fill = ax.contourf(oslon, oslat, contour_line_data, find_hgt_levels(np.nanmax(contour_line_data), np.nanmin(contour_line_data), slevels), transform=ccrs.PlateCarree(), cmap=cmap_r)
        if labels is not None:
            ax.text(ax.get_xlim()[0], ax.get_ylim()[1], slabel, transform=ccrs.PlateCarree(central_longitude=180))

        if vdata is not None:
            try:
                udata = np.ma.masked_less(udata, 30)
                # print(udata[np.where(udata>30)])
                sfill = ax.contourf(oslon, oslat, udata, 3, transform=ccrs.PlateCarree(), cmap="Greys")
                vdata = np.ma.masked_less(vdata, 30)
                # print(udata[np.where(udata>30)])
                sfill = ax.contourf(oslon, oslat, vdata, 3, transform=ccrs.PlateCarree(), cmap="pink")
            except:
                print('no wind is more than 30')



        # 设置坐标轴经纬度标志
        ax.set_extent((ranges[0][2], ranges[0][3], ranges[0][0], ranges[0][1]), crs=ccrs.PlateCarree())
        ax.set_xticks(ranges[2], crs=ccrs.PlateCarree())
        ax.set_yticks(ranges[1], crs=ccrs.PlateCarree())
        lon_formatter = LongitudeFormatter(zero_direction_label=True)
        lat_formatter = LatitudeFormatter()
        ax.xaxis.set_major_formatter(lon_formatter)
        ax.yaxis.set_major_formatter(lat_formatter)
        # 添加网格线
        ax.gridlines(color='black', linestyle='--', alpha=0.4)
        ax.coastlines(alpha=0.4)

        # # #添加shp文件进入图片中
        if clipshape == True:
            pathss = shp2clip(r'D:\basis\data\shp\shengjie\shengjie.shp')
            plate_carre_data_transform = ccrs.PlateCarree()._as_mpl_transform(ax)
            upath = PathPatch(pathss, transform=plate_carre_data_transform)
            for collection in fill.collections:
                collection.set_clip_path(upath)

        shape_feature = ShapelyFeature(Reader(r'D:\basis\data\shp\shengjie\bou2_4p.shp').geometries(),
                                       ccrs.PlateCarree(),
                                       edgecolor='k', facecolor='none', linewidths=0.55)
        ax.add_feature(shape_feature)
        return fill

    # plot subplotsize
    for i in range(1, subplotsize[0] * subplotsize[1] + 1):
        ax = fig.add_subplot(subplotsize[0], subplotsize[1], i, projection=ccrs.PlateCarree(central_longitude=180))
        axes.append(ax)
        if len(data) == 4:
            fill = plot_single_ax(ax, contour_line_data=data[0][i-1], contourf_shade_data=data[1][i-1],vdata=data[3][i-1], udata=data[2][i-1], slevels=rowlevels[i-1], slabel=labels[i - 1])
        elif len(data) ==3:
            fill = plot_single_ax(ax, contour_line_data=data[0][i-1], contourf_shade_data=data[1][i-1],udata=data[2][i-1], slevels=rowlevels[i-1], slabel=labels[i - 1])
        else:
            fill = plot_single_ax(ax, contour_line_data=data[0][i-1], contourf_shade_data=data[1][i-1], slevels=rowlevels[i-1], slabel=labels[i - 1])

    # add colorbar
    if colorbar == True:
        # cb_ax = fig.add_axes([0.1, 0.1, 0.8, 0.02])
        cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
        cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
    return axes


def find_hgt_levels(vmax, vmin, space):
    roundmin = round(vmin)
    roundmax = round(vmax)
    if space != 10:
        roundmin = roundmin - roundmin % space
        roundmax = roundmax + space - roundmax % space
        levels = np.arange(roundmin, roundmax+1, space)
    else:
        roundmin = roundmin - roundmin % 5
        roundmax = roundmax + 5 - roundmax % 5
        levels = np.arange(roundmin, roundmax+10, space)
    return levels

def shp2clip(shpfile):
    import shapefile
    from matplotlib.path import Path
    from matplotlib.patches import PathPatch
    sf = shapefile.Reader(shpfile)
    vertices = []
    codes = []
    for shape_rec in sf.shapeRecords():
        pts = shape_rec.shape.points
        prt = list(shape_rec.shape.parts) + [len(pts)]
        for i in range(len(prt) - 1):
            for j in range(prt[i], prt[i + 1]):
                vertices.append((pts[j][0], pts[j][1]))
            codes += [Path.MOVETO]
            codes += [Path.LINETO] * (prt[i + 1] - prt[i] - 2)
            codes += [Path.CLOSEPOLY]
        clip = Path(vertices, codes)
    return clip

def del_ll(area, data, lat, lon):
    from current_function_in_fd import ll_del
    del_area = ll_del(lat, lon, area)
    lat = lat[del_area[0]:del_area[1]+1]
    lon = lon[del_area[2]:del_area[3] + 1]
    data = np.array(data)
    data0 = data[0][:, del_area[0]:del_area[1]+1, del_area[2]:del_area[3] + 1]
    data1 = data[1][:, del_area[0]:del_area[1]+1, del_area[2]:del_area[3] + 1]
    if data.shape[0] == 3:
        data3 = data[2][:, del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]
        return lat, lon, [data0, data1, data3]
    if data.shape[0] == 4:
        data3 = data[2][:, del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]
        data4 = data[3][:, del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]
        return lat, lon, [data0, data1, data3, data4]
    return lat, lon, [data0, data1]

if __name__ == '__main__':
    from nc4 import read_nc4

    et = read_nc4(r'D:\basis\fd\data\china et')[0:3]
    # et = np.ma.masked_equal(et, -9999)
    # et = np.ma.masked_invalid(et)
    lats = read_nc4(r'D:\basis\fd\data\china lat')
    lons = read_nc4(r'D:\basis\fd\data\china lon')
    plotcors((1, 3), et, lons, lats, labels=['1', '2', '3'],
             ranges=[[15, 55, 70, 140], [15, 25, 35, 45, 55], [70, 90, 110, 130]])