import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from pandas import Series
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import numpy as np
from pylab import mpl

mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False


def get_before_after_period_data(fd_timeindex, varible_name, anomaly=False, divided10=False, get_mean=False):
    begin = fd_timeindex[0]
    end = fd_timeindex[1]
    print(begin, end)
    et = read_nc4(r'D:\basis\fd\data\china %s' % varible_name)
    et = np.ma.masked_equal(et, -9999.0)
    et[np.where(et == 32766)] = np.nan
    et[np.where(et == -32767)] = np.nan
    et[np.where(et == -32767)] = np.nan
    et = np.ma.masked_invalid(et)

    mth_b_fd = np.nanmean(et[begin - 6:begin, :, :], axis=0)
    mth_fd = np.nanmean(et[begin:end + 1, :, :], axis=0)
    mth_a_fd = np.nanmean(et[end + 1: end + 7], axis=0)
    if (anomaly is True) | (get_mean is True):
        reet = et.reshape([int(et.shape[0] / 73), 73, et.shape[1], et.shape[2]])

        etbegin = (begin+1) % 73 - 1
        etend= (end+1) % 73 - 1
        mth_b_fd_mean = np.nanmean(reet[:, etbegin - 6:etbegin, :, :], axis=(0, 1))
        mth_fd_mean = np.nanmean(reet[:, etbegin: etend+1, :, :], axis=(0, 1))
        mth_a_fd_mean = np.nanmean(reet[:, etend + 1: etend + 7, :, :], axis=(0, 1))
        mth_fd = mth_fd - mth_fd_mean
        mth_b_fd = mth_b_fd - mth_b_fd_mean
        mth_a_fd = mth_a_fd - mth_a_fd_mean
        if anomaly is True:
            return [mth_b_fd, mth_fd, mth_a_fd]
        if get_mean is True:
            print('jsksk')
            return [mth_b_fd_mean, mth_fd_mean, mth_a_fd_mean]
    if divided10 is True:
        return [mth_b_fd/10, mth_fd/10, mth_a_fd/10]

    return [mth_b_fd, mth_fd, mth_a_fd]

def plothgt(data, area, space, labels):
    import cartopy.crs as ccrs
    from nc4 import read_nc4
    import plot_hgt as pl
    x1 = area[0]
    x2 = area[1]
    y1 = area[2]
    y2 = area[3]
    area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])

    lats = read_nc4(r'D:\basis\fd\data\noaa lat')
    lons = read_nc4(r'D:\basis\fd\data\noaa lon')

    axes = pl.plotcors((1, 3), data, lons, lats, rowlevels=space, clipshape=False,
                       ranges=[[89, 0, 20, 140], [80, 60, 40, 20, 0], [20, 50, 80, 110,140]],
                       labels=labels, figsize=(9, 3),subplotsizes=[0.07,0.11,0.96,1,0.2,0.2])
    for i in axes:
        i.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color='red')
    return

def cal_plot_hgt(maskdata_name, varible, area, space):
    import pandas as pd

    allmask = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv' % maskdata_name, index_col=0, parse_dates=True)
    mask = np.array(allmask['mask'])

    begin = np.argwhere(mask[:] == 1).flatten()
    end = np.argwhere(mask[:] == 3).flatten()
    print(begin)
    for i in range(len(begin)):
        if ((allmask.index[begin[i]].month >= 4) & (allmask.index[begin[i]].month <= 10)) & ((
                                                                                                     allmask.index[end[
                                                                                                         i]].month >= 4) & (
                                                                                                     allmask.index[end[
                                                                                                         i]].month <= 10)):

            #######################修改处#############################

            # 前期绘图
            data0 = get_before_after_period_data([begin[i], end[i]], varible, anomaly=False, divided10=True)
            data1 = get_before_after_period_data([begin[i], end[i]], varible, anomaly=True)

            labels = ['-1M %s %s' % (varible, ((str(allmask.index[begin[i] - 6]))[0:10])),
                      'O %s %s to %s' % (
                          varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10]),
                      '+1M %s %s' % (varible, (str(allmask.index[end[i] + 7]))[0:10])]

            if (varible[-3:] == '850') | (varible[-3:] == '500'):
                data3 = get_before_after_period_data([begin[i], end[i]], 'uwnd ' + varible[-3:], anomaly=True)
                data4 = get_before_after_period_data([begin[i], end[i]], 'vwnd ' + varible[-3:], anomaly=True)
                plothgt([data0, data1, data3, data4], area, space, labels)
            else:
                data3 = get_before_after_period_data([begin[i], end[i]], 'uwnd ' + varible[-3:], anomaly=False)
                data4 = get_before_after_period_data([begin[i], end[i]], 'uwnd ' + varible[-3:], anomaly=False, get_mean=True)
                plothgt([data0, data1, data3, data4], area, space, labels)

            plt.show()
            plt.savefig(r'D:\basis\fd\ano %s %s.png' % (maskdata_name, '%s %s to %s' % (
                varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10])), dpi=800)
            plt.close()


if __name__ == '__main__':
    from nc4 import read_nc4, save_nc4

    area_name = ['yzr', 'xinan', 'dongbei', 'xinjiang']
    allarea = [[26.125, 36.125, 110.125, 120.125],
               [24.125, 33.125, 100.125, 110.125],
               [40.125, 50.125, 122.125, 134.125],
               [40.125, 48.125, 80.125, 90.125]]
    varibles = ['hgt', 'uwnd', 'omega']

    cal_plot_hgt(area_name[0], 'hgt' + ' 200', allarea[0], space=[10, 10, 10])
    # print(mth_fd[np.where(mth_fd > 30)].shape)
    # for i in range(0, 3):
    #     # cal_plot_hgt(area_name[i], 'hgt' + ' 100', allarea[i], space=[10,10,10])
    #     cal_plot_hgt(area_name[i], 'hgt' + ' 200', allarea[i], space=[10, 10, 10])
        # cal_plot_hgt(area_name[i], 'hgt' + ' 500', allarea[i], space=[5, 5, 5])
        # cal_plot_hgt(area_name[i], 'hgt' + ' 850', allarea[i], space=[2, 2, 2])


    # data4 = get_before_after_period_data([1631, 1635], 'uwnd ' + '200', anomaly=False)
    # et = data4[1]
    # print(et.shape)
    # et = read_nc4(r'D:\basis\fd\data\china uwnd 200')
    # print(et[np.where(et > 30)].shape)
    # from current_function_in_fd import ll_del
    # lat = read_nc4(r'D:\basis\fd\data\noaa lat')
    # lon = read_nc4(r'D:\basis\fd\data\noaa lon')
    # del_area = ll_del(lat, lon, [60, 0, 60, 140])
    # et = et[del_area[0]:del_area[1] + 1, del_area[2]:del_area[3] + 1]
    # print(et[np.where(et > 30)].shape)

    # for i in range(4):
    #     for j in varibles:
    #         plot_area(area_name[i], j+' 200', allarea[i])
    #         plot_area(area_name[i], j+' 500', allarea[i])
    #         plot_area(area_name[i], j+' 850', allarea[i])
