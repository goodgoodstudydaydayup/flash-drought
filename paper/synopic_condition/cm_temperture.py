import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from pandas import Series
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import numpy as np
from pylab import mpl
from nc4 import read_nc4, save_nc4
import pandas as pd
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False

def find_fd_start_time(areaname):
    import pandas as pd
    data = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv' % areaname, index_col=0, parse_dates=True)
    sdata = data['mask'][data['mask'] == 1]
    edata = data['mask'][data['mask'] == 3]
    month45 = sdata[(sdata.index.month == 4) | (sdata.index.month == 5)]
    month678 = sdata[(sdata.index.month == 6) | (sdata.index.month == 7) | (sdata.index.month == 8)]
    month910 = sdata[(sdata.index.month == 9) | (sdata.index.month == 10)]

    emonth45 = edata[(edata.index.month == 4) | (edata.index.month == 5)]
    emonth678 = edata[(edata.index.month == 6) | (edata.index.month == 7) | (edata.index.month == 8)]
    emonth910 = edata[(edata.index.month == 9) | (edata.index.month == 10)]
    information = pd.DataFrame({'smonth45': month45, 'smonth678': month678, 'smonth910': month910,
                                'emonth45': emonth45, 'emonth678': emonth678, 'emonth910': emonth910})
    information.to_csv(r'D:\basis\fd\data\fd_time_%s.csv' % areaname)


def get_before_after_period_data(areaname, area, varible_level, space):
    import pandas as pd
    # 读入数据
    mask = pd.read_csv(r'D:\basis\fd\data\fd_time_%s.csv' % areaname,index_col=0, parse_dates=True)
    def read_data(varible_name):
        et = read_nc4(r'D:\basis\fd\data\china %s %s' % (varible_name, varible_level))
        et = np.ma.masked_equal(et, -9999.0)
        et[np.where(et == 32766)] = np.nan
        et[np.where(et == -32767)] = np.nan
        et = np.ma.masked_invalid(et)
        return et
    hgt = read_data('air') -273.15

    from current_function_in_fd import time_del
    fd_timeindex = []
    monthname = ['smonth45', 'smonth678', 'smonth910']

    savename = ['AM', 'JJA', 'SO']
    for mn in monthname:
        start_fdtime = mask.index[mask['%s' % mn] == 1]
        fdtimeindex = []
        for i in start_fdtime:
            begin = mask.index.get_loc(i)
            fdtimeindex.extend(time_del([str(i)[:10], str(mask.index[begin + 1])[:10]]))
        fd_timeindex.append(fdtimeindex)
    # 按月份循环绘图, month = 0,1,2
    print(fd_timeindex)
    for month in range(len(fd_timeindex)):

        if len(fd_timeindex[month]) > 0:

            def cal_mean_data(data, month, divide10=False, anomaly=True, hgt200=False):
                mth_b_fd = []
                mth_fd = []
                mth_a_fd = []
                for day in range(0, len(fd_timeindex[month]), 2):
                    begin = fd_timeindex[month][day]
                    end = fd_timeindex[month][day + 1]
                    def find_data_and_anomaly(et, begin, end, anomaly=True, hgt200=False):
                        mth_b_fd = np.nanmean(et[begin - 6:begin, :, :], axis=0)
                        mth_fd = np.nanmean(et[begin:end + 1, :, :], axis=0)
                        mth_a_fd = np.nanmean(et[end + 1: end + 7], axis=0)

                        reet = et.reshape([int(et.shape[0] / 73), 73, et.shape[1], et.shape[2]])

                        etbegin = (begin+1) % 73 -1
                        etend = (end+1) % 73 -1
                        mth_b_fd_mean = np.nanmean(reet[:, etbegin - 6:etbegin, :, :], axis=(0, 1))
                        mth_fd_mean = np.nanmean(reet[:, etbegin: etend + 1, :, :], axis=(0, 1))
                        mth_a_fd_mean = np.nanmean(reet[:, etend + 1: etend + 7, :, :], axis=(0, 1))
                        if hgt200 == True:
                            return [mth_b_fd_mean, mth_fd_mean, mth_a_fd_mean]
                        mth_fd_mean = mth_fd - mth_fd_mean
                        mth_b_fd_mean = mth_b_fd - mth_b_fd_mean
                        mth_a_fd_mean = mth_a_fd - mth_a_fd_mean
                        if anomaly == False:
                            return [mth_b_fd, mth_fd, mth_a_fd]
                        return [mth_b_fd_mean, mth_fd_mean, mth_a_fd_mean]

                    mth = find_data_and_anomaly(data, begin, end, anomaly=anomaly, hgt200=hgt200)
                    # print(begin, end)
                    # plotsnoaa([mth, mth], area, labels=labels, space=space)
                    # plt.show()
                    mth_b_fd.append(mth[0])
                    mth_fd.append(mth[1])
                    mth_a_fd.append(mth[2])

                mth_b_fd = np.nanmean(np.array(mth_b_fd), axis=0)
                mth_fd = np.nanmean(np.array(mth_fd), axis=0)
                mth_a_fd = np.nanmean(np.array(mth_a_fd), axis=0)

                if divide10 == True:
                    return [mth_b_fd/10, mth_fd/10, mth_a_fd/10]
                return [mth_b_fd, mth_fd, mth_a_fd]

            labels = ['Lag-1', 'Onset tem%s %s %s' % (varible_level, areaname, savename[month]), 'Lag+1']
            if (varible_level == '850') | (varible_level == '500'):
                data = [cal_mean_data(hgt, month, anomaly=False), cal_mean_data(hgt, month)]
            else:
                data = [cal_mean_data(hgt, month, anomaly=False), cal_mean_data(hgt, month)]

            plotsnoaa(data, area, labels=labels, space=space)

            # plt.show()
            plt.savefig(r'D:\basis\fd\%s_tem%s_%s.png' % (areaname, varible_level, savename[month]),dpi=800)
            plt.close()
    return



def plotsnoaa(data, area, labels, space):
    import cartopy.crs as ccrs
    from nc4 import read_nc4
    import plot_hgt as pl
    x1 = area[0]
    x2 = area[1]
    y1 = area[2]
    y2 = area[3]
    area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])

    lats = read_nc4(r'D:\basis\fd\data\noaa lat')
    lons = read_nc4(r'D:\basis\fd\data\noaa lon')

    # axes = pl.plotcors((1, 3), data, lons, lats, rowlevels=space, clipshape=False,
    #                    ranges=[[89, 0, 20, 140], [80, 60, 40, 20, 0], [20, 50, 80, 110,140]],
    #                    labels=labels, figsize=(9, 3),subplotsizes=[0.07,0.11,0.96,1,0.2,0.2])
    axes = pl.plotcors((1, 3), data, lons, lats, rowlevels=space, clipshape=False,
                       ranges=[[60, 0, 60, 140], [60, 40, 20, 0], [60, 80, 100,120,140]],
                       labels=labels, figsize=(9, 3),subplotsizes=[0.07,0.11,0.96,1,0.2,0.2])
    for i in axes:
        i.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color='red')
    return



if __name__ == '__main__':
    area_name = ['yzr', 'xinan', 'dongbei', 'xinjiang']
    allarea = [[26.125, 36.125, 110.125, 120.125],
               [24.125, 33.125, 100.125, 110.125],
               [40.125, 50.125, 122.125, 134.125],
               [40.125, 48.125, 80.125, 90.125]]

    for i in range(0, 3):
        get_before_after_period_data(area_name[i], allarea[i], '200',[2,2,2])
        get_before_after_period_data(area_name[i], allarea[i], '500', [2, 2, 2])
        get_before_after_period_data(area_name[i], allarea[i], '850', [2, 2, 2])
