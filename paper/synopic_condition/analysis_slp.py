import numpy as np
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt

def get_before_after_period_data(year, month, varible_name):
    beginmonth = month[0]
    endmonth = month[1]
    # sst = xr.open_dataset(r'D:\basis\fd\data\sst.mnmean.v4.nc')
    sst = xr.open_dataset(r'/data/slp.mon.mean.nc')

    def select_bytime(month):
        if month[1] > 12:
            obs_data = sst.sel(time=slice('%i-%i' % (year, month[0]), '%i-%i' % (year+1, month[1]-12))).variables[
                '%s'%varible_name].mean(dim='time')
        else:
            obs_data = sst.sel(time=slice('%i-%i' % (year, month[0]), '%i-%i' % (year, month[1]))).variables[
                '%s'%varible_name].mean(dim='time')
        def is_amj(sstmonth, month):
            if month[1] > 12:
                return (sstmonth >= month[0]) | (sstmonth <= month[1]-12)
            return (sstmonth >= month[0]) & (sstmonth <= month[1])
        fano_data = sst.sel(time=is_amj(sst['time.month'], month)).variables['%s'%varible_name].mean(dim='time')
        return obs_data, obs_data-fano_data

    onset_sst, ano_onset_sst = select_bytime(month)
    three_month_before,ano_three_month_before= select_bytime([beginmonth-3, beginmonth-1])
    three_month_after, ano_three_month_after = select_bytime([endmonth+1, endmonth+3])

    return [[three_month_before, onset_sst, three_month_after], [ano_three_month_before, ano_onset_sst, ano_three_month_after]]
def plotsst(data, area, labels):
    import cartopy.crs as ccrs
    from nc4 import read_nc4
    import plot_hgt as pl
    x1 = area[0]
    x2 = area[1]
    y1 = area[2]
    y2 = area[3]
    area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])

    # da = xr.open_dataset(r'D:\basis\fd\data\sst.mnmean.v4.nc')
    da = xr.open_dataset(r'/data/slp.mon.mean.nc')
    lats = da['lat'].values
    lons = da['lon'].values

    axes = pl.plotcors((3, 1), data, lons, lats, rowlevels=[2,2,2], clipshape=False,
                       ranges=[[80, -20, 0, 358], [80, 60, 40, 20, 0, -20], [0, 60, 120, 180, 240, 300, 358]],
                       labels=labels, figsize=(9, 7), subplotsizes=[0.03, 0.11, 1, 0.98, 0.2, 0.2])

    for i in axes:
        i.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color='red')
    return

def cal_plot_hgt(maskdata_name, varible, area):
    import pandas as pd

    allmask = pd.read_csv(r'D:\basis\fd\data\datavarible1979 %s.csv' % maskdata_name, index_col=0, parse_dates=True)
    mask = np.array(allmask['mask'])

    begin = np.argwhere(mask[:] == 1).flatten()
    end = np.argwhere(mask[:] == 3).flatten()



    for i in range(len(begin)):
        if ((allmask.index[begin[i]].month >= 4) & (allmask.index[begin[i]].month <= 10)) & ((
             allmask.index[end[
                 i]].month >= 4) & (
             allmask.index[end[
                 i]].month <= 10)):


            labels = ['-1M %s %s' % (varible, ((str(allmask.index[begin[i] - 6]))[0:10])),
                      'O %s %s to %s' % (
                          varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10]),
                      '+1M %s %s' % (varible, (str(allmask.index[end[i] + 7]))[0:10])]
            year = allmask.index[begin[i]].year
            beginmonth = allmask.index[begin[i]].month
            endmonth = allmask.index[end[i]].month
            data = get_before_after_period_data(year, [beginmonth, endmonth], varible)
            plotsst(data, area, labels)

            # plt.show()
            plt.savefig(r'D:\basis\fd\%s %s.png' % (maskdata_name, '%s %s to %s' % (
                varible, ((str(allmask.index[begin[i]]))[0:10]), (str(allmask.index[end[i] + 1]))[0:10])), dpi=800)
            plt.close()

if __name__ == '__main__':
    from nc4 import read_nc4, save_nc4

    area_name = ['yzr', 'xinan', 'dongbei', 'xinjiang']
    allarea = [[26.125, 36.125, 110.125, 120.125],
               [24.125, 33.125, 100.125, 110.125],
               [40.125, 50.125, 122.125, 134.125],
               [40.125, 48.125, 80.125, 90.125]]

    for i in range(0,3):
        cal_plot_hgt(area_name[i], 'slp', allarea[i])
