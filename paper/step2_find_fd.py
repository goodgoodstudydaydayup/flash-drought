import numpy as np
import pandas as pd

np.set_printoptions(threshold=np.inf)
period_fd = 3
def oktin_standard(et, pet):
    sesr, percentile_sesr, osesr, percentile_osesr = do_cal_in_onepoint(np.array(et).flatten(),
                                                                        np.array(pet).flatten())
    smask, percentile25 = do_cri_in_onepoint(sesr, percentile_sesr, osesr, percentile_osesr)
    return smask, percentile25

def do_cal_in_onepoint(et, pet):
    from pandas import DataFrame
    def szscore(data):
        from scipy.stats.mstats import zscore
        data = data.reshape(19, 73)
        # DataFrame(data).to_csv('esi.csv')
        percentile_data = np.zeros([19, 73])
        for i in range(73):
            data[:, i] = zscore(data[:, i])
            percentile_data[:, i] = calc_percentile(data[:, i])
        # DataFrame(data).to_csv('resi.csv')
        return data.flatten(), percentile_data.flatten()

    # 获得rses
    sesr = et / pet
    sesr, percentile_sesr = szscore(sesr)
    # 获得orses
    a0 = np.insert(sesr[:-1], 0, 0, axis=0)
    osesr = sesr - a0
    osesr, percentile_osesr = szscore(osesr)
    return sesr, percentile_sesr, osesr, percentile_osesr

def do_cri_in_onepoint(sesr, percentile_sesr, osesr, percentile_osesr):
    # rses 只为最后的per
    def find5con(percentile_osesr, sesr):
        # 找到变化率在40以下的orses，设定为1
        percentile_osesr_in01 = np.array(percentile_osesr)
        sesr = np.array(sesr)
        percentile_osesr_in01[np.where((percentile_osesr_in01[:] > 0.4))] = 9
        percentile_osesr_in01[np.where(percentile_osesr_in01[:] <= 0.4)] = 1
        percentile_osesr_in01[np.where(percentile_osesr_in01[:] == 9)] = 0
        s = pd.Series(percentile_osesr_in01)
        percentile_osesr_incum = s.groupby(s.eq(0).cumsum()).cumsum().tolist()
        # 找到单独的0，符合前后的rese更小的，单独存储位置。
        def find_zero(percentile_osesr_inmask, sesr):
            # 传入data为小于orses40%的1111序列累加型。也就是1，2，3，0，1类似形势。
            # 返回其中满足 仅有一个0 且前后sesr减小的0值。
            percentile_osesr_inmask = np.array(percentile_osesr_inmask)
            sesr = np.array(sesr)
            mask_single_zero = np.zeros(len(percentile_osesr_inmask))
            c = np.argwhere(percentile_osesr_inmask[:] == 1).flatten()
            c = c[np.argwhere((c - 1) >= 0)].flatten()
            if len(c) != 0:
                d = percentile_osesr_inmask[(c - 2)].flatten()
                zero_arg = (c[np.argwhere(d[:] != 0)].flatten()) - 1
                mask_single_zero[zero_arg] = 1
                s = sesr[zero_arg + 1] - sesr[zero_arg - 1]
                mask_single_zero[zero_arg[np.argwhere(s[:] > 0)]] = 0
            return mask_single_zero

        mask_single_zero_in_posesrin01 = find_zero(percentile_osesr_incum, sesr)
        percentile_osesr_in01[np.where(mask_single_zero_in_posesrin01 == 1)] = 1
        s = pd.Series(percentile_osesr_in01)
        percentile_osesr_in01 = np.array(s.groupby(s.eq(0).cumsum()).cumsum().tolist())

        # 将低于30天的删除
        mask = np.zeros(percentile_osesr_in01.shape)
        cs = np.argwhere(percentile_osesr_in01[:] > period_fd)
        cc = np.argwhere(percentile_osesr_in01[:] == period_fd).flatten()
        cs = np.append(cs, cc)
        for i in range(1, period_fd):
            cs = np.append(cs, cc - i)
        mask[[int(i) for i in cs]] = 1
        percentile_osesr_in01[np.where(mask[:] == 0)] = 0
        return percentile_osesr_in01, mask_single_zero_in_posesrin01

    def del_two_zero(percentile_osesr_in01, mask_single_zero_in_posesrin01):

        def ssdel_two_zero(mask_ranges, mask_zero_ranges):
            ## 此函数中mask为单条mask，相当于mask_ranges，只是当初变量设定不够严格，所以暂时不改，在此提示。
            # 本函数需要传入每一条单独的闪旱序列，用处为删除有两个0的闪旱。
            mask_ranges[np.where(mask_ranges[:] > 0)] = 1
            mask_ranges[np.where(mask_zero_ranges[:] == 1)] = 0

            # 将有两个0的给mask了。

            def find_OK5(sp):
                for i, imask_ranges in enumerate(mask_ranges[sp:-period_fd]):
                    ssum = np.sum(mask_ranges[sp:-period_fd][i:i + period_fd])
                    if ssum == (period_fd-1):
                        zero_number = 1
                        return sp + i, zero_number
                    if ssum == period_fd:
                        zero_number = 0
                        return sp + i, zero_number
                return -999, 0

            def find_ep(sp, zero_number):
                for i, imask_ranges in enumerate(mask_ranges[sp + period_fd:]):
                    if zero_number == 1:
                        if imask_ranges == 0:
                            if mask_ranges[sp + period_fd-1 + i] != 0:
                                mask[sp] = 1
                                mask[sp + period_fd-1 + i] = 3
                                mask[sp + 1: sp + period_fd-1 + i] = 2
                                return (sp + period_fd+1 + i)
                            else:

                                mask[sp] = 1
                                mask[sp + period_fd-2 + i] = 3
                                mask[sp + 1: sp + period_fd-2 + i] = 2
                                return (sp + period_fd+1 + i)
                    else:
                        if imask_ranges == 0:
                            zero_number = 1

                return sp + len(mask_ranges[sp:])

            mask = np.zeros(mask_ranges.shape)
            sp = 0
            lenth = len(mask_ranges) - period_fd
            for i in range(10):
                if sp <= lenth:
                    sp, zero_number = find_OK5(sp)
                    if sp == -999:
                        break
                    sp = find_ep(sp, zero_number)
                else:
                    break
            return mask

        # 如果只有一个闪旱，则无法循环，所以做一个if
        d1 = np.argwhere(percentile_osesr_in01[:] == 1).flatten()
        if len(d1) == 1:
            percentile_osesr_in01[int(d1):] = ssdel_two_zero(percentile_osesr_in01[int(d1):], mask_single_zero_in_posesrin01[int(d1):])
        else:
            for i in range(len(d1) - 1):
                percentile_osesr_in01[d1[i]:d1[i + 1]] = ssdel_two_zero(percentile_osesr_in01[d1[i]:d1[i + 1]], mask_single_zero_in_posesrin01[d1[i]:d1[i + 1]])
            percentile_osesr_in01[d1[-1]:] = ssdel_two_zero(percentile_osesr_in01[d1[-1]:], mask_single_zero_in_posesrin01[d1[-1]:])
        return percentile_osesr_in01

    def final_del_less_20(percentile_osesr_in01, percentile_sesr):
        ## 此处data 为 mask
        percentile_osesr_in01[np.where(percentile_osesr_in01[:] > 0)] = 1
        s = pd.Series(percentile_osesr_in01)
        percentile_osesr_in01 = s.groupby(s.eq(0).cumsum()).cumsum().tolist()
        percentile_osesr_in01 = np.array(percentile_osesr_in01)

        m = np.argwhere(percentile_osesr_in01[:] >= period_fd).flatten()
        c = percentile_sesr[m].flatten()
        a_in_rses = m[np.argwhere(c[:] >= 0.2).flatten()]
        percentile_osesr_in01[a_in_rses] = 0

        d1 = np.argwhere(percentile_osesr_in01[:] == 1).flatten()
        if len(d1) != 0:
            # 将最后不满足rses<20% 以及 两个0的闪旱全部归为0
            def del_only15(ranges):
                if np.nanmax(ranges) <= (period_fd-1):
                    ranges[:] = 0
                elif np.nanmax(ranges) > (period_fd-1):
                    a = np.nanargmax(ranges)
                    ranges[1:a] = 2
                    ranges[0] = 1
                    ranges[a] = 3
                return ranges

            # 如果只有一个闪旱，则无法循环，所以做一个if
            if len(d1) == 1:
                percentile_osesr_in01[int(d1):] = del_only15(percentile_osesr_in01[int(d1):])
            else:
                for i in range(len(d1) - 1):
                    percentile_osesr_in01[d1[i]:d1[i + 1]] = del_only15(percentile_osesr_in01[d1[i]:d1[i + 1]])
                percentile_osesr_in01[d1[-1]:] = del_only15(percentile_osesr_in01[d1[-1]:])
        return percentile_osesr_in01

    def per_less_25(osesr, mask):
        ## 此函数中data为 osesr
        def per(osesr, sp, ep, mask, percentile25):
            osesr = np.array(osesr)
            sp = int(sp)
            ep = int(ep)
            mask = np.array(mask)
            imean = np.nanmean(osesr[sp:ep+1])

            osesr_reshapeinyear = osesr.reshape([19, 73])

            etbegin = (sp + 1) % 73 - 1
            etend = (ep + 1) % 73 - 1
            meandata = np.nanmean(osesr_reshapeinyear[:, etbegin: etend + 1], axis= 1)

            if np.count_nonzero(meandata != meandata) > 0:
                mask[sp:ep + 1] = 0
                return mask, percentile25
            a = calc_percentile(meandata)[int(sp/73)]
            if a > 0.25:
                mask[sp:ep + 1] = 0
            else:
                mask[sp] = 1
                mask[sp+1:ep] = 2
                mask[ep] = 3
                percentile25[sp:ep + 1] = calc_percentile(meandata)[int(sp/73)]
            return mask, percentile25


        percentile25 = np.zeros(mask.shape)
        sp = np.argwhere(mask[:] == 1).flatten()-1

        ep = np.argwhere(mask[:] == 3).flatten()
        for i in range(len(sp)):
            mask, percentile25 = per(osesr, sp[i], ep[i], mask, percentile25)
        return mask, percentile25


    mask, mask_zero = find5con(percentile_osesr, sesr)
    mask = del_two_zero(mask, mask_zero)
    mask = final_del_less_20(mask, percentile_sesr)

    allmask, percentile25 = per_less_25(osesr, mask)
    return allmask, percentile25

def calc_percentile(a, method='min'):
        # 获得传入序列的百分位值
        from scipy.stats import rankdata
        import numpy as np
        if isinstance(a, list):
            a = np.asarray(a)
        s = rankdata(a, method=method) / float(len(a))
        return [round(i, 3) for i in s]


def calc_percentiles(sample):
    # 获得传入序列的百分位值
    from statsmodels.distributions.empirical_distribution import ECDF
    # sample = np.array(sample).reshape([16+18*31,])
    model = ECDF(sample)
    result = []
    for ssi in sample:
        result.append(model(ssi))
    return np.array(result)

def choose_month(mask, month, method='both'):
    # 对于mask进行选择对应月份。
    def del_fd_not410(smask):
        sfd = np.argwhere(smask == 1).flatten()
        efd = np.argwhere(smask == 3).flatten()
        for i in range(len(sfd)):
            if method == 'both':
                if (((sfd[i] + 1) % 73 - 1) < month[0]) | (((efd[i] + 1) % 73 - 1) > month[1]):
                    smask[sfd[i]:efd[i] + 1] = 0
            elif method == '1':
                if (((sfd[i] + 1) % 73 - 1) < month[0]) | (((sfd[i] + 1) % 73 - 1) > month[1]):
                    smask[sfd[i]:efd[i] + 1] = 0
            elif method == '3':
                if (((efd[i] + 1) % 73 - 1) < month[0]) | (((efd[i] + 1) % 73 - 1) > month[1]):
                    smask[sfd[i]:efd[i] + 1] = 0
        return smask

    mask_4_10 = np.zeros(mask.shape)
    for ilat in range(mask.shape[1]):
        for ilon in range(mask.shape[2]):
            mask_4_10[:, ilat, ilon] = del_fd_not410(mask[:, ilat, ilon])
    return mask_4_10

if __name__ == '__main__':
    from nc4 import read_nc4, save_nc4
    import xarray as xr
    import numpy as np

    def main(step):
        if step == 1:
            et = read_nc4(r'china et 2000')
            pet = read_nc4(r'china pet 2000')
            et[np.where(et == -9999.0)] = np.nan
            pet[np.where(pet == -9999.0)] = np.nan
            et = np.ma.masked_invalid(et)
            pet = np.ma.masked_invalid(pet)

            mask = np.zeros(et.shape)
            for ilat in range(len(et[0, :, 0])):
                for ilon in range(len(et[0, 0, :])):
                    sset = et[:, ilat, ilon]
                    spet = pet[:, ilat, ilon]
                    # 判断缺失数目
                    if (sset[np.isnan(sset)].shape[0] > (sset.shape[0]-50)) | (spet[np.isnan(spet)].shape[0] > (spet.shape[0]-50)):
                        print('NO')
                    else:
                        smask, percentile25 = oktin_standard(sset, spet)
                        mask[:, ilat, ilon] = smask
            save_nc4(mask, 'maskdata')

        if step == 2:
            '''mask 除了4和10月以外的闪旱。'''
            mask = read_nc4(r'D:\basis\fd\paper\maskdata')
            # print(choose_month(mask))
            save_nc4(choose_month(mask,[18,60]), 'mask410_p4')

        if step == 3:
            '''划分为四季的闪旱。'''
            hs = [[18, 30],[31,48],[49,60]]
            mask = read_nc4(r'mask410')
            save_nc4(choose_month(mask, hs[0], '1'), r'maskspring')
            mask = read_nc4(r'mask410')
            save_nc4(choose_month(mask, hs[1], '1'), r'masksummer')
            mask = read_nc4(r'mask410')
            save_nc4(choose_month(mask, hs[2], '1'), r'maskfall')

    main(2)
    # main(3)
    # 存储一下坐标信息。
    # import xarray as xr
    # a = xr.open_dataset(r'D:\basis\fd\data\china et 2000.nc4')
    # save_nc4(np.array(a.lat), 'china lat')
    # save_nc4(np.array(a.lon), 'china lon')
