import cartopy.crs as ccrs
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def plotpc(ax, pc, sy, ey, fm_range):
    def fast_moving_average(x, N):
        return np.convolve(x, np.ones((N,)) / N)[(N - 1):]
    ma = fast_moving_average(pc, fm_range)
    years = range(sy, ey + 1)
    ax.plot(years, pc, color='black')
    plt.plot(years, ma, color='k', linewidth=1.5)

    ax.axhline(0, color='k')
    ax.axhline(1, color='k', linestyle='--', alpha=0.5)
    ax.axhline(-1, color='k', linestyle='--', alpha=0.5)
    # ax.set_xticks(range(sy, ey + 1, int((ey-sy)/5)))
    # # ax.set_yticks([-2, -1, 0, 1, 2])
    # plt.xlim(sy, ey)
    # plt.ylim(-3, 3)

    # plt.text(sy + 1, 3.15, labela)
    ax.tick_params(labelsize=8)
    return plt

def regg(ax, time, y, monthname, ylabel_name, labels=None, timeticksname=None, xlabel=None):
    from pylab import mpl
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False
    def fit_line(x, y):
        # from scipy.stats import linregress
        # (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(np.array(x), y)
        # return beta_coeff, intercept, rvalue, pvalue, stderr
        import statsmodels.api as sm
        import statsmodels
        from statsmodels.formula.api import ols
        import pandas as pd
        sx = sm.add_constant(x)
        models = sm.OLS(y, sx)
        model = models.fit()
        fit_y = model.fittedvalues
        from sklearn.metrics import r2_score
        me = np.sum(fit_y-y)/len(y)
        r2 = r2_score(fit_y, y)
        mae = statsmodels.tools.eval_measures.meanabs(fit_y,y)
        rmse = statsmodels.tools.eval_measures.rmse(fit_y,y)
        return model.params[1], model.params[0], model.rsquared, model.pvalues[1], mae, rmse

    def plotall(ax, time, line1, month, beta_coeff, intercept, r, mse, mae, rmse,
                varible_name, timeticksname=None, labels=None):
        line2 = time * beta_coeff + intercept

        ax.scatter(time, line1, label='观测', color='blue', s=1.5)
        ax.plot(time, line2, label='趋势', color='red')

        ax.text(0.02, 0.9,
                '%s=%.4fx+%.2f' % (month, beta_coeff, intercept), fontsize=12, transform=ax.transAxes)

        ax.text(0.25, 0.02,
                'p=%f' % mse, fontsize=12, transform=ax.transAxes)
        ax.text(0.65, 0.02,
                '${r^2}$=%.2f' % (r), fontsize=12, transform=ax.transAxes)



        # plt.legend(loc=1)
        # ax.set_ylabel('%s' % varible_name)
        if xlabel == True:
            ax.set_xlabel('year')

        if labels is not None:
            ax.text(0.02, 1.01, labels, transform=ax.transAxes)

        ax.set_xticks(range(2000, 2019, 4))
        if timeticksname is not None:
            from pandas import Series
            ax.set_xticks(range(2000,2019,5))
            x = list(Series(ax.get_xticks()).map(int))
            ax.set_xticklabels(timeticksname[x])

        return


    beta_coeff, intercept, rvalue, mse, mae, rmse = fit_line(time, y)
    plotall(ax, time, y, monthname, beta_coeff, intercept, rvalue, mse, mae, rmse, ylabel_name, labels=labels)
    return [mse,mae,rmse,rvalue]


area_name = ['xinan', 'dongbei', 'xibei', 'MRYR']
allarea = [[24.125, 33.125, 102.125, 108.125],
           [40.125, 50.125, 122.125, 128.125],
           [36.125,47.125,87.125,98.125]]
data1 = pd.read_csv(r'datavarible1979 %s.csv'% area_name[0], index_col=0, parse_dates=True)
data2 = pd.read_csv(r'datavarible1979 %s.csv'% area_name[1], index_col=0, parse_dates=True)
data3 = pd.read_csv(r'datavarible1979 %s.csv'% area_name[2], index_col=0, parse_dates=True)
data4 = pd.read_csv(r'datavarible1979 %s.csv'% area_name[3], index_col=0, parse_dates=True)
data1 = data1.groupby(data1.index.year).mean()
data2 = data2.groupby(data2.index.year).mean()
data3 = data3.groupby(data3.index.year).mean()
data4 = data4.groupby(data4.index.year).mean()

fig,ax = plt.subplots(4,2, figsize=(9,7.5))
plt.subplots_adjust(0.06,0.07,0.99,0.97,0.2,0.35)

# plotpc(ax, np.array(data1['mpre']), 2000, 2018, 5)
regg(ax[0][0], np.arange(2000,2019), np.array(data1['tem']), 'TMEP', 'TEMP', labels='a) SW')
regg(ax[0][1], np.arange(2000,2019), np.array(data1['swdown']), 'SWD', 'SWD', labels='b) SW')

regg(ax[1][0], np.arange(2000,2019), np.array(data2['swdown']), 'SWD', 'SWD', labels='c) NE')

regg(ax[1][1], np.arange(2000,2019), np.array(data3['qair']), 'SH', 'SH', labels='d) NW')

regg(ax[2][0], np.arange(2000,2019), np.array(data4['tem']), 'TEMP', 'TEMP', labels='e) MRYR')
regg(ax[2][1], np.arange(2000,2019), np.array(data4['qair']), 'SH', 'SH', labels='f) MRYR', xlabel=True)
regg(ax[3][0], np.arange(2000,2019), np.array(data4['wind']), 'WIN', 'WIN', labels='g) MRYR', xlabel=True)

ax[3][1].set_xticks([])
ax[3][1].set_yticks([])
ax[3][1].axes.get_yaxis().set_visible(False)
ax[3][1].axes.get_xaxis().set_visible(False)
ax[3][1].spines['top'].set_visible(False)  # 去掉上边框
ax[3][1].spines['bottom'].set_visible(False)  # 去掉下边框
ax[3][1].spines['left'].set_visible(False)  # 去掉左边框
ax[3][1].spines['right'].set_visible(False)  # 去掉右边框
plt.show()
