import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def plot_fd(ax, plotdata, titlename=' ', legend=True, labels=None):
    from pandas.plotting import register_matplotlib_converters
    register_matplotlib_converters()
    import numpy as np
    import matplotlib.pyplot as plt
    from pylab import mpl
    mpl.rcParams['font.sans-serif'] = ['SimHei']
    mpl.rcParams['axes.unicode_minus'] = False

    spring = plotdata[plotdata.index.month.isin([4, 5])]
    summer = plotdata[plotdata.index.month.isin([6, 7, 8])]
    fall = plotdata[plotdata.index.month.isin([9, 10])]
    data = [spring, summer, fall]
    print(data)
    # labels = ['spring', 'summer', 'fall']
    # for i, color in enumerate(['tab:green', 'tab:red', 'tab:orange']):
    #     ax.scatter(data[i].index, np.array(data[i]), c=color, s=40, alpha=0.5, label=labels[i])
    if legend == True:
        ax.legend()

    data410 = plotdata[plotdata.index.month.isin(np.arange(4,11))][plotdata == 1]
    x = data410.index
    y = np.array(data410)

    ajtext = []
    for i in range(len(x)):
        text = ax.text(x[i], y[i]+0.02, str(x[i])[0:10], rotation=50)
        ajtext.append(text)
        # ax.plot([x[i], x[i]], [y[i]-0.01, y[i]-0.03*(i+1)], color='black', alpha=0.2)

    data410 = plotdata[plotdata.index.month.isin(np.arange(4, 11))][plotdata == 1.25]
    x = data410.index
    y = np.array(data410)
    for i in range(len(x)):
        text = ax.text(x[i], y[i] + 0.02, str(x[i])[0:10], rotation=50)
        ajtext.append(text)
        # ax.plot([x[i], x[i]], [y[i] + 0.01, y[i] + 0.03 * (i)], color='black', alpha=0.2)

    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates

    # years = mdates.YearLocator(3)  # every year
    # # months = mdates.MonthLocator(5)  # every month
    # yearsFmt = mdates.DateFormatter('%Y-%m')

    # format the ticks
    # ax.xaxis.set_major_locator(years)
    # ax.xaxis.set_major_formatter(yearsFmt)
    # ax.xaxis.set_minor_locator(months)

    # round to nearest years...
    # datemin = np.datetime64(str(plotdata.index.year[0]), 'Y')
    # datemax = np.datetime64(str(plotdata.index.year[-1]), 'Y') + np.timedelta64(1, 'Y')

    datemin = np.datetime64('2000', 'Y')
    datemax = np.datetime64('2018', 'Y') + np.timedelta64(1, 'Y')
    ax.set_xlim(datemin, datemax)
    ax.set_ylim(0.95, 1.45)

    ax.set_title(titlename)

    ax.set_yticks([1, 1.25])
    ax.set_yticklabels(['begin', 'end'])

    ax.set_xticklabels([])
    # xt = plotdata[plotdata.index.month.isin(np.arange(4, 11))][plotdata == 1]
    # ax.set_xticks(xt.index)
    # ax.set_xticklabels([str(i)[0:10] for i in xt.index])

    ax.grid(True, alpha=0.2)
    # from adjustText import adjust_text
    return


if __name__ == '__main__':
    area_name = ['xinan', 'dongbei', 'xibei', 'MRYR', 'south']
    area_name = [str(i) for i in range(7)]

    new_name = ['SW', 'NE', 'NW', 'MRYR', 'south']
    new_name = area_name
    fig = plt.figure(figsize=(9,8))
    plt.subplots_adjust(top=0.915,
bottom=0.05,
left=0.06,
right=0.935,
hspace=0.2,
wspace=0.2)
    listname = ['a) ', 'b) ', 'c) ', 'd) ', 'e']
    legend = [True, True, True, True, True]
    labels = [['AM：1', 'JJA：2', 'SO：1'],
              ['AM：2', 'JJA：2', 'SO：1'],
              ['AM：1', 'JJA：1', 'SO：1'],
              ['AM：3', 'JJA：2', 'SO：0'],
              ['AM：2', 'JJA：2', 'SO：1']]
    for i in range(7):
        result = pd.read_csv(r'datavarible1979 %s.csv' % area_name[i], index_col=0, parse_dates=True)
        result['mask'][(result['mask'] == 3)] = 1.25
        # plot_fd(fig.add_subplot(7,1,i+1), result['mask'][(result['mask'] == 1) | (result['mask'] == 1.25)],
        #         titlename=listname[i]+new_name[i], legend=legend[i], labels=labels[i])
        plot_fd(fig.add_subplot(7, 1, i + 1), result['mask'][(result['mask'] == 1) | (result['mask'] == 1.25)],
                titlename='1', legend=True, labels='1')

    plt.show()
    # plt.savefig('figure4.png', dpi=800)