import numpy as np
import pandas as pd
from pandas import DataFrame
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
import matplotlib.pyplot as plt

'''# cal corr'''

def get_cor_data(type):
    if type == 1:
        def mean_all_fd_into_one_fd(data, variable, absvalue=False, five_day=False):
            from current_function_in_fd import time_del
            def percentile_in_array(data):
                def calc_percentile(a, method='min'):
                    # 获得传入序列的百分位值
                    from scipy.stats import rankdata
                    import numpy as np
                    if isinstance(a, list):
                        a = np.asarray(a)
                    s = rankdata(a, method=method) / float(len(a))
                    result = [round(i, 3) for i in s]
                    return result

                osesr_reshapeinyear = np.array(data).reshape([19, 73])
                percentile = np.ones([19, 73])

                for i in range(73):
                    begin = osesr_reshapeinyear[:, i]
                    sbegin = calc_percentile(begin)
                    percentile[:, i] = sbegin
                return np.array(percentile).flatten()

            if absvalue is False:
                for i in data.columns[1:]:
                    data[i] = percentile_in_array(data[i])
            sfd = np.array(time_del(data.index[(data['mask'] == 1)]))
            efd = time_del(data.index[data['mask'] == 3])

            data_mean = np.zeros([len(sfd), 6])
            for i in range(len(sfd)):
                if five_day is not True:
                    # data_mean[i, 0:6] = data[variable].iloc[sfd[i]-6:sfd[i]]
                    data_mean[i, 0:5] = data[variable].iloc[sfd[i]:sfd[i] + 5]
                    data_mean[i, 5:6] = np.nanmean(data[variable].iloc[sfd[i] + 5:efd[i] + 1])
                else:
                    # data_mean[i, 0:4] = data[variable].iloc[sfd[i] - 4:sfd[i]]
                    data_mean[i, 0:3] = data[variable].iloc[sfd[i]:sfd[i] + 3]
                    data_mean[i, 3:4] = np.nanmean(data[variable].iloc[sfd[i] + 3:efd[i] + 1])

            return data_mean.flatten()


        etlist = ['sm', 'mpre', 'rootsoil']
        petlist = ['swdown', 'wind', 'tem', 'qair', 'mpre']

        mean_all_fd_into_one_fd(data5.copy(), 'et')
        # etcor = []
        # petcor = []
        # for i, data in enumerate([data1, data2, data5, data4]):
        #     if i == 2:
        #         fiveday=True
        #
        #     else:
        #         fiveday=False
        #     et = mean_all_fd_into_one_fd(data.copy(), 'et', five_day=fiveday)
        #     etreult = []
        #     for j in etlist:
        #         a = np.corrcoef(et, mean_all_fd_into_one_fd(data.copy(), j, five_day=fiveday))[0][1]
        #         etreult.append(a)
        #
        #     petresult = []
        #     pet = mean_all_fd_into_one_fd(data.copy(), 'pet', five_day=fiveday)
        #     for j in petlist:
        #         b = np.corrcoef(pet, mean_all_fd_into_one_fd(data.copy(), j, five_day=fiveday))[0][1]
        #         petresult.append(b)
        #     print(etcor)
        #     etcor.append(etreult)
        #     petcor.append(petresult)
        # # pd.DataFrame(etcor).to_csv('etcor.csv')
        # # pd.DataFrame(petcor).to_csv(('petcor.csv'))
        # pd.DataFrame(etcor).to_csv('etcor_onset.csv')
        # pd.DataFrame(petcor).to_csv(('petcor_onset.csv'))

    if type == 2:
        '''存储闪旱发生前:后数据'''
        data1 = pd.read_csv(r'datavarible1979 %s.csv' % area_name[0])
        data2 = pd.read_csv(r'datavarible1979 %s.csv' % area_name[1])
        data3 = pd.read_csv(r'datavarible1979 %s.csv' % area_name[2])
        data4 = pd.read_csv(r'datavarible1979 %s.csv' % area_name[3])
        data5 = pd.read_csv(r'datavarible1979 %s.csv' % area_name[4])

        from current_function_in_fd import time_del

        for i, data in enumerate([data1, data2, data3, data4, data5]):
            sfd = np.array(data.index[(data['mask'] == 1)])
            efd = np.array(data.index[data['mask'] == 3])
            sfd = sfd - 6
            efd = efd + 6
            loc = []
            for j in range(len(sfd)):
                loc.extend(range(sfd[j], efd[j]))
            data.iloc[loc].to_csv('data%i.csv' % (i + 1), index=False)

def mainplot(type):
    if type == 1:
        variable_name = ['swdown', 'wind', 'tem', 'qair', 'mpre']
        data = pd.read_csv('petcor.csv', header=0, index_col=0)
        xinan = np.array(data.iloc[0])
        dongbei = np.array(data.iloc[1])
        xibei = np.array(data.iloc[2])
        mryr = np.array(data.iloc[3])

        x = np.arange(0,10,2)  # the label locations
        width = 0.4  # the width of the bars


        fig, ax = plt.subplots(figsize=(8,5))
        ax.set_ylim(-1,1.1)
        rects1 = ax.bar(x - 1.5*width, xinan, width, label='Southwest')
        rects2 = ax.bar(x - width/2, dongbei, width, label='Northeast')
        rects3 = ax.bar(x + width/2, xibei, width, label='Northwest')
        rects4 = ax.bar(x + width*1.5, mryr, width, label='MRYR')
        plt.legend(loc='lower right')

        def autolabel(rects, label):
            """Attach a text label above each bar in *rects*, displaying its height."""
            lens = len(rects)
            for i in range(lens):
                rect = rects[i]
                label = variable_name[i]
                height = rect.get_height()
                if height > 0:
                    ax.annotate('%.2f'%height,
                                xy=(rect.get_x() + rect.get_width() / 2, height),
                                xytext=(0, 3),  # 3 points vertical offset
                                textcoords="offset points",
                                ha='center', va='bottom')
                    # ax.annotate(label,
                    #             xy=(rect.get_x() + rect.get_width() / 2, height-0.1),
                    #             xytext=(0, 3),  # 3 points vertical offset
                    #             textcoords="offset points",
                    #             ha='center', va='bottom')
                else:
                    ax.annotate('%.2f'%height,
                                xy=(rect.get_x() + rect.get_width() / 2, height-0.1),
                                xytext=(0, 3),  # 3 points vertical offset
                                textcoords="offset points",
                                ha='center', va='bottom')
                    # ax.annotate(label,
                    #             xy=(rect.get_x() + rect.get_width() / 2, height),
                    #             xytext=(0, 3),  # 3 points vertical offset
                    #             textcoords="offset points",
                    #             ha='center', va='bottom')
        print(rects1)
        autolabel(rects1, 'SW')
        autolabel(rects2,'NE')
        autolabel(rects3,'NW')
        autolabel(rects4,'MRYR')
        ax.fill_between([ax.get_xlim()[0],ax.get_xlim()[1]], -0.5, 0.5, facecolor='black',alpha=0.3)
        ax.set_xticks([0,2,4,6,8])
        ax.set_xticklabels(variable_name)
        # ax.set_title('')
        plt.show()
    if type == 2:
        import proplot as plot
        def plot_bar(ax, variable_name, data, title,width):

            data.columns = variable_name
            data = data.T
            data.columns = ['SW', 'NE', 'NW', 'MRYR']
            print(data)


            # Generate figure

            # Side-by-side bars
            obj = ax.bar(
                data, cycle='Reds',legend='lc',
                edgecolor='red9', legend_kw={ 'ncol': 4, 'labels': ['SW', 'NE', 'NW', 'MRYR']}
           ,width=width)

            def autolabel(rects):
                """Attach a text label above each bar in *rects*, displaying its height."""
                lens = len(rects)
                for i in range(lens):
                    rect = rects[i]
                    label = variable_name[i]
                    height = rect.get_height()
                    if height > 0.5:
                        ax.annotate('%.2f' % height,
                                    xy=(rect.get_x() + rect.get_width() / 2, height-0.05),
                                    xytext=(0, 3),  # 3 points vertical offset
                                    textcoords="offset points",
                                    ha='center', va='bottom')
                        # ax.annotate(label,
                        #             xy=(rect.get_x() + rect.get_width() / 2, height-0.1),
                        #             xytext=(0, 3),  # 3 points vertical offset
                        #             textcoords="offset points",
                        #             ha='center', va='bottom')
                    elif height < -0.5:
                        ax.annotate('%.2f' % height,
                                    xy=(rect.get_x() + rect.get_width() / 2, height - 0.2),
                                    xytext=(0, 3),  # 3 points vertical offset
                                    textcoords="offset points",
                                    ha='center', va='bottom')
                        # ax.annotate(label,
                        #             xy=(rect.get_x() + rect.get_width() / 2, height),
                        #             xytext=(0, 3),  # 3 points vertical offset
                        #             textcoords="offset points",
                        #             ha='center', va='bottom')
            for rect in obj:
                autolabel(rect)
            ax.fill_between([ax.get_xlim()[0],ax.get_xlim()[1]], -0.5, 0.5, facecolor='black',alpha=0.3)
            ax.format(
                xlocator=1, xminorlocator=0.5, ytickminor=False,
                title=title, ylim=[-1,1.25]
            )
            ax.axhline(0, color='k', linewidth=1)

        variable_name_for_et = ['SM', 'PRCP', 'RS']
        variable_name_for_pet = ['SWD', 'WIN', 'TEMP', 'SH']

        data_names = ['etcor', 'etcor_onset', 'petcor', 'petcor_onset']
        widths = [0.3,0.3,0.6,0.6]
        plot.rc.titleloc = 'uc'
        fig, ax = plot.subplots(nrows=2,ncols=2, aspect=2, axwidth=4.8, share=0, hratios=3,
                                figsize=[9, 7.5])

        for i, data_name in enumerate(data_names):
            data = pd.read_csv('%s.csv'%data_name, header=0, index_col=0)
            if i > 1:
                plot_bar(ax[i],variable_name_for_pet, data, data_name[0:3].upper(), widths[i])
            else:
                plot_bar(ax[i],variable_name_for_et, data, data_name[0:2].upper(), widths[i])
        ax.format(collabels=['Early Stage', 'Onset'], abc=True, abcloc='ul', abcstyle='(a)')
        # plt.show()
        plt.savefig('cor.jpg', dpi=800)
    if type == 3:
        variable_name = ['ET', 'LW', 'QH', 'QLE']
        xinan = [0.906, -0.65,-0.67,0.90]
        dongbei = [-0.60, -0.61, 0.73, -0.58]
        mryr = [-0.88,-0.33,0.79,-0.89]

        x = np.arange(0, 8, 2)  # the label locations
        width = 0.4  # the width of the bars

        fig, ax = plt.subplots(figsize=(8, 5))
        ax.set_ylim(-1, 1.1)
        rects1 = ax.bar(x - width, xinan, width, label='Southwest')
        rects2 = ax.bar(x, dongbei, width, label='Northeast')
        rects4 = ax.bar(x + width, mryr, width, label='MRYR')
        # plt.legend(loc='lower right')
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.1,
                         box.width, box.height * 0.9])

        # Put a legend below current axis
        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
                  fancybox=True, shadow=True, ncol=5)
        def autolabel(rects, label):
            """Attach a text label above each bar in *rects*, displaying its height."""
            lens = len(rects)
            for i in range(lens):
                rect = rects[i]
                label = variable_name[i]
                height = rect.get_height()
                if height > 0:
                    ax.annotate('%.2f' % height,
                                xy=(rect.get_x() + rect.get_width() / 2, height),
                                xytext=(0, 3),  # 3 points vertical offset
                                textcoords="offset points",
                                ha='center', va='bottom')
                    # ax.annotate(label,
                    #             xy=(rect.get_x() + rect.get_width() / 2, height-0.1),
                    #             xytext=(0, 3),  # 3 points vertical offset
                    #             textcoords="offset points",
                    #             ha='center', va='bottom')
                else:
                    ax.annotate('%.2f' % height,
                                xy=(rect.get_x() + rect.get_width() / 2, height - 0.1),
                                xytext=(0, 3),  # 3 points vertical offset
                                textcoords="offset points",
                                ha='center', va='bottom')
                    # ax.annotate(label,
                    #             xy=(rect.get_x() + rect.get_width() / 2, height),
                    #             xytext=(0, 3),  # 3 points vertical offset
                    #             textcoords="offset points",
                    #             ha='center', va='bottom')

        autolabel(rects1, 'SW')
        autolabel(rects2, 'NE')
        autolabel(rects4, 'MRYR')
        ax.fill_between([ax.get_xlim()[0], ax.get_xlim()[1]], -0.6, 0.6, facecolor='black', alpha=0.3)
        ax.set_xticks([0, 2, 4, 6])
        ax.set_xticklabels(variable_name)
        # ax.set_title('')
        plt.show()
mainplot(2)

