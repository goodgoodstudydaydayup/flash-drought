import numpy as np
import pandas as pd
import xarray as xr
from nc4 import read_nc4, save_nc4
from scipy.stats import pearsonr
from scipy.stats import skew
from plot_new_method import plot_single_ax
from plot_in_given_levels import plotcors
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

'''Figure 1 '''
lats = read_nc4(r'D:\basis\fd\data\china lat')
lons = read_nc4(r'D:\basis\fd\data\china lon')
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
fig = plt.figure(figsize = [9,5])
fig.subplots_adjust(bottom=0.02, top=0.97,
                    left=0.055, right=0.99, wspace=0.10)


'''temporal distribution and MK'''
# mask = read_nc4(r'D:\basis\fd\data\mask410')
# mask_seasons = [read_nc4(r'D:\basis\fd\data\maskspring'),
#                 read_nc4(r'D:\basis\fd\data\masksummer'),
#                 read_nc4(r'D:\basis\fd\data\maskfall')]
# def sums(mask):
#     mask = mask.reshape([40, int(mask.shape[0] / 40), 144, 252])
#     year = []
#     for i in range(40):
#         def sums(data):
#             mask = np.sum(data[np.where((data == 1))])
#             return mask
#         year.append(sums(mask[i]))
#     fd_year = np.array(year).flatten()
#     return fd_year
#
# np.savetxt('fd_yearspring.txt',sums(mask_seasons[0]))
# np.savetxt('fd_yearsummer.txt',sums(mask_seasons[1]))
# np.savetxt('fd_yearfall.txt',sums(mask_seasons[2]))
# fd_year = sums(mask)
# np.savetxt('fd_year.txt',fd_year)
# ax1 = fig.add_subplot(111)
# ax1.plot(list(range(1979, 2019)), fd_year, label='Annual')
# ax1.plot(list(range(1979, 2019)), sums(mask_seasons[0]), label='AM')
# ax1.plot(list(range(1979, 2019)), sums(mask_seasons[1]), label='JJA')
# ax1.plot(list(range(1979, 2019)), sums(mask_seasons[2]), label='SO')
# ax1.axvline(2000, color='grey', linestyle='--')
# ax1.legend()
# ax1.text(ax1.get_xlim()[0], ax1.get_ylim()[1], 'a) temporal distribution')
# ax1.grid()
# plt.show()
#
# # fig = plt.figure()
# # from MK_mutation import mk_analysis
# # mk_analysis(fig.add_subplot(2,2,1), fd_year, 1979, 2018)
# # mk_analysis(fig.add_subplot(2,2,2), sums(mask_seasons[0]), 1979, 2018)
# # mk_analysis(fig.add_subplot(2,2,3), sums(mask_seasons[1]), 1979, 2018)
# # mk_analysis(fig.add_subplot(2,2,4), sums(mask_seasons[2]), 1979, 2018)
#
# #
# # '''frequency of year'''
# # def fd_frequency(mask, pointshape=(40,73), freq='all'):
# #     '''!!!!!!!!!!!!!!!!!!!!!!警惕，此处只有all进行了修改，单年的并没有进行修改！！！！！！！！'''
# #     # 对频率进行计算，，可分为年或者全序列, 仅针对年份为58， 且
# #     def point_frequency(point_mask, freq='year'):
# #         if freq != 'year':
# #             point_mask = point_mask.reshape(pointshape)
# #             point_freq = len(np.argwhere(np.sum(point_mask, axis=1) > 0).flatten())
# #             return point_freq/pointshape[0]
# #
# #         else:
# #             point_mask = point_mask.reshape(pointshape)
# #             point_freq = np.zeros(pointshape[0])
# #             for i in range(pointshape[0]):
# #                 point_freq[i] = len(np.argwhere(point_mask[i, :] > 0).flatten()) / pointshape[1]
# #         return point_freq
# #
# #     if freq == 'year':
# #         fd_freq = np.zeros([pointshape[0], mask.shape[1], mask.shape[2]])
# #         for ilat in range(len(mask[0, :, 0])):
# #             for ilon in range(len(mask[0, 0, :])):
# #                 fd_freq[:, ilat, ilon] = point_frequency(mask[:, ilat, ilon].flatten(), freq=freq)
# #     else:
# #         fd_freq = np.zeros([mask.shape[1], mask.shape[2]])
# #         for ilat in range(len(mask[0, :, 0])):
# #             for ilon in range(len(mask[0, 0, :])):
# #                 fd_freq[ilat, ilon] = point_frequency(mask[:, ilat, ilon].flatten(), freq=freq)
# #     return fd_freq
# # mask = read_nc4(r'D:\basis\fd\data\mask410')
# # frequency = fd_frequency(mask, pointshape=(40,73))
# #
# # '''duration of fd over china'''
#
# duration = np.ones(mask.shape[1:3])
# for i in range(mask.shape[1]):
#     for j in range(mask.shape[2]):
#         smask = mask[:, i, j]
#
#         len_of_fd = len(np.argwhere(smask > 0).flatten())
#         number_of_fd = len(np.argwhere(smask == 1).flatten())
#
#         if number_of_fd == 0:
#             duration[i,j] = np.nan
#         else:
#             a = len_of_fd/number_of_fd
#             if a < 5:
#                 print(len_of_fd, number_of_fd)
#             duration[i, j] = len_of_fd/number_of_fd
#
#
# lats = read_nc4(r'D:\basis\fd\data\china lat')
# lons = read_nc4(r'D:\basis\fd\data\china lon')
# import matplotlib.pyplot as plt
# import cartopy.crs as ccrs
#
# plot_single_ax(fig.add_subplot(2,2,2, projection=ccrs.PlateCarree()), [[frequency*100, 'contourf', list(np.arange(0,40, 1))]]
#                , lons, lats, clipshape=True, colorbar='v', labels='b) percent of years',
#                ranges=[[55,15, 70, 138],[20,30,40,50],[80, 90, 100,110,120,130]])
# ax_jiuduanxian = fig.add_axes([0.815, 0.58, 0.125, 0.09], projection=ccrs.PlateCarree())
# plot_single_ax(ax_jiuduanxian,
#                [[frequency*100, 'contourf', list(np.arange(0,40, 1))]],
#                oslat=lats, oslon=lons, clipshape=True, gridline=False, shplinewidth=1.5,
#                ranges=[[25, 0, 105, 125], [], []])
#
# plot_single_ax(fig.add_subplot(2,2,3, projection=ccrs.PlateCarree()), [[duration, 'contourf', list(range(5,8))]]
#                , lons, lats, clipshape=True, colorbar='v', labels='c) duration distribution',
#                ranges=[[55,15, 70, 138],[20,30,40,50],[80, 90, 100,110,120,130]])
#
# ax_jiuduanxian = fig.add_axes([0.321, 0.06, 0.125, 0.09], projection=ccrs.PlateCarree())
# plot_single_ax(ax_jiuduanxian,
#                [[duration, 'contourf', list(range(5,8))]],
#                oslat=lats, oslon=lons, clipshape=True, gridline=False, shplinewidth=1.5,
#                ranges=[[25, 0, 105, 125], [], []])
# plt.subplots_adjust(wspace=0.13)
# # plt.show()
# plt.savefig('figure1.png', dpi=800)



'''composite analysis'''
def composite_analysis(name, method='all', months=None):
    '''method: all:全部的闪旱
                types: 两种类型闪旱
                no fd: 直接平均，用绝对值。'''
    if method == 'no fd':
        percentile_mask = read_nc4(r'D:\basis\fd\data\china1979 %s' % name).reshape([40, 73, 144, 252])
        percentile_mask = np.ma.masked_invalid(percentile_mask)
        percentile_mask = np.ma.masked_equal(percentile_mask, -9999)
    else:
        percentile_mask = read_nc4(r'D:\basis\fd\data\percentile china1979 %s'%name).reshape([40,73,144,252])
        percentile_mask = np.ma.masked_invalid(percentile_mask)
        percentile_mask = np.ma.masked_equal(percentile_mask, -9999)
    percentile_mask = read_nc4(r'D:\basis\fd\data\percentile china1979 %s' % name).reshape([40, 73, 144, 252])
    percentile_mask = np.ma.masked_invalid(percentile_mask)
    percentile_mask = np.ma.masked_equal(percentile_mask, -9999)
    if months is not None:
        percentile_mask_months = percentile_mask[:, months[0]:months[1]+1,:,:]
        rowmask_month = rowmask[:, months[0]:months[1]+1,:,:]
        mask_month = rowmask[:, months[0]:months[1]+1,:,:]
    else:
        percentile_mask_months = percentile_mask
        rowmask_month = rowmask
        mask_month = mask
    if method == 'all':
        # rowmask here is global variable
        mask_all = np.mean(np.ma.masked_where(rowmask_month<1, percentile_mask_months), axis=(0,1))
        return mask_all
    elif method == 'types':
        # mask here is global variable
        mask1 = np.mean(np.ma.masked_where(mask_month<4, percentile_mask_months), axis=(0,1))
        mask2 = np.mean(np.ma.masked_where((mask_month<1)|(mask_month>3), percentile_mask_months), axis=(0,1))
        return [mask1, mask2]
    elif method == 'no fd':
        a = np.nanmean(percentile_mask_months, axis=(0,1))
        a = np.ma.masked_invalid(a)
        return a
#
# rowmask = read_nc4(r'D:\basis\fd\data\mask410').reshape([40,73,144,252])
# mask = read_nc4(r'D:\basis\fd\data\type_mask').reshape([40,73,144,252])
#
# lats = read_nc4(r'D:\basis\fd\data\china lat')
# lons = read_nc4(r'D:\basis\fd\data\china lon')
#
# fig = plt.figure(figsize = [9,5])
# fig.subplots_adjust(bottom=0.1, top=0.95,
#                     left=0.04, right=0.95, wspace=0.02, hspace=0.1)
#
# method = 'all'
# levels = np.arange(0,1.1,0.1)
# contour = 'contourf'
# namelist = ['et', 'pet', 'sm', 'tem', 'pre', 'win', 'qair', 'swdown']
# newlist = ['ET', 'PET', 'SM', 'TEMP', 'PRCP', 'WIN', 'SH', 'SWD']
# number = ['a)','b)','c)','d)','e)','f)','g)','h)','i)','g)','k)','l)','m)','n)','o)','p)','q)','r)','s)','t)','u)','v)','w)','x)']
# for rows in range(8):
#     data1 = composite_analysis(namelist[rows], months=[18,30], method=method)
#     data2 = composite_analysis(namelist[rows], months=[31,48], method=method)
#     data3 = composite_analysis(namelist[rows], months=[49,60], method=method)
#     # data = np.array([data1, data2, data3])
#     # levels = np.linspace(np.nanmin(data, axis=None), np.nanmax(data,axis=None), 11)
#
#     plot_single_ax(fig.add_subplot(4, 6, rows*3+1, projection=ccrs.PlateCarree()),
#                    [[data1,  contour, levels]]
#                    , lons, lats, labels=number[rows*3] + newlist[rows] + ' spring', clipshape=True)
#
#     plot_single_ax(fig.add_subplot(4, 6, rows*3+2, projection=ccrs.PlateCarree()),
#                    [[data2, contour, levels]]
#                    , lons, lats, labels=number[rows*3+1]+ newlist[rows] + ' summer', clipshape=True)
#     fill = plot_single_ax(fig.add_subplot(4, 6, rows*3+3, projection=ccrs.PlateCarree()),
#                    [[data3, contour, levels]]
#                    , lons, lats, labels=number[rows*3+2]+ newlist[rows] + ' fall', clipshape=True)
#
#
# # for rows in range(8):
# #     plot_single_ax(fig.add_subplot(4, 6, rows*3+1, projection=ccrs.PlateCarree()),
# #                    [[composite_analysis(namelist[rows]), 'contourf', list(np.arange(0, 1.1, 0.1))]]
# #                    , lons, lats, labels=number[rows*3] + newlist[rows] + ' all', clipshape=True)
# #
# #     data = composite_analysis(namelist[rows], method='types')
# #     plot_single_ax(fig.add_subplot(4, 6, rows*3+2, projection=ccrs.PlateCarree()),
# #                    [[data[0], 'contourf', list(np.arange(0, 1.1, 0.1))]]
# #                    , lons, lats, labels=number[rows*3+1] + newlist[rows] + ' Type1', clipshape=True)
# #     fill = plot_single_ax(fig.add_subplot(4, 6, rows*3+3, projection=ccrs.PlateCarree()),
# #                    [[data[1], 'contourf', list(np.arange(0, 1.1, 0.1))]]
# #                    , lons, lats, labels=number[rows*3+2] + newlist[rows] + ' Type2', clipshape=True)
#
# cb_ax = fig.add_axes([0.1, 0.08, 0.8, 0.02])
# # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
# cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
# # plt.show()
# plt.savefig('figure2.png', dpi=800)

'''characteristics of two types of fd'''
def argindex_of_types_fd(vari):
    '''verify the events into two types based on tem percentile'''
    def del_types(smask, stem):
        sfd = np.argwhere(smask == 1).flatten()
        efd = np.argwhere(smask == 3).flatten()

        for i in range(len(sfd)):
            # if ((per_et[-1] < 0.5)&(per_et[0] < 0.5)):
            if ((np.mean(stem[sfd[i]: efd[i]+1]) < 0.5)):
                smask[sfd[i]] = 4
                smask[sfd[i]+1: efd[i]] = 5
                smask[efd[i]] = 6
        return smask
    mask = read_nc4(r'D:\basis\fd\data\mask410')
    tem = read_nc4(r'D:\basis\fd\data\percentile china %s 2000'%vari)

    type_mask = np.zeros(mask.shape)
    for i in range(mask.shape[1]):
        for j in range(mask.shape[2]):
            smask = mask[:, i, j]
            stem = tem[:, i, j]
            type_mask[:, i, j] = del_types(smask, stem)
    print(len(np.argwhere(type_mask == 4)))
    print(len(np.argwhere(type_mask == 1)))
    save_nc4(type_mask, 'type_mask %s' %vari)

# vari_list = ['pre', 'tem', 'et', 'wind', 'qair']
#
# type_mask = read_nc4('type_mask tem')
# print(len(np.argwhere(type_mask == 4)))
# print(len(np.argwhere(type_mask == 1)))

# a = read_nc4(r'D:\basis\fd\data\type_mask et')
# a[np.where(a >3)] = 0
# save_nc4(a, 'type1 et')
a = read_nc4(r'D:\basis\fd\data\type_mask et')
a[np.where(a <4)] = 0
a[np.where(a ==4)] = 1
a[np.where(a ==5)] = 2
a[np.where(a ==6)] = 3
save_nc4(a, r'D:\basis\fd\data\type2 et')
def argindex_for_months(months):
    '''selected date in different year sometimes may cause problem, use argindex may be more easier'''
    argindex = []
    label_for_groupby = []
    if months[0] > months[1]:  # Determine whether previous year's data is required
        for i in range(40):
            argindex.append(range((months[0] - 1) + 12 * i, months[1] + 12 + 12 * i))
            label_for_groupby.append(np.tile(i, 13 - months[0] + months[1]))
    else:
        for i in range(40):
            argindex.append(range((months[0] + 11) + 12 * i, months[1] + 12 + 12 * i))
            label_for_groupby.append(np.tile(i, months[1] - months[0] + 1))
    return np.array(argindex).flatten()

def statistic(data):
    statis = []
    statis.append(np.mean(data, axis=None))
    statis.append(np.max(data, axis=None))
    statis.append(np.min(data, axis=None))
    statis.append(np.std(data, axis=None))
    statis.append(np.median(data, axis=None))
    statis.append(skew(data,axis=None))
    return np.array(statis).flatten()

def corr_with_et(et, data):
    et = et.flatten()
    data = data.flatten()—
    return np.corrcoef(et, data)[0,1]

def main(months, monthname, area=None):
    # types_mask = read_nc4('type_mask')
    #
    # months_index = argindex_for_months(months)
    # types_mask = types_mask[months_index, :,:]
    # type1_index = np.where(types_mask > 3)
    # type2_index = np.where((types_mask <=3) & (types_mask >0))
    #
    # types = pd.DataFrame(index=['mean', 'max', 'min', 'std', 'median', 'skew'])
    # for i in ['sm', 'et', 'pet', 'tem', 'qair', 'qh', 'qle', 'rootsoil', 'swdown']:
    #     a = read_nc4(r'D:\basis\fd\data\percentile china1979 %s' %i)[months_index, :,:]
    #     # from scipy.stats.mstats import ttest_ind
    #     #
    #     # t,p = ttest_ind(a[type1_index], a[type2_index])
    #     # if p > 0.05:
    #     #     print(monthname, i)
    #     type1 = statistic(a[type1_index])
    #     type2 = statistic(a[type2_index])
    #     types['1%s' %i] = type1
    #     types['2%s' % i] = type2

    # corr

    et = read_nc4(r'D:\basis\fd\data\china1979 et')
    types_mask = read_nc4(r'D:\basis\fd\data\type_mask')
    if area is not None:
        et = et[:, area[0]:area[1]+1, area[2]:area[3]+1]
        types_mask = types_mask[:, area[0]:area[1] + 1, area[2]:area[3] + 1]
    months_index = argindex_for_months(months)
    types_mask = types_mask[months_index, :,:]
    type1_index = np.where(types_mask > 3)
    type2_index = np.where((types_mask <=3) & (types_mask >0))

    types = pd.DataFrame()
    for i in ['tem', 'qair', 'win']:
        a = read_nc4(r'D:\basis\fd\data\china1979 %s' %i)[months_index, :,:]
        if area is not None:
            a = a[:, area[0]:area[1] + 1, area[2]:area[3] + 1]
        type1 = corr_with_et(et[type1_index], a[type1_index])
        type2 = corr_with_et(et[type2_index], a[type2_index])
        print(monthname, i, type1)
        print(monthname, i, type2)
        types['1%s' %i] = type1
        types['2%s' % i] = type2
    # types.to_excel('corr_types_statistic %s.xlsx' % monthname)


# months = [[4,5,6,7,8,9,10]]
#
# area_name = ['yzr', 'xinan', 'dongbei', 'xinjiang']
# allarea = [[26.125, 36.125, 110.125, 120.125],
#            [24.125, 33.125, 100.125, 110.125],
#            [40.125, 50.125, 122.125, 134.125],
#            [40.125, 48.125, 80.125, 90.125]]
# from current_function_in_fd import china_del
#
# monthname = ['spring', 'summer', 'fall']
#
# for i in range(3):
#     main(months[0], area_name[i], china_del(allarea[i]))


'''corrlation_with_ 180+ index in weather bureau'''
def corrlation_with_three_climate_indexs(months, maskmonth=None):
    mask = read_nc4(r'D:\basis\fd\data\mask410')
    mask = mask.reshape([40, int(mask.shape[0] / 40), 144, 252])
    if maskmonth is not None:
        mask[:, maskmonth[0]:maskmonth[1]+1, :, :] = 0
    year = []
    for i in range(40):
        def sums(data):
            mask = np.sum(data[np.where((data == 1))])
            return mask
        year.append(sums(mask[i]))
    fd_year = np.array(year).flatten()

    def index_into_year(name, months):
        '''according to given months, average into year '''
        def argindex_for_months(months):
            '''selected date in different year sometimes may cause problem, use argindex may be more easier'''
            argindex = []
            label_for_groupby = []
            if months[0] > months[1]:   # Determine whether previous year's data is required
                for i in range(40):
                    argindex.append(range((months[0]-1) + 12*i, months[1]+12+12*i))
                    label_for_groupby.append(np.tile(i, 13-months[0]+months[1]))
            else:
                for i in range(40):
                    argindex.append(range((months[0]+11) + 12*i, months[1]+12+12*i))
                    label_for_groupby.append(np.tile(i, months[1]-months[0]+1))
            return np.array(argindex).flatten(), np.array(label_for_groupby).flatten()

        cindex = pd.read_csv(r'D:\basis\fd\data\%s.txt'%name, delim_whitespace=True, index_col=0)

        index = pd.date_range(start='1951-01', end='2019-11', freq='M')
        cindex = cindex.set_index(index)['1978':'2018']

        argindex, labels_for_groupby = argindex_for_months(months)
        cindex = cindex.iloc[argindex]
        print(months)
        print(cindex)
        print(labels_for_groupby)
        cindex['labels'] = labels_for_groupby
        index_inyear = cindex.groupby(cindex['labels']).mean().reset_index().set_index(pd.date_range(start='1979', end='2019', freq='Y'))
        index_inyear = index_inyear.drop(['labels'],axis=1)
        return index_inyear

    def cal_corr(index_inyear):
        index_inyear['fd'] = fd_year
        corr_name = []
        corr = []
        p_value = []
        for i in index_inyear.columns[:-1]:
            corr.append(pearsonr(index_inyear[i], index_inyear['fd'])[0])
            p_value.append(pearsonr(index_inyear[i], index_inyear['fd'])[1])
            corr_name.append(i)

        corr_between_fd_cindex = pd.DataFrame()
        corr_between_fd_cindex['corr'] = np.array(corr)
        corr_between_fd_cindex['name'] = np.array(corr_name)
        corr_between_fd_cindex['p'] = np.array(p_value)
        return corr_between_fd_cindex

    result = []
    for i in ['atm', 'oce']:
        index_inyear = index_into_year(i, months)
        result.append(cal_corr(index_inyear))

    return result

def seasons(season, months, maskmonth=None):
    mnames = np.array(range(1,8))
    with pd.ExcelWriter('corr_eventsover_china %s.xlsx' %season) as writer:
        for i in range(7):
            df = corrlation_with_three_climate_indexs(months[i], maskmonth)
            df[0].to_excel(writer, sheet_name='%s atm' % mnames[i])
            df[1].to_excel(writer, sheet_name='%s oce' % mnames[i])

# months = [
#     [[4,10], [3,9],[2,8],[1,7],[12,6],[11,5],[10,4]],
#     [[4,5], [3,4],[2,3],[1,2],[12,1],[11,2],[10,3]],
#     [[6,8], [5,7],[4,6],[3,5],[2,4],[1,3],[12,2]],
#     [[9,10], [8,9],[7,8],[6,7],[5,6],[4,5],[3,4]]
#           ]
# maskmonth = [None, [18,30], [31,48], [49,60]]
# season = ['all', 'spring', 'summer', 'fall']
# for i in range(4):
#     seasons(season[i], months=months[i], maskmonth=maskmonth[i])


'''cor with SST'''
'''这个有问题，用之前做的。
其实也没问题，主要是不知道如何在11，12，1，2，3这种月份进行取值。之后再说吧，等需要的时候。'''
# def get_before_after_period_data(year, month, range=None):
    # if range is not None:
    #     data = data.sel(lats=slice(range[0]), lons=slice(range[1]))
    # beginmonth = month[0]
    # endmonth = month[1]
    # if beginmonth > endmonth:
    #     year = [year[0]-1, year[1]]
    #
    # sst = xr.open_dataset(r'D:\basis\fd\data\sst.mnmean.v4.nc').sel(time=slice('%i'%year[0],'%i'%year[1]))
    # def is_amj(sstmonth, month):
    #     if month[1] > 12:
    #         return (sstmonth >= month[0]) | (sstmonth <= month[1]-12)
    #     return (sstmonth >= month[0]) & (sstmonth <= month[1])
    # fano_data = sst.sel(time=is_amj(sst['time.month'], month))
    #
    # fano_data = sst.sel(time=slice('%i-%i'%(year[0], beginmonth),
    #                                '%i-%i'%(year[1], endmonth))).groupby('time.year').mean(dim=('time'))
    #
    #
    # return fano_data.variables['sst']

# data = read_nc4(r'D:\basis\fd\data\type_mask')
# data[np.where(data>3)] =0
# # data[np.where(data==4)] =1
# # data[np.where(data==5)] =2
# # data[np.where(data==6)] =3
# save_nc4(data, r'D:\basis\fd\data\type2_mask')
# allarea = [[26.125, 36.125, 110.125, 120.125],
#            [24.125, 33.125, 100.125, 110.125],
#            [40.125, 50.125, 122.125, 134.125]]

# from current_function_in_fd import china_del
#
# number = []
# for i in allarea:
#     area=china_del(i)
#     mask = data[:, :, area[0]:area[1]+1, area[2]:area[3]+1]
#     year = []
#     for i in range(40):
#         def sums(data):
#             mask = np.sum(data[np.where((data == 1))])
#             return mask
#         year.append(sums(mask[i]))
#     number.append(np.array(year).flatten())
# print(np.sum(np.array(number), axis=0))
# np.savetxt('regionyzr.txt', number[0])
# np.savetxt('regionxinan.txt', number[1])
# np.savetxt('regiondongbei.txt', number[2])