import numpy as np
import pandas as pd
import xarray as xr
from nc4 import read_nc4, save_nc4
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False
def forward_regression(X, y,
                       threshold_in,
                       verbose=False):
    initial_list = []
    included = list(initial_list)
    while True:
        changed=False
        excluded = list(set(X.columns)-set(included))
        new_pval = pd.Series(index=excluded)
        for new_column in excluded:
            if (len(included) == 0):
                model = sm.OLS(y, sm.add_constant(pd.DataFrame(X[included+[new_column]]))).fit()
            else:
                model = sm.OLS(y, pd.DataFrame(X[included + [new_column]])).fit()
            new_pval[new_column] = model.pvalues[new_column]
        best_pval = new_pval.min()
        if best_pval < threshold_in:
            best_feature = new_pval.idxmin()
            included.append(best_feature)
            changed=True
            if verbose:
                print('Add  {:30} with p-value {:.6}'.format(best_feature, best_pval))

        if not changed:
            break

    return included

def backward_regression(X, y,
                           threshold_out,
                           verbose=False):
    included=list(X.columns)
    while True:
        changed=False
        if (len(included) == 1):
            model = sm.OLS(y, sm.add_constant(pd.DataFrame(X[included]))).fit()
        else:
            model = sm.OLS(y, sm.add_constant(pd.DataFrame(X[included]))).fit()
        # use all coefs except intercept
        pvalues = model.pvalues.iloc[1:]
        worst_pval = pvalues.max() # null if pvalues is empty
        if worst_pval > threshold_out:
            changed=True
            worst_feature = pvalues.idxmax()
            included.remove(worst_feature)
            if verbose:
                print('Drop {:30} with p-value {:.6}'.format(worst_feature, worst_pval))
        if not changed:
            break
    return included


'''temporal distribution and MK'''

def sums(mask,range=None):
    mask = mask.reshape([19, int(mask.shape[0] / 19), mask.shape[1], mask.shape[2]])
    if range is not None:
        from current_function_in_fd import china_del
        rangenumber = china_del(range)
        mask = mask[:,:, rangenumber[0]:rangenumber[1], rangenumber[2]:rangenumber[3]]
    year = []
    for i in np.arange(19):
        def sums(data):
            mask = np.sum(data[np.where((data == 1))])
            return mask
        year.append(sums(mask[i]))
    fd_year = np.array(year).flatten()
    return fd_year

def mean_varible(mask, range=None):
    mask = np.ma.masked_equal(mask, -9999)
    mask = np.ma.masked_invalid(mask)
    mask = mask.reshape([19, int(mask.shape[0] / 19), mask.shape[1], mask.shape[2]])
    if range is not None:
        from current_function_in_fd import china_del
        rangenumber = china_del(range)
        mask = mask[:,:, rangenumber[0]:rangenumber[1], rangenumber[2]:rangenumber[3]]
    year = []
    for i in np.arange(19):
        def sums(sdata):
            mask = np.nanmean(sdata)
            return mask
        year.append(sums(mask[i]))
    fd_year = np.array(year).flatten()
    return fd_year


def output_year_index(range, save_name, masktype, seasontype):
    year_mean_series = []
    for i in namelist:
        mask = read_nc4(r'D:\basis\fd\data\china %s 2000' %i)
        year_mean_series.append(mean_varible(mask, range=range))
    np.savetxt('year_mean_%s.txt' % save_name, np.array(year_mean_series))

    mask = read_nc4(r'D:\basis\fd\data\%s' %masktype)
    np.savetxt('year_index_%s.txt' % save_name, sums(mask, range=range))

    mask_seasons = [read_nc4(r'D:\basis\fd\data\%sspring'%seasontype),
                    read_nc4(r'D:\basis\fd\data\%ssummer'%seasontype),
                    read_nc4(r'D:\basis\fd\data\maskfall')]
    maskseason = np.array([sums(mask_seasons[0], range),
                           sums(mask_seasons[1], range),
                           sums(mask_seasons[2], range)])
    np.savetxt('maskseason_%s.txt'%save_name, maskseason)

namelist = ['et', 'pet', 'sm', 'swdown', 'wind', 'tem', 'qair', 'pre', 'root','lw', 'sw', 'qh', 'qle']
# newlist = ['e) ET', 'f) PET', 'g) SM', 'h) SWD', 'i) WIN', 'g) TEMP','k) SH', 'l) PRCP',
#            'm) RS', 'n) LW', 'o) SW', 'p) QH', 'q) QLE']
newlist = ['e) ET (mm)', 'f) PET (mm)', 'g) SM (mm)', 'h) SWD (J/m2)', 'i) WIN (m/s)', 'g) TEMP (T)',
           'k) SH (g/g)', 'l) PRCP (mm)',
           'm) RS (mm)', 'n) LW (J/m2)', 'o) SW (J/m2)', 'p) QH (J/m2)', 'q) QLE (J/m2)']
'''将所有的变量都转化为指数，存入txt，预备给calculate使用'''
allarea = [[24.125, 33.125, 102.125, 108.125],
           [40.125, 50.125, 122.125, 128.125],
           [36, 47, 87, 98]]
masktype = 'mask410'
seasontype = 'mask'
# output_year_index(None, 'all', masktype, seasontype)
# output_year_index(allarea[0], 'xinan', masktype, seasontype)
# output_year_index(allarea[1], 'dongbei', masktype, seasontype)
# output_year_index(allarea[2], 'xinjiang', masktype, seasontype)


def calculate(save_name, area_name, color, ssi):
    '''绘制给定数据的plot图，然后计算他们的逐步回归以及相关系数'''
    mask_year = np.loadtxt('year_index_%s.txt'%save_name)
    year_mean = np.loadtxt('year_mean_%s.txt'%save_name)
    mask_seasons = np.loadtxt('maskseason_%s.txt'%save_name)
    corr = []
    for i in range(13):
        x = pd.Series(year_mean[i])
        y = pd.Series(mask_year)
        corr.append(y.corr(x))
    a = pd.DataFrame(corr)
    a['variable'] = newlist
    a.to_excel('corr_%s.xlsx' % save_name)

    fr = []
    ym = pd.DataFrame(year_mean.T, columns=newlist)
    fr.append(forward_regression(ym, mask_year, 0.05))
    print(forward_regression(ym, mask_year, 0.05))

    # colors = ['black', 'forestgreen', 'darkgreen', 'r', 'lightcoral', 'navy', 'mediumpurple', 'indigo',
    #           'royalblue', 'dimgray', 'darkorange','red', 'tab:blue']

    def plot_frequency(ax):
        ax.plot(list(range(2000, 2019, 1)), mask_year, label='Annual', color='black')
        ax.plot(list(range(2000, 2019, 1)), mask_seasons[0], label='AM')
        ax.plot(list(range(2000, 2019, 1)), mask_seasons[1], label='JJA')
        ax.plot(list(range(2000, 2019, 1)), mask_seasons[2], label='SO')
        # ax.legend()
        ax.set_title('%s frequency' % area_name)
        # ax.axvline(2000, color='grey', linestyle='--')
        ax.grid()
    #
    # plot_frequency(ax[0][ssi])


    for si in range(4):
        for sj in range(4):

            ax1 = ax[si][sj]
            i = 4 * si + sj
            if i <= 12:
                if i == 5:
                    ax1.plot(list(range(2000, 2019)), year_mean[i] - 273, color=color)
                    # ax1.set_ylabel('T')
                else:
                    ax1.plot(list(range(2000, 2019)), year_mean[i], color=color)
                ax1.set_title(newlist[i])
                ax1.grid()
            if i >=9:
                ax1.set_xlabel('年')
            if i > 12:
                sax = ax[si][sj]
                sax.get_yaxis().set_visible(False)  # 不显示y轴
                sax.get_xaxis().set_visible(False)
                sax.spines['top'].set_visible(False)  # 去掉上边框
                sax.spines['bottom'].set_visible(False)  # 去掉下边框
                sax.spines['left'].set_visible(False)  # 去掉左边框
                sax.spines['right'].set_visible(False)  # 去掉右边框
            ax1.grid()

            # ax1.axvline(2000, color='grey', linestyle='--')

    # plt.show()
    # plt.savefig('%s.png'%area_name, dpi=800)
def calculate_china(save_name, area_name, seasontype, ssi=None, ylim=None):
        mask_year = np.loadtxt('year_index_%s.txt' % save_name)
        mask_seasons = np.loadtxt('maskseason_%s.txt' %save_name)

        def plot_frequency(ax):
            ax.plot(list(range(2000, 2019, 1)), mask_year, label='Annual', color='red')
            ax.plot(list(range(2000, 2019, 1)), mask_seasons[0], label='AM', color='blue')
            ax.plot(list(range(2000, 2019, 1)), mask_seasons[1], label='JJA', color='green')
            ax.plot(list(range(2000, 2019, 1)), mask_seasons[2], label='SO', color='black')
            # ax.legend()
            ax.set_title('%s 年频率 (次)' % area_name)
            # ax.axvline(2000, color='grey', linestyle='--')
            ax.grid()
            ax.set_xlabel('年')
            if ylim is not None:
                ax.set_ylim(ylim)
        if ssi is not None:
            plot_frequency(ax[ssi])


# fig,ax = plt.subplots(4,4, figsize=[9,9])
# plt.subplots_adjust(top=0.955,
#                     bottom=0.065,
#                     left=0.05,
#                     right=0.995,
#                     hspace=0.37,
#                     wspace=0.335)
# area_name_list = ['a) 全年', 'b) 西南', '东北', ' ']
# save_name = ['all', 'xinan', 'dongbei', 'xinjiang']
# colors = ['red', 'blue', 'green', 'black']
# for i in range(4):
#     calculate(save_name[i], area_name_list[i], colors[i], i)
# plt.show()
# plt.savefig('figure3_2.png', dpi=800)

# fig,ax = plt.subplots(1,4, figsize=[9,3])
# plt.subplots_adjust(top=0.90,
# bottom=0.165,
# left=0.045,
# right=0.995,
# hspace=0.37,
# wspace=0.335)
# area_name_list = ['a) 全国', 'b) 西南', 'c) 东北', 'd) 西北']
# save_name = ['all', 'xinan', 'dongbei', 'xinjiang']
# colors = ['red', 'blue', 'green', 'black']
# ylims = [None, [0,700], [0,700],[0,700]]
# for i in range(4):
#     calculate_china(save_name[i], area_name_list[i], seasontype, i, ylims[i])
# plt.show()
# plt.savefig('figure3.png', dpi=800)


fig = plt.figure(figsize=[9, 0.3])
plt.plot([0,1],[0,0], alpha=0)

plt.plot([0.0,0.1],[0,0], color='red')
plt.text(0.12, -0.03, "全国"
           )

plt.plot([0.25,0.35],[0,0], color='blue')
plt.text(0.37, -0.03, "西南"
           )

plt.plot([0.5,0.6],[0,0], color='green')
plt.text(0.62, -0.03, "东北"
           )

plt.plot([0.75,0.85],[0,0], color='black')
plt.text(0.87, -0.03, "西北"
           )

sax = plt.gca()
sax.get_yaxis().set_visible(False)  # 不显示y轴
sax.get_xaxis().set_visible(False)
sax.spines['top'].set_visible(False)  # 去掉上边框
sax.spines['bottom'].set_visible(False)  # 去掉下边框
sax.spines['left'].set_visible(False)  # 去掉左边框
sax.spines['right'].set_visible(False)
plt.show()


# fig = plt.figure(figsize=[9, 0.3])
# plt.plot([0,1],[0,0], alpha=0)
#
#
# plt.plot([0.0,0.1],[0,0], color='blue')
# plt.text(0.12, -0.03, "西南"
#            )
#
# plt.plot([0.45,0.55],[0,0], color='green')
# plt.text(0.57, -0.03, "东北"
#            )
#
# plt.plot([0.9,1],[0,0], color='black')
# plt.text(1.02, -0.03, "西北"
#            )
#
# sax = plt.gca()
# sax.get_yaxis().set_visible(False)  # 不显示y轴
# sax.get_xaxis().set_visible(False)
# sax.spines['top'].set_visible(False)  # 去掉上边框
# sax.spines['bottom'].set_visible(False)  # 去掉下边框
# sax.spines['left'].set_visible(False)  # 去掉左边框
# sax.spines['right'].set_visible(False)
# plt.show()