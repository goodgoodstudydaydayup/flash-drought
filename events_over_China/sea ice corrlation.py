import xarray as xr
import pandas as pd
import numpy as np

# sea_ice = xr.open_dataset(r'D:\basis\data\HadISST_ice.nc')
# print(sea_ice)



def names(name):
    a = pd.read_csv(r'D:\basis\data\N_extent_v3.0.csv')
    print(a)
    a[a == -9999] = np.nan
    sea_ice = a[(a[' mo']==4) | (a[' mo']==5)].groupby('year').mean().reset_index().set_index('year').iloc[1:41]

    fd_year = np.loadtxt(r'D:\basis\fd\events_over_China\fd_year%s.txt'%name)
    sea_ice['fd_year'] = fd_year
    sea_ice.columns = [' mo', 'extent', 'area', 'fd_year']
    print(sea_ice[['fd_year', 'area']].corr())


a = pd.read_csv(r'D:\basis\data\N_extent_v3.0.csv')
a[a == -9999] = np.nan
sea_ice = a.groupby('year').mean().reset_index().set_index('year').iloc[1:41]

fd_year = np.loadtxt(r'D:\basis\fd\events_over_China\fd_year.txt')
fd_year = np.loadtxt(r'D:\basis\fd\events_over_China\regionyzr.txt')
# fd_year = np.loadtxt(r'D:\basis\fd\events_over_China\regionxinan.txt')
# fd_year = np.loadtxt(r'D:\basis\fd\events_over_China\regiondongbei.txt')
sea_ice['fd_year'] = fd_year
sea_ice.columns = [' mo', 'extent', 'area', 'fd_year']
print(sea_ice[['fd_year', 'area']].corr())
# names('spring')
# names('summer')
# names('fall')
