import numpy as np
import pandas as pd


def fit_line2(x, y):
    from scipy.stats import linregress
    from scipy.stats.mstats import zscore
    (beta_coeff, intercept, rvalue, pvalue, stderr) = linregress(zscore(x), zscore(y))
    return beta_coeff


# data = pd.read_csv(r'D:\basis\data\row\EVP.txt', delim_whitespace=True, header=None,
#                               index_col=None, usecols=[0, 4, 5, 6, 7, 8],
#                               names=['station', 'year', 'month', 'day', 'min', 'max'])
# # data = pd.read_csv('data10000.csv', index_col=None)
# datatime = pd.to_datetime(data['year'] * 10000 + data['month'] * 100 + data['day'], format='%Y%m%d')
# data = data.set_index(datatime)
# data = data.drop(['year','month', 'day'], axis=1)
# data['min'][data['min'] == 32766] = np.nan
# data['max'][data['max'] == 32766] = np.nan
# data.to_csv('data.csv')

# datatime = pd.to_datetime(data.index)
# data = data.set_index(datatime)

#######￥￥####################
# data = pd.read_csv('data.csv', index_col=1)
# def counts(data):
#     names = data.name
#     data = data.dropna()
#     a,b = data.count()
#     if a > 200 and b > 200:
#         return
#     else:
#         dellist.append(names)
# dellist = []
# data.drop('Unnamed: 0',axis=1).groupby(data.index).apply(lambda a: counts(a))
# data = data.drop(dellist)
# np.savetxt('dellist.txt', dellist, fmt='%i')
# data.to_csv('compare.csv')


data = pd.read_csv('compare.csv', index_col=1)
del data.index.name
def change(data):
    names = data.name
    sdata = data.dropna()
    a = fit_line2(np.array(sdata['min']), np.array(sdata['max']))
    data.loc[data['max'].isnull(),'max'] = data[data['max'].isnull()]['min'] * a
    return data['max']

wronglist = []
ss = data.groupby(data['station']).apply(lambda a: change(a)).T.reindex()

np.savetxt('wronglist.txt', wronglist, fmt='%i')
ss.to_csv('result.csv')