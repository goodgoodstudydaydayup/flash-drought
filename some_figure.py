
import numpy as np
import pandas as pd
import xarray as xr
from nc4 import read_nc4, save_nc4
from scipy.stats import pearsonr
from scipy.stats import skew
from plot_new_method import plot_single_ax

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['SimHei']
mpl.rcParams['axes.unicode_minus'] = False


'''Figure 1 '''



def cal_duration(mask):
    duration = np.ones(mask.shape[1:3])
    for i in range(mask.shape[1]):
        for j in range(mask.shape[2]):
            smask = mask[:, i, j]

            len_of_fd = len(np.argwhere(smask > 0).flatten())
            number_of_fd = len(np.argwhere(smask == 1).flatten())

            if number_of_fd == 0:
                duration[i,j] = np.nan
            else:
                a = len_of_fd/number_of_fd
                if a < 5:
                    print(len_of_fd, number_of_fd)
                duration[i, j] = len_of_fd/number_of_fd
    return duration


lats = read_nc4(r'D:\basis\fd\data\china lat')
lons = read_nc4(r'D:\basis\fd\data\china lon')
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

def plot_standard_foc(duration, ax, ax_jiuduanxinposition, ranges, title):

    # foc = read_nc4('ss')
    # print(foc)
    from plot_new_method import plot_single_ax
    import matplotlib.pyplot as plt

    slat = read_nc4(r'D:\basis\fd\data\china lat')
    slon = read_nc4(r'D:\basis\fd\data\china lon')

    fill, cbar = plot_single_ax(ax,
                   [
                    [duration, 'contourf', ranges]
                   ],
                   oslat=slat, oslon=slon, clipshape=True, gridline=True, colorbar='v',
                          ranges=[[55,15, 70, 138],[20,30,40,50],[80,90,100,110,120,130]])
    ax_jiuduanxian = fig.add_axes(ax_jiuduanxinposition, projection=ccrs.PlateCarree())
    plot_single_ax(ax_jiuduanxian,
                   [
                    [duration, 'contourf', np.arange(0,20,2.5)]
                   ],
                   oslat=slat, oslon=slon, clipshape=True, gridline=False, shplinewidth=1.5,
                          ranges=[[25,0, 105, 125],[],[]])

    def areas(area, color, label):
        x1 = area[0]
        x2 = area[1]
        y1 = area[2]
        y2 = area[3]
        area = np.array([[x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1]])
        ax.plot(area[1], area[0], transform=ccrs.PlateCarree(), linewidth=2, color=color, label=label)
    # areas(allarea[0], 'red', 'Yangtz-River')
    areas(allarea[0], 'blue', 'Southwest')
    areas(allarea[1], 'green', 'Northeast')
    areas(allarea[2], 'black', '新疆')
    # ax.legend(loc='upper left')
    ax.set_title(title)

    cbar.set_ticks(ranges[::2])
    cbar.set_ticklabels([int(i) for i in ranges[::2]])
    return fill



# fig, ax = plt.subplots(2,2, sharex='col', sharey='row',figsize=(9,5), subplot_kw={'projection': ccrs.PlateCarree(central_longitude=180)})
# plt.subplots_adjust(top=0.965,
# bottom=0.05,
# left=0.06,
# right=0.94,
# hspace=0.2,
# wspace=0.2)
# dataname = ['mask410', 'maskspring', 'masksummer', 'maskfall']
# ax_position = [[0.362, 0.563,0.08,0.12],
#                [0.845, 0.563,0.08,0.12],
#                [0.362,0.062,0.08,0.12],
#                [0.845,0.062,0.08,0.12]]
# # ranges=[np.arange(5,10,0.5),
# #         np.arange(5,10,0.5),
# #         np.arange(5,10,0.5),
# #         np.arange(5,10,0.5)]
#
# ranges=[np.arange(0,6.5,0.5),
#         np.arange(0,4,0.5),
#         np.arange(0,4,0.5),
#         np.arange(0,3,0.5)]
#
# allarea = [[24.125, 33.125, 102.125, 108.125],
#            [40.125, 50.125, 122.125, 128.125],
#            [36, 47, 87, 98]]
#
#
# title = ['a) 持续时间(侯) 全年', 'b) 持续时间(侯) 春季', 'c) 持续时间(侯) 夏季', 'd) 持续时间(侯) 秋季']
# for i in range(2):
#     for j in range(2):
#         count = i*2+j
#         fill = plot_standard_foc(cal_duration(read_nc4(r'D:\basis\fd\data\%s'%dataname[count])), ax[i][j],
#                                  ax_position[count], ranges[count], title[count])
# plt.show()


def composite_analysis(name, method='all', months=None):
    '''method: all:全部的闪旱
                types: 两种类型闪旱
                no fd: 直接平均，用绝对值。'''
    if method == 'no fd':
        percentile_mask = read_nc4(r'D:\basis\fd\data\china %s 2000' % name, name).reshape([19,73,156,252])
        percentile_mask = np.ma.masked_invalid(percentile_mask)
        percentile_mask = np.ma.masked_equal(percentile_mask, -9999)
    else:
        percentile_mask = read_nc4(r'D:\basis\fd\data\percentile china %s 2000' % name).reshape([19,73,156,252])
        percentile_mask = np.ma.masked_invalid(percentile_mask)
        percentile_mask = np.ma.masked_equal(percentile_mask, -9999)
    percentile_mask = read_nc4(r'D:\basis\fd\data\percentile china %s 2000' % name).reshape([19,73,156,252])
    percentile_mask = np.ma.masked_invalid(percentile_mask)
    percentile_mask = np.ma.masked_equal(percentile_mask, -9999)
    if months is not None:
        percentile_mask_months = percentile_mask[:, months[0]:months[1]+1,:,:]
        rowmask_month = rowmask[:, months[0]:months[1]+1,:,:]
        mask_month = rowmask[:, months[0]:months[1]+1,:,:]
    else:
        percentile_mask_months = percentile_mask
        rowmask_month = rowmask
        mask_month = mask
    if method == 'all':
        # rowmask here is global variable
        mask_all = np.mean(np.ma.masked_where(rowmask_month<1, percentile_mask_months), axis=(0,1))
        return mask_all
    elif method == 'types':
        # mask here is global variable
        mask1 = np.mean(np.ma.masked_where(mask_month<4, percentile_mask_months), axis=(0,1))
        mask2 = np.mean(np.ma.masked_where((mask_month<1)|(mask_month>3), percentile_mask_months), axis=(0,1))
        return [mask1, mask2]
    elif method == 'no fd':
        a = np.nanmean(percentile_mask_months, axis=(0,1))
        a = np.ma.masked_invalid(a)
        return a

rowmask = read_nc4(r'D:\basis\fd\data\mask410').reshape([19,73,156,252])
mask = read_nc4(r'D:\basis\fd\data\type_mask et').reshape([19,73,156,252])

lats = read_nc4(r'D:\basis\fd\data\china lat')
lons = read_nc4(r'D:\basis\fd\data\china lon')

# fig = plt.figure(figsize = [9,5])
# fig.subplots_adjust(bottom=0.1, top=0.95,
#                     left=0.04, right=0.95, wspace=0.02, hspace=0.1)
#
# fig = plt.figure(figsize = [9,6])
# fig.subplots_adjust(bottom=0.1, top=0.95,
#                     left=0.005, right=0.995, wspace=0.02, hspace=0.15)
#
method = 'all'
levels = np.arange(0,1.1,0.1)
contour = 'contourf'
save_name = ['root', 'lw', 'sw', 'qh', 'qle']
namelist = ['et', 'pet', 'sm', 'tem', 'pre', 'wind', 'qair', 'swdown','root', 'lw', 'sw', 'qh', 'qle']
newlist = ['ET', 'PET', 'SM', 'TEMP', 'PRCP', 'WIN', 'SH', 'SWD','RS', 'LW', 'SW', 'QH', 'QLE']
number = ['a)','b)','c)','d)','e)','f)','g)','h)','i)','g)','k)','l)','m)','n)','o)','p)','q)','r)','s)','t)','u)','v)',
          'w)','x)', 'y)', 'z)', 'aa)', 'ab)', 'ac)','ad)','ae)','af)', 'ag)', 'ah)', 'ai)', 'ag)', 'ak)',
          'al)', 'am)', 'an)', 'ao)','ap)']


def main(type):
    if type == 1:
        for rows in range(13):
            fig = plt.figure(figsize=[9, 14.5])
            fig.subplots_adjust(bottom=0.02, top=0.98,
                                left=0.005, right=0.995, wspace=0.02, hspace=0.15)
            data1 = composite_analysis(namelist[rows], months=[18,30], method=method)
            data2 = composite_analysis(namelist[rows], months=[31,48], method=method)
            data3 = composite_analysis(namelist[rows], months=[49,60], method=method)
            # data = np.array([data1, data2, data3])
            # levels = np.linspace(np.nanmin(data, axis=None), np.nanmax(data,axis=None), 11)

            plot_single_ax(fig.add_subplot(3, 1, 1, projection=ccrs.PlateCarree()),
                           [[data1,  contour, levels]]
                           , lons, lats,gridline=True, labels=number[0] + newlist[rows] + ' 春季', clipshape=True,
                           ranges=[[57,15, 70, 138],[20,30,40,50],[80,90,100,110,120,130]])

            plot_single_ax(fig.add_subplot(3, 1, 2, projection=ccrs.PlateCarree()),
                           [[data2, contour, levels]]
                           , lons, lats,gridline=True, labels=number[1]+ newlist[rows] + ' 夏季', clipshape=True
                           , ranges=[[57, 15, 70, 138], [20, 30, 40, 50], [80, 90, 100, 110, 120, 130]]
                           )
            fill = plot_single_ax(fig.add_subplot(3, 1,  3, projection=ccrs.PlateCarree()),
                           [[data3, contour, levels]]
                           , lons, lats,gridline=True, labels=number[2]+ newlist[rows] + ' 秋季', clipshape=True
                            , ranges = [[57, 15, 70, 138], [20, 30, 40, 50], [80, 90, 100, 110, 120, 130]]
                           )


            ax_jiuduanxian = fig.add_axes([0.748,0.69085,0.15,0.08], projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                            [data1,  contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5,
                                  ranges=[[25,0, 105, 125],[],[]])

            ax_jiuduanxian = fig.add_axes([0.748,0.3575,0.15,0.08], projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                            [data2,  contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5,
                                  ranges=[[25,0, 105, 125],[],[]])

            ax_jiuduanxian = fig.add_axes([0.748,0.024,0.15,0.08], projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                            [data3,  contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5,
                                  ranges=[[25,0, 105, 125],[],[]])
            # plt.show()
            plt.savefig('%s.png'%newlist[rows])
            plt.close()
    if type == 2:
        namelist = ['et', 'pet', 'sm', 'tem', 'pre', 'wind', 'qair', 'swdown', 'root', 'lw', 'sw', 'qh', 'qle']
        newlist = ['ET', 'PET', 'SM', 'TEMP', 'PRCP', 'WIN', 'SH', 'SWD', 'RS', 'LW', 'SW', 'QH', 'QLE']
        number = ['a)', 'b)', 'c)', 'd)', 'e)', 'f)', 'g)', 'h)', 'i)', 'g)', 'k)', 'l)', 'm)', 'n)', 'o)', 'p)', 'q)',
                  'r)', 's)', 't)', 'u)', 'v)',
                  'w)', 'x)', 'y)', 'z)', 'aa)', 'ab)', 'ac)', 'ad)', 'ae)', 'af)', 'ag)', 'ah)', 'ai)', 'ag)', 'ak)',
                  'al)', 'am)', 'an)', 'ao)', 'ap)']
        fig,ax = plt.subplots(7,3, sharey='row', sharex='col',figsize=[9, 14.5], subplot_kw=dict(projection=ccrs.PlateCarree()))
        fig.subplots_adjust(bottom=0.02, top=0.98,
                            left=0.07, right=0.995, wspace=0.02, hspace=0.15)
        for rows in range(7):
            data1 = composite_analysis(namelist[rows], months=[18,30], method=method)
            data2 = composite_analysis(namelist[rows], months=[31,48], method=method)
            data3 = composite_analysis(namelist[rows], months=[49,60], method=method)
            # data = np.array([data1, data2, data3])
            # levels = np.linspace(np.nanmin(data, axis=None), np.nanmax(data,axis=None), 11)

            plot_single_ax(ax[rows][0],
                           [[data1,  contour, levels]]
                           , lons, lats,gridline=True, labels=number[rows*3] + newlist[rows] + ' 春季', clipshape=True,
                           ranges=[[57,15, 70, 138],[20,30,40,50],[90,110,130]])

            plot_single_ax(ax[rows][1],
                           [[data2, contour, levels]]
                           , lons, lats,gridline=True, labels=number[rows*3+1]+ newlist[rows] + ' 夏季', clipshape=True
                           ,ranges=[[57, 15, 70, 138], [20, 30, 40, 50], [90, 110, 130]]
                           )
            fill = plot_single_ax(ax[rows][2],
                           [[data3, contour, levels]]
                           , lons, lats,gridline=True, labels=number[rows*3+2]+ newlist[rows] + ' 秋季', clipshape=True
                            ,ranges = [[57, 15, 70, 138], [20, 30, 40, 50], [90, 110, 130]]
                           )
            # cb_ax = fig.add_axes([0.1, 0.12, 0.8, 0.02])
            # # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
            # cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
            # plt.show()

            position = [[0.309,0.92,0.1,0.05],[0.613,0.91,0.1,0.05],[0.928,0.90,0.1,0.05],
                        [0.309,0.645,0.1,0.05],[0.609,0.645,0.1,0.05],[0.928,0.645,0.1,0.05],
                        [0.309,0.465,0.1,0.05],[0.607,0.465,0.1,0.05],[0.928,0.465,0.1,0.05],
                        [0.309,0.285,0.1,0.05],[0.611,0.285,0.1,0.05],[0.928,0.285,0.1,0.05],
                        [0.309,0.105,0.1,0.05],[0.612,0.285,0.1,0.05],[0.928,0.285,0.1,0.05],
                        [0.309,0.285,0.1,0.05],[0.61,0.285,0.1,0.05],[0.928,0.285,0.1,0.05],
                        [0.309,0.285,0.1,0.05],[0.614,0.285,0.1,0.05],[0.928,0.285,0.1,0.05],]
            hwidth = 0.139

            ax_jiuduanxian = fig.add_axes([0.303,0.8605-rows*hwidth,0.08,0.04], projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                            [data1,  contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5,cutdata=True,
                                  ranges=[[25,0, 105, 125],[],[]])

            ax_jiuduanxian = fig.add_axes([0.614,0.8605-rows*hwidth,0.08,0.04], projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                            [data2,  contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5,cutdata=True,
                                  ranges=[[25,0, 105, 125],[],[]])

            ax_jiuduanxian = fig.add_axes([0.925,0.8605-rows*hwidth,0.08,0.04], projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                            [data3,  contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5,cutdata=True,
                                  ranges=[[25,0, 105, 125],[],[]])

        plt.savefig('aaaabbb.png')
        plt.close()
    if type == 3:
        namelist = ['qair', 'swdown', 'root', 'lw', 'sw', 'qh', 'qle']
        newlist = ['SH', 'SWD', 'RS', 'LW', 'SW', 'QH', 'QLE']
        number = ['s)', 't)', 'u)', 'v)',
                  'w)', 'x)', 'y)', 'z)', 'aa)', 'ab)', 'ac)', 'ad)', 'ae)', 'af)', 'ag)', 'ah)', 'ai)', 'ag)', 'ak)',
                  'al)', 'am)', 'an)', 'ao)', 'ap)','aq)', 'ar)', 'as)', 'at)', 'au)', 'av)', 'aw)', 'ax)', 'ay)', 'az)']

        # fig, ax = plt.subplots(7, 3, sharey='row', sharex='col', figsize=[9, 14.5],
        #                        subplot_kw=dict(projection=ccrs.PlateCarree()))
        fig, ax = plt.subplots(2, 3, sharey='row', sharex='col', figsize=[9, 6],
                               subplot_kw=dict(projection=ccrs.PlateCarree()))
        fig.subplots_adjust(bottom=0.02, top=0.98,
                            left=0.07, right=0.995, wspace=0.02, hspace=0.15)
        for rows in range(7):
            data1 = composite_analysis(namelist[rows], months=[18, 30], method=method)
            data2 = composite_analysis(namelist[rows], months=[31, 48], method=method)
            data3 = composite_analysis(namelist[rows], months=[49, 60], method=method)
            # data = np.array([data1, data2, data3])
            # levels = np.linspace(np.nanmin(data, axis=None), np.nanmax(data,axis=None), 11)

            plot_single_ax(ax[rows][0],
                           [[data1, contour, levels]]
                           , lons, lats, gridline=True, labels=number[rows * 3] + newlist[rows] + ' 春季',
                           clipshape=True,
                           ranges=[[57, 15, 70, 138], [20, 30, 40, 50], [90, 110, 130]])

            plot_single_ax(ax[rows][1],
                           [[data2, contour, levels]]
                           , lons, lats, gridline=True, labels=number[rows * 3 + 1] + newlist[rows] + ' 夏季',
                           clipshape=True
                           , ranges=[[57, 15, 70, 138], [20, 30, 40, 50], [90, 110, 130]]
                           )
            fill = plot_single_ax(ax[rows][2],
                                  [[data3, contour, levels]]
                                  , lons, lats, gridline=True, labels=number[rows * 3+2] + newlist[rows] + ' 秋季',
                                  clipshape=True
                                  , ranges=[[57, 15, 70, 138], [20, 30, 40, 50], [90, 110, 130]]
                                  )
            cb_ax = fig.add_axes([0.1, 0.12, 0.8, 0.02])
            # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
            cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal',ticks=list(np.arange(0,1.1,0.1)))
            cbar.ax.set_xticklabels(['0%', '10%', '20%', '30%', '40%', '50%','60%', '70%','80%', '90%','100%'])
            plt.savefig('sldksd.png', dpi=800)
            plt.show()

            position = [[0.309, 0.92, 0.1, 0.05], [0.613, 0.91, 0.1, 0.05], [0.928, 0.90, 0.1, 0.05],
                        [0.309, 0.645, 0.1, 0.05], [0.609, 0.645, 0.1, 0.05], [0.928, 0.645, 0.1, 0.05],
                        [0.309, 0.465, 0.1, 0.05], [0.607, 0.465, 0.1, 0.05], [0.928, 0.465, 0.1, 0.05],
                        [0.309, 0.285, 0.1, 0.05], [0.611, 0.285, 0.1, 0.05], [0.928, 0.285, 0.1, 0.05],
                        [0.309, 0.105, 0.1, 0.05], [0.612, 0.285, 0.1, 0.05], [0.928, 0.285, 0.1, 0.05],
                        [0.309, 0.285, 0.1, 0.05], [0.61, 0.285, 0.1, 0.05], [0.928, 0.285, 0.1, 0.05],
                        [0.309, 0.285, 0.1, 0.05], [0.614, 0.285, 0.1, 0.05], [0.928, 0.285, 0.1, 0.05], ]
            hwidth = 0.139

            ax_jiuduanxian = fig.add_axes([0.303, 0.8605 - rows * hwidth, 0.08, 0.04],
                                          projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                               [data1, contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5, cutdata=True,
                           ranges=[[25, 0, 105, 125], [], []])

            ax_jiuduanxian = fig.add_axes([0.614, 0.8605 - rows * hwidth, 0.08, 0.04],
                                          projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                               [data2, contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5, cutdata=True,
                           ranges=[[25, 0, 105, 125], [], []])

            ax_jiuduanxian = fig.add_axes([0.925, 0.8605 - rows * hwidth, 0.08, 0.04],
                                          projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                               [data3, contour, levels]
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5, cutdata=True,
                           ranges=[[25, 0, 105, 125], [], []])

        plt.savefig('aaaa.png')
        plt.close()
# main(3)

# fig = plt.figure(figsize = [9,6])
# fig.subplots_adjust(bottom=0.1, top=0.95,
#                     left=0.005, right=0.995, wspace=0.02, hspace=0.15)
# for rows in range(8,12):
#     data1 = composite_analysis(namelist[rows], months=[18,30], method=method)
#     data2 = composite_analysis(namelist[rows], months=[31,48], method=method)
#     data3 = composite_analysis(namelist[rows], months=[49,60], method=method)
#     # data = np.array([data1, data2, data3])
#     # levels = np.linspace(np.nanmin(data, axis=None), np.nanmax(data,axis=None), 11)
#     axs = rows-8
#     plot_single_ax(fig.add_subplot(3, 4, 12*(axs//4)+1+axs%4, projection=ccrs.PlateCarree()),
#                    [[data1,  contour, levels]]
#                    , lons, lats, labels=number[rows*3] + newlist[rows] + ' 春季', clipshape=True)
#
#     plot_single_ax(fig.add_subplot(3, 4, 12*(axs//4)+5+axs%4, projection=ccrs.PlateCarree()),
#                    [[data2, contour, levels]]
#                    , lons, lats, labels=number[rows*3+1]+ newlist[rows] + ' 夏季', clipshape=True)
#     fill = plot_single_ax(fig.add_subplot(3, 4,  12*(axs//4)+9+axs%4, projection=ccrs.PlateCarree()),
#                    [[data3, contour, levels]]
#                    , lons, lats, labels=number[rows*3+2]+ newlist[rows] + ' 秋季', clipshape=True)

# for rows in range(12,13):
#     data1 = composite_analysis(namelist[rows], months=[18,30], method=method)
#     data2 = composite_analysis(namelist[rows], months=[31,48], method=method)
#     data3 = composite_analysis(namelist[rows], months=[49,60], method=method)
#     # data = np.array([data1, data2, data3])
#     # levels = np.linspace(np.nanmin(data, axis=None), np.nanmax(data,axis=None), 11)
#     plot_single_ax(fig.add_subplot(1, 4, 1, projection=ccrs.PlateCarree()),
#                    [[data1,  contour, levels]]
#                    , lons, lats, labels=number[rows*3] + newlist[rows] + ' 春季', clipshape=True)
#
#     plot_single_ax(fig.add_subplot(1, 4, 2, projection=ccrs.PlateCarree()),
#                    [[data2, contour, levels]]
#                    , lons, lats, labels=number[rows*3+1]+ newlist[rows] + ' 夏季', clipshape=True)
#     fill = plot_single_ax(fig.add_subplot(1, 4, 3, projection=ccrs.PlateCarree()),
#                    [[data3, contour, levels]]
#                    , lons, lats, labels=number[rows*3+2]+ newlist[rows] + ' 秋季', clipshape=True)
# cb_ax = fig.add_axes([0.1, 0.12, 0.8, 0.02])
# # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
# cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')


fig = plt.figure(figsize = [9,10])
fig.subplots_adjust(top=0.98,
bottom=0.1,
left=0.04,
right=1.0,
hspace=0.15,
wspace=0.02)
position = [[0.27,0.825,0.1,0.05],[0.592,0.825,0.1,0.05],[0.915,0.825,0.1,0.05],
            [0.27,0.645,0.1,0.05],[0.592,0.645,0.1,0.05],[0.915,0.645,0.1,0.05],
            [0.27,0.465,0.1,0.05],[0.592,0.465,0.1,0.05],[0.915,0.465,0.1,0.05],
            [0.27,0.285,0.1,0.05],[0.592,0.285,0.1,0.05],[0.915,0.285,0.1,0.05],
            [0.27,0.105,0.1,0.05]]
for row in range(5):
    for col in range(3):
    # plot_single_ax(fig.add_subplot(7, 6, rows*3+1, projection=ccrs.PlateCarree()),
    #                [[composite_analysis(namelist[rows]), 'contourf', list(np.arange(0, 1.1, 0.1))]]
    #                , lons, lats, labels=number[rows*3] + newlist[rows] + ' all', clipshape=True)
        rows = row*3+col
        if rows < 13:
            data = composite_analysis(namelist[rows], method='types')
            ax = fig.add_subplot(5, 3, rows+1, projection=ccrs.PlateCarree())
            fill = plot_single_ax(ax,
                           [[data[0]-data[1], 'contourf', 'zero']]
                           , lons, lats, labels=number[rows] + newlist[rows]+' 差值', clipshape=True,
                            ranges=[[55, 15, 70, 138], [20, 30, 40, 50], [80, 100, 120]]
                                  )
            ax_jiuduanxian = fig.add_axes(position[rows], projection=ccrs.PlateCarree())
            plot_single_ax(ax_jiuduanxian,
                           [
                            [data[0]-data[1], 'contourf', 'zero']
                           ],
                           lons, lats, clipshape=True, gridline=False, shplinewidth=1.5,
                                  ranges=[[25,0, 105, 125],[],[]])

            if rows % 3 !=0:
                sax = ax
                sax.get_yaxis().set_visible(False)  # 不显示y轴

            if rows <=9:
                sax = ax
                sax.get_xaxis().set_visible(False)  # 不显示y轴
        # if rows == 6:
        #     plt.show()
        else:
            break


    # fill = plot_single_ax(fig.add_subplot(5, 3, rows*3+3, projection=ccrs.PlateCarree()),
    #                [[data[1], 'contourf', list(np.arange(0, 1.1, 0.1))]]
    #                , lons, lats, labels=number[rows*3+2] + newlist[rows] + ' Type2', clipshape=True)

cb_ax = fig.add_axes([0.1, 0.06, 0.8, 0.01])
# cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
# cbar.set_xlabel('%')
plt.show()
plt.savefig('figure2.png', dpi=800)




'''曾经用来绘制剩余五个变量，但是图太丑了，换掉。'''
# namelist = ['root', 'lw', 'sw', 'qh', 'qle']
# newlist = ['RS', 'LW', 'SW', 'QH', 'QLE']
# for rows in range(5):
#     data1 = composite_analysis(namelist[rows], months=[18,30], method=method)
#     data2 = composite_analysis(namelist[rows], months=[31,48], method=method)
#     data3 = composite_analysis(namelist[rows], months=[49,60], method=method)
#     # data = np.array([data1, data2, data3])
#     # levels = np.linspace(np.nanmin(data, axis=None), np.nanmax(data,axis=None), 11)
#
#     plot_single_ax(fig.add_subplot(5, 3, rows*3+1, projection=ccrs.PlateCarree()),
#                    [[data1,  contour, levels]]
#                    , lons, lats, labels=number[rows*3] + newlist[rows] + ' spring', clipshape=True)
#
#     plot_single_ax(fig.add_subplot(5, 3, rows*3+2, projection=ccrs.PlateCarree()),
#                    [[data2, contour, levels]]
#                    , lons, lats, labels=number[rows*3+1]+ newlist[rows] + ' summer', clipshape=True)
#     fill = plot_single_ax(fig.add_subplot(5, 3, rows*3+3, projection=ccrs.PlateCarree()),
#                    [[data3, contour, levels]]
#                    , lons, lats, labels=number[rows*3+2]+ newlist[rows] + ' fall', clipshape=True)
#
# cb_ax = fig.add_axes([0.1, 0.05, 0.8, 0.02])
# # cb_ax = fig.add_axes([0.1, 0.04, 0.8, 0.02])
# cbar = fig.colorbar(fill, cax=cb_ax, orientation='horizontal')
# plt.show()
# plt.savefig('figure2.png', dpi=800)