import step3_function_find_fd_use_oktin as fff
from nc4 import read_nc4, save_nc4
import numpy as np

'''计算全部区域单点的mask'''


# for i in range(3,4):
#     for j in range(5,6):
#         ''''#   单独一个拉出来跑'''
#         a = np.loadtxt(r'shape pevp %i %i.txt'%(i, j))
#         et = np.loadtxt(r'evp %i %i.txt'%(i, j)).reshape([int(i) for i in a])
#         pet = np.loadtxt(r'pevp %i %i.txt'%(i, j)).reshape([int(i) for i in a])
#         rses, percentile_rses, orses, percentile_orses = cal_RSES(et, pet)
#
#         mask = criteria(percentile_rses, percentile_orses)
#         np.savetxt('mask %i %i.txt'%(i, j), mask.flatten())

def del_ocean_fd(mask):
    lat_partition = [312, 340, 380, 420, 456]
    lon_partition = [1008, 1060, 1100, 1140, 1180, 1220, 1260]
    lat = np.loadtxt('lat.csv')
    lon = np.loadtxt('lon.csv')
    slat = lat[lat_partition[0]:lat_partition[4]]
    slon = lon[lon_partition[0]:lon_partition[6]]
    lon_grid, lat_grid = np.meshgrid(slon, slat)
    # 将海洋上错误的闪旱判断设为0
    from global_land_mask import globe
    globe_land_mask = globe.is_ocean(lat_grid, lon_grid)
    for i in range(mask.shape[0]):
        mask[i][globe_land_mask] = -9999.0
    return mask


def cal_area_mean(data, area):
    lat_partition = [312, 340, 380, 420, 456]
    lon_partition = [1008, 1060, 1100, 1140, 1180, 1220, 1260]
    lat = np.loadtxt('lat.csv')
    lon = np.loadtxt('lon.csv')
    lat = lat[lat_partition[0]:lat_partition[4]]
    lon = lon[lon_partition[0]:lon_partition[6]]

    lat0 = int(np.argwhere(lat[:] == area[0]))
    lat1 = int(np.argwhere(lat[:] == area[1]))
    lon0 = int(np.argwhere(lon[:] == area[2]))
    lon1 = int(np.argwhere(lon[:] == area[3]))

    # 将缺省值mask
    data = np.array(data)
    # data = del_ocean_fd(data)
    data = np.ma.masked_equal(data, -9999.0)
    # 将海洋值mask
    meandata = np.nanmean(data[:, lat0:lat1, lon0:lon1], axis=(1, 2))
    return meandata


def find_area_fd(area):
    '''计算区域mask时候使用的
    要求已经有了给定的区域范围，最后给出mask(类似单点mask)'''
    et = read_nc4('china et')
    pet = read_nc4('china pet')
    set = cal_area_mean(et, area)
    spet = cal_area_mean(pet, area)
    rses, percentile_rses, orses, percentile_orses = fff.do_cal_in_onepoint(set, spet)
    mask = fff.do_cri_in_onepoint(percentile_rses, percentile_orses)
    return mask




def area_fd(area, areaname):
    mask = find_area_fd(area)
    et = read_nc4('china et')
    set = cal_area_mean(et, area)
    pet = read_nc4('china pet')
    spet = cal_area_mean(pet, area)

    day_index = list(range(0, 365, 5))
    daylen = [28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    sday = list(range(1, 32))
    smonth = list(np.tile(1, 31))
    for s, i in enumerate(daylen):
        sday.extend(list(range(1, i + 1)))
        smonth.extend(np.tile(s + 2, i))

    sday = np.array(sday)
    day = np.tile(sday[day_index], 58)
    smonth = np.array(smonth)
    month = np.tile(smonth[day_index], 58)
    year = np.arange(1961, 2019).repeat(73)
    from pandas import DataFrame
    import pandas as pd

    data = DataFrame({'year': year, 'month': month, 'day': day})
    datatime = pd.to_datetime(data['year'] * 10000 + data['month'] * 100 + data['day'], format='%Y%m%d')

    datas = DataFrame({'mask': mask, 'et': set, 'pet': spet}, index=datatime)
    datas.to_csv('mask %s.csv' % areaname)


# area_name = ['华中', '西南', '东北', '新疆']
# allarea = [[26.125, 36.125, 110.125, 120.125],
#         [24.125, 33.125, 100.125, 110.125],
#         [40.125, 50.125, 122.125, 134.125],
#         [40.125, 48.125, 80.125, 90.125]]
#
# for i in range(4):
#     print(area_name[i])
#     area_fd(allarea[i], area_name[i])

# area_fd([26.125, 36.125, 110.125, 120.125], 'huazhong')