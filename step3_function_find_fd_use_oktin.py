import numpy as np
import pandas as pd
np.set_printoptions(threshold=np.inf)

# 传入ET，PET为narray格式，考虑到本研究，所以是对网格化的ET，PET做研究。
def cal_RSES(et, pet):
    def szscore(data):
        from scipy.stats.mstats import zscore
        data = data.reshape(58, 73)
        percentile_data = np.zeros([58,73])
        for i in range(73):
            data[:, i] = zscore(data[:, i])
            percentile_data[:, i] = calc_percentile(data[:, i])
        return data.flatten(), percentile_data.flatten()

    def calc_percentile(a, method='min'):
        # 获得传入序列的百分位值
        from scipy.stats import rankdata
        import numpy as np
        if isinstance(a, list):
            a = np.asarray(a)
        s = rankdata(a, method=method) / float(len(a))
        return [round(i, 3) for i in s]

    # 获得rses
    rses = et/pet
    percentile_rses = np.zeros(rses.shape)
    for ilat in range(len(rses[0, :, 0])):
        for ilon in range(len(rses[0, 0, :])):
            rses[:, ilat, ilon], percentile_rses[:, ilat, ilon] = szscore(rses[:, ilat, ilon].flatten())

    # 获得orses
    a0 = np.zeros(rses.shape[1:3])
    a0 = np.insert(rses[:-1], 0, a0, axis=0)
    orses = rses-a0
    percentile_orses = np.zeros(rses.shape)
    for ilat in range(len(rses[0, :, 0])):
        for ilon in range(len(rses[0, 0, :])):
            orses[:, ilat, ilon], percentile_orses[:, ilat, ilon] = szscore(orses[:, ilat, ilon].flatten())
    return rses, percentile_rses, orses, percentile_orses

def do_cal_in_onepoint(et, pet):
    def szscore(data):
        from scipy.stats.mstats import zscore
        data = data.reshape(58, 73)
        percentile_data = np.zeros([58,73])
        for i in range(73):
            data[:, i] = zscore(data[:, i])
            percentile_data[:, i] = calc_percentile(data[:, i])
        return data.flatten(), percentile_data.flatten()

    def calc_percentile(a, method='min'):
        # 获得传入序列的百分位值
        from scipy.stats import rankdata
        import numpy as np
        if isinstance(a, list):
            a = np.asarray(a)
        s = rankdata(a, method=method) / float(len(a))
        return [round(i, 3) for i in s]

    # 获得rses
    rses = et/pet
    percentile_rses = np.zeros(rses.shape)
    rses, percentile_rses = szscore(rses)

    # 获得orses
    a0 = np.insert(rses[:-1], 0, 0, axis=0)
    orses = rses-a0
    percentile_orses = np.zeros(rses.shape)
    orses, percentile_orses = szscore(orses)
    return rses, percentile_rses, orses, percentile_orses


def do_cri_in_onepoint(sorses, srses):
    def find5con(data, srses):
        # 找到变化率在40以下的orses，设定为1
        data = np.array(data)
        srses = np.array(srses)
        data[np.where((data[:] > 0.4))] = 9
        data[np.where(data[:] <= 0.4)] = 1
        data[np.where(data[:] == 9)] = 0
        s = pd.Series(data)
        cumdata = s.groupby(s.eq(0).cumsum()).cumsum().tolist()

        # 找到单独的0，符合前后的rese更小的，单独存储位置。
        def find_zero(data, srses):
            # 传入data为小于orses40%的1111序列累加型。也就是1，2，3，0，1类似形势。
            # 返回其中满足 仅有一个0 且前后rses减小的0值。
            data = np.array(data)
            srses = np.array(srses)
            mask_single_zero = np.zeros(len(data))
            c = np.argwhere(data[:] == 1).flatten()
            c = c[np.argwhere((c - 1) >= 0)].flatten()
            if len(c) != 0:
                d = data[(c - 2)].flatten()
                zero_arg = (c[np.argwhere(d[:] != 0)].flatten()) - 1
                mask_single_zero[zero_arg] = 1
                s = srses[zero_arg + 1] - srses[zero_arg - 1]
                mask_single_zero[zero_arg[np.argwhere(s[:] > 0)]] = 0
            return mask_single_zero

        mask_zero = find_zero(cumdata, srses)
        data[np.where(mask_zero == 1)] = 1
        s = pd.Series(data)
        data = np.array(s.groupby(s.eq(0).cumsum()).cumsum().tolist())

        # 将低于30天的删除
        mask = np.zeros(data.shape)
        cs = np.argwhere(data[:] > 5)
        cc = np.argwhere(data[:] == 5).flatten()
        cs = np.append(cs, cc)
        for i in range(1, 5):
            cs = np.append(cs, cc - i)
        mask[[int(i) for i in cs]] = 1
        data[np.where(mask[:] == 0)] = 0

        return data, mask_zero

    def del_two_zero(mask, mask_zero):

        def ssdel_two_zero(mask_ranges, mask_zero_ranges):
            ## 此函数中mask为单条mask，相当于mask_ranges，只是当初变量设定不够严格，所以暂时不改，在此提示。
            # 本函数需要传入每一条单独的闪旱序列，用处为删除有两个0的闪旱。
            mask_ranges[np.where(mask_ranges[:] > 0)] = 1
            mask_ranges[np.where(mask_zero_ranges[:] == 1)] = 0

            # 将有两个0的给mask了。

            def find_OK5(sp):
                for i, imask_ranges in enumerate(mask_ranges[sp:-5]):
                    ssum = np.sum(mask_ranges[sp:-5][i:i + 5])
                    if ssum == 4:
                        zero_number = 1
                        return sp + i, zero_number
                    if ssum == 5:
                        zero_number = 0
                        return sp + i, zero_number
                return -999, 0

            def find_ep(sp, zero_number):
                for i, imask_ranges in enumerate(mask_ranges[sp + 5:]):
                    if zero_number == 1:
                        if imask_ranges == 0:
                            if mask_ranges[sp + 4 + i] != 0:
                                mask[sp] = 1
                                mask[sp + 4 + i] = 3
                                mask[sp + 1: sp + 4 + i] = 2
                                return (sp + 6 + i)
                            else:

                                mask[sp] = 1
                                mask[sp + 3 + i] = 3
                                mask[sp + 1: sp + 3 + i] = 2
                                return (sp + 6 + i)
                    else:
                        if imask_ranges == 0:
                            zero_number = 1

                return sp + len(mask_ranges[sp:])

            mask = np.zeros(mask_ranges.shape)
            sp = 0
            lenth = len(mask_ranges) - 5
            for i in range(10):
                if sp <= lenth:
                    sp, zero_number = find_OK5(sp)
                    if sp == -999:
                        break
                    sp = find_ep(sp, zero_number)
                else:
                    break
            return mask

        # 如果只有一个闪旱，则无法循环，所以做一个if
        d1 = np.argwhere(mask[:] == 1).flatten()
        if len(d1) == 1:
            mask[int(d1):] = ssdel_two_zero(mask[int(d1):], mask_zero[int(d1):])
        else:
            for i in range(len(d1) - 1):
                mask[d1[i]:d1[i + 1]] = ssdel_two_zero(mask[d1[i]:d1[i + 1]], mask_zero[d1[i]:d1[i + 1]])
            mask[d1[-1]:] = ssdel_two_zero(mask[d1[-1]:], mask_zero[d1[-1]:])
        return mask

    def final_del_less_20(data, rses):
        ## 此处data 为 mask
        data[np.where(data[:] > 0)] = 1
        s = pd.Series(data)
        data = s.groupby(s.eq(0).cumsum()).cumsum().tolist()
        data = np.array(data)

        m = np.argwhere(data[:] >= 5).flatten()
        c = rses[m].flatten()
        a_in_rses = m[np.argwhere(c[:] >= 0.2).flatten()]
        data[a_in_rses] = 0

        d1 = np.argwhere(data[:] == 1).flatten()
        if len(d1) != 0:
            # 将最后不满足rses<20% 以及 两个0的闪旱全部归为0
            def del_only15(ranges):
                if np.nanmax(ranges) <= 4:
                    ranges[:] = 0
                elif np.nanmax(ranges) > 4:
                    a = np.nanargmax(ranges)
                    ranges[1:a] = 2
                    ranges[0] = 1
                    ranges[a] = 3
                return ranges

            # 如果只有一个闪旱，则无法循环，所以做一个if
            if len(d1) == 1:
                data[int(d1):] = del_only15(data[int(d1):])
            else:
                for i in range(len(d1) - 1):
                    data[d1[i]:d1[i + 1]] = del_only15(data[d1[i]:d1[i + 1]])
                data[d1[-1]:] = del_only15(data[d1[-1]:])
        return data

    def per_less_25(data, mask):
        ## 此函数中data为 porses
        def per(data, sp, ep, mask):
            data = np.array(data)
            sp = int(sp)
            ep = int(ep)
            mask = np.array(mask)

            imean = np.mean(data[sp:ep + 1])
            allsp = list(range(sp, 0, -73))
            allsp.extend(list(range(sp + 73, len(data) + 1, 73)))
            allep = list(range(ep, 0, -73))
            allep.extend(list(range(ep + 73, len(data) + 1, 73)))
            meandata = []
            for i in range(len(allsp)):
                meandata.append(np.mean(data[int(allsp[i]): int(allep[i]) + 1]))
            a = np.percentile(meandata, 0.25)
            if imean > a:
                mask[sp:ep + 1] = 0
            return mask

        sp = np.argwhere(mask[:] == 1).flatten()
        ep = np.argwhere(mask[:] == 3).flatten()
        for i in range(len(sp)):
            mask = per(data, sp[i], ep[i], mask)
        return mask

    mask, mask_zero = find5con(sorses, srses)
    mask = del_two_zero(mask, mask_zero)
    mask = final_del_less_20(mask, srses)
    allmask = per_less_25(sorses, mask)
    return allmask

def criteria(rses, orses):
    allmask = np.zeros(orses.shape)
    for ilat in range(len(orses[0, :, 0])):
        for ilon in range(len(orses[0, 0, :])):
            allmask[:, ilat, ilon] = do_cri_in_onepoint(rses[:, ilat, ilon], orses[:, ilat, ilon])
    allmask = del_ocean_fd(allmask)
    return allmask


def del_ocean_fd(mask):
    from nc4 import read_nc4
    slat = read_nc4('china lat')
    slon = read_nc4('china lon')
    lon_grid, lat_grid = np.meshgrid(slon, slat)
    # 将海洋上错误的闪旱判断设为0
    from global_land_mask import globe
    globe_land_mask = globe.is_ocean(lat_grid, lon_grid)
    for i in range(mask.shape[0]):
        mask[i][globe_land_mask] = 0
    return mask

# for i in range(3,4):
#     for j in range(5,6):
#         ''''#   单独一个拉出来跑'''
#         a = np.loadtxt(r'shape pevp %i %i.txt'%(i, j))
#         et = np.loadtxt(r'evp %i %i.txt'%(i, j)).reshape([int(i) for i in a])
#         pet = np.loadtxt(r'pevp %i %i.txt'%(i, j)).reshape([int(i) for i in a])
#         rses, percentile_rses, orses, percentile_orses = cal_RSES(et, pet)
#
#         mask = criteria(percentile_rses, percentile_orses)
#         np.savetxt('mask %i %i.txt'%(i, j), mask.flatten())

























